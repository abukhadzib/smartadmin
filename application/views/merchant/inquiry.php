<ol class="breadcrumb">
	<li>Merchant </li><li>Inquiry</li>
</ol>

</div>

<div id="content">

<section id="widget-grid" class="">
<div class="right_col" role="main">
	<div class="">

<div class="row">
	<div class="col-xs-12">
		<h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-barcode"></i> Report <span> > Laporan Inquiry </span></h3>
	</div>
    <div class="col-md-12 col-xs-12">
        <div class="box-body">
            <div align="left" style="margin-bottom: 1px">   
                <form enctype="multipart/form-data" id="form-search" class="form-horizontal" method="post" action="<?=site_url('merchant/search_inquiry')?>">  
                     <div class="form-group input-group" style="margin: auto;">
                         <input class="form-control" placeholder="Date Range Search" style="width: 200px;" name="daterange" id="daterange" required>
                         <input type="text" class="form-control" name="merchant_code" style="width: 200px;" id="idMerchant" placeholder="Id Merchant" value="">
                         <input type="text" class="form-control" name="msisdn" style="width: 200px;" id="msisdn" placeholder="No Telepon" value="">
                         <button class="btn btn-danger" type="submit" title="Search" id="search"><i class="fa fa-search"></i></button>
                         <button class="btn btn-danger" type="reset" title="Clear Search" id="resetSearch"><i class="glyphicon glyphicon-remove"></i></button>
                         <button type="button" class="btn btn-danger" id="excel" title="Download"><i class="fa fa-download"></i> </button>
                     </div>
                 </form>               
            </div>
        </div>               
    </div>    
    <div class="col-lg-12">
    <div class="table-responsive" style="margin-bottom: auto;  overflow: auto;">	
       <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-xxx" >
            <header >
                <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                <h2>Table Inquiry </h2>
            </header>
           <!-- widget edit box -->
            <div class="jarviswidget-editbox">
                <input class="form-control" type="text">
                <span class="note"><i class="fa fa-check text-success"></i> Change title to update and save instantly!</span>
            </div>
            <!-- widget div-->
            <div>
                <div class="widget-body no-padding">	
                    <table id="dt_basic" class="table table-striped table-bordered table-hover" style="margin-bottom: auto; background: #fff; overflow: auto;">
                        <thead>
                            <tr>
                            <th>#</th>
                            <th>date</th>
                            <th>RequestId </th>
                            <th>Amount </th>
                            <th>Fee </th>
                            <th>Discount </th>
                            <th>Total </th>
                            <th>App Code</th>
                            <th>Merchant Id</th>
                            <th>Merchant</th>
                            <th>Msisdn</th>
                            <th>Name</th>
                            </thead>
                            <tbody>
                                    <?php 
                            $no = 1; 
                            foreach ($content as $row){ 
                        ?>
                            <tr>
                                <td data-title="#"><?=($page*$perpage)+$no;?></td>
                                <td data-title="Date"><?=$row->date;?></td>
                                <td data-title="RequestId"><?=$row->requestid;?></td>
                                <td data-title="Amount"><?=$row->amount;?></td>
                                <td data-title="Amount"><?=$row->fee;?></td>
                                <td data-title="Amount"><?=$row->discount;?></td>
                                <td data-title="Amount"><?=$row->total_amount;?></td>
                                <td data-title="Apps"><?=$row->app_code;?></td>
                                <td data-title="Id Merchant"><?=$row->id_merchant;?></td>
                                <td data-title="Merchant"><?=$row->merchant;?></td>
                                <td data-title="Msisdn"><?=$row->msisdn;?></td>
                                <td data-title="Name"><?=$row->name;?></td>
                            </tr>                         
                        <?php 
                            $no++; 
                            }
                        ?>
                            </tbody>
                    </table>       
                </div>	
            </div>	
       </div>
        <div align="right" style=" margin-top: -25px;">
            <?//=$pagination;?>
        </div>           
    </div>                      
</div>
</div>
</div>
</section>
  
<!-- Modal Hapus -->
<div class="modal fade" id="deleteCodeModal" tabindex="-1" role="dialog" aria-labelledby="labelDeleteCode">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title" id="labelDeleteCode">Hapus Data Product</h4>
            </div>
            <div class="modal-body">  
                <i class="fa fa-lg fa-fw fa-warning"></i>Yakin mau hapus data Product <span id="namaCodeHapus"></span> ?                  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="deleteCodeModalYes" data-code-id="">Ya</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal Notifikasi -->
<div class="modal fade" id="notifikasiCodeModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiCode">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="labelNotifikasiCode">Notifikasi</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="statusNotifikasiCode" style="display: none;"></span>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiCodeOK">OK</button>
            </div>
        </div>
    </div>
</div>

<script>
$(function () {
    $("#tbinquiry").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
    
$('#daterange').daterangepicker({format: 'YYYY/MM/DD'});

$("#search").click(function() {  
   if ( $('#daterange').val()=='' ){
        $('#daterange').focus();
        return false;
    }
});

$("#resetSearch").click(function() {  
   window.location="<?=site_url('merchant/inquiry/')?>";
});


$("#excel").click(function() {   
    if ( $('#daterange').val()=='' ){
        $('#daterange').focus();
        return false;
    }
    
   window.location="<?=site_url('merchant/export_inquiry/')?>?daterange="+$('#daterange').val()+"&merchant_code="+$('#idMerchant').val()+"&msisdn="+$('#msisdn').val();
});
</script>