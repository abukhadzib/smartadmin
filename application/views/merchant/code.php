<style>
    #image{
        margin-left: 30px;
    }  
    #footer{
        margin-left: 30px;
    } 
    p.label{
        text-align: center;
        color: black;
        font-size: 20px;
    }
</style>
<ol class="breadcrumb">
	<li>Merchant </li><li>Code</li>
</ol>

</div>

<div id="content">

<section class="content">
<div class="row">
    <div class="col-xs-12">
		<h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw  fa-qrcode"></i> Your Qrcode <span> > Code </span></h3>
	</div>
    <div class="col-xs-12">
        <div id="image"  style="width: auto;">
            <img src="<?=$qrcode.$code.'.png'?>" /> </br></br>
            <b><input value="<?=$code?>" style="text-align: center;font-size: 20px;width: 250px;border: none;background: transparent;" disabled="disabled"></b><p class="label"><label > </label></p>
        </div>
        </br>
        <div id="footer">
        <button type="button" class="btn btn-danger" onclick="save()">Save Images QR Code Id Merchant</button>
        
        </div>
    </div>
</div>  
</section>

<a href="#"   id="temporary" download="<?=$code.'.png'?>" style="display: none;"></a>
    <div id="img-out" style="display: none">
        <canvas id="newimage"></canvas>
    </div>

<script>
    function save() {
      
       html2canvas($("#image"), {
            onrendered: function(canvas) {
                theCanvas = canvas;
                document.body.appendChild(canvas);

                // Convert and download as image 
                
                $("#newimage").append(canvas);
                try{
                //Canvas2Image.saveAsPNG(canvas, , ,$('#temporary')[0]); 
                Canvas2Image.saveAsPNG({'canvas':canvas,'ahref':$('#temporary')[0]}); 
            }catch(e){
                alert(e);
            }
                // Clean up 
                //document.body.removeChild(canvas);
            }
        });
    }
</script>