<ol class="breadcrumb">
	<li>Merchant </li><li>Edit</li>
</ol>

</div>

<div id="content">

<section class="content">
    <div class="col-xs-12">
            <h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-bar-chart-o"></i> Merchant <span> > Edit Merchant</span></h3>
    </div> 
    
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header">
                  <h3 class="box-title">Edit Data Merchant & LKD <span id="loading-data" style="display:none;margin-left: 10px;"><img src="<?=base_url('assets/img/Preloader_3.gif')?>" /></span></h3>
                  
                </div>
                <form enctype="multipart/form-data" id="form-merchant" class="form-horizontal" method="post" >
                <input type="hidden" name="id" value="<?=$merchant->id?>">
                <div class="box-body">
                    <div class="col-md-3">
                        <div class="form-group">
                          <label>Id Merchant * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-key"></i>
                            </div>
                              <input type="text" name="id_merchant" id="id_merchant" class="form-control" placeholder="Id Merchant" value="<?=$merchant->id_merchant?>" maxlength="6">
                          </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                          <label>Nama Merchant * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-keyboard-o"></i>
                            </div>
                              <input type="text" name="nama_merchant" id="nama_merchant" class="form-control" placeholder="Nama Merchant" value="<?=$merchant->name?>">
                          </div>
                        </div>
                     </div>
                    <div class="col-md-3">
                        <div class="form-group">
                          <label>Handphone * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-phone"></i>
                            </div>
                              <input type="text" name="handphone" id="handphone" class="form-control" placeholder="No Handphone" value="<?=$merchant->phone?>"  maxlength="13">
                          </div>
                        </div>
                   </div>
                    <div class="col-md-3">
                        <div class="form-group">
                          <label>Email :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-phone"></i>
                            </div>
                              <input type="text" name="email" id="email" class="form-control" placeholder="Email" value="<?=$merchant->email?>" >
                          </div>
                        </div>
                   </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Alamat :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-home"></i>
                            </div>
                              <input type="text" name="alamat" id="alamat" class="form-control" placeholder="Alamat" value="<?=$merchant->alamat?>">
                          </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                          <label>Cluster :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-home"></i>
                            </div>
                              <input type="text" name="cluster" id="cluster" class="form-control" placeholder="Cluster" value="<?=$merchant->cluster?>">
                          </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                          <label>Sales :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-user"></i>
                            </div>
                              <input type="text" name="sales" id="sales" class="form-control" placeholder="Sales" value="<?=$merchant->sales?>">
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>PIC :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-user"></i>
                            </div>
                             <input type="text" name="pic" id="pic" class="form-control" placeholder="PIC" value="<?=$merchant->pic?>">
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Kode Pos :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-briefcase"></i>
                            </div>
                              <input type="text" name="zipcode" id="zipcode" class="form-control" placeholder="Kode pos" value="<?=$merchant->zipcode?>" maxlength="7">
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Jenis Usaha :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-shopping-cart"></i>
                            </div>
                              <input type="text" name="usaha" id="usaha" class="form-control" placeholder="Jenis Usaha" value="<?=$merchant->jenis_usaha?>">
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Type Id * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-credit-card"></i>
                            </div>
                          <select name="type" id="type" class="form-control select2" style="width: 100%;">
                            <option value="">--- Type ---</option>
                            <option value="KTP" <?php if($merchant->type_identitas=="KTP") echo 'selected="selected"'; ?>> KTP </option>
                            <option value="SIM" <?php if($merchant->type_identitas=="SIM") echo 'selected="selected"'; ?>> SIM </option>
                            <option value="KTM" <?php if($merchant->type_identitas=="KTM") echo 'selected="selected"'; ?>> KTM </option>
                            <option value="Pasport" <?php if($merchant->type_identitas=="Pasport") echo 'selected="selected"'; ?>>Pasport</option>
                          </select>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>No Identitas * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-credit-card"></i>
                            </div>
                              <input type="text" name="identitas" id="identitas" class="form-control" placeholder="No Identitas" value="<?=$merchant->no_identitas?>" maxlength="16">
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Bank * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-bank"></i>
                            </div>
                          <select name="idbank" id="idbank" class="form-control select2" style="width: 100%;">
                              <option value="">--- Bank ---</option>
                            <?php if ($bank) { foreach($bank as $row_bank) { ?>
                            <option value="<?=$row_bank->kode?>" <?=$row_bank->nama == $merchant->bank?'selected':''?>><?=$row_bank->nama?></option>
                            <?php } } ?>
                          </select>
                        </div>
                        </div>
                        <input type="hidden" id="getText" name="bank" value="<?=$merchant->bank?>">
                        <input type="hidden" id="balance" name="balance" value="<?=$merchant->balance?>">
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>No Rekening * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-bank"></i>
                            </div>
                              <input type="text" name="rekening" id="rekening" class="form-control" placeholder="No Rekening" value="<?=$merchant->rekening?>" maxlength="20">
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Nama Pemilik Rekening * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-bank"></i>
                            </div>
                              <input type="text" name="nama_rekening" id="nama_rekening" class="form-control" placeholder="Nama Pemilik Rekening" value="<?=$merchant->nama_rekening?>">
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Hubungan * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-credit-card"></i>
                            </div>
                          <select name="hubungan" id="hubungan" class="form-control select2" style="width: 100%;">
                            <option value="">--- Hubungan ---</option>
                            <option value="PEMILIK" <?php if($merchant->hubungan=="PEMILIK") echo 'selected="selected"'; ?>> PEMILIK </option>
                            <option value="SUAMI" <?php if($merchant->hubungan=="SUAMI") echo 'selected="selected"'; ?>> SUAMI </option>
                            <option value="ISTRI" <?php if($merchant->hubungan=="ISTRI") echo 'selected="selected"'; ?>> ISTRI </option>
                            <option value="ANAK" <?php if($merchant->hubungan=="ANAK") echo 'selected="selected"'; ?>> ANAK </option>
                            <option value="KERABAT" <?php if($merchant->hubungan=="KERABAT") echo 'selected="selected"'; ?>> KERABAT </option>
                            <option value="OTHER" <?php if($merchant->hubungan=="OTHER") echo 'selected="selected"'; ?>> OTHER </option>
                          </select>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>MDR * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="">%</i>
                            </div>
                              <input type="text" name="mdr" id="mdr" class="form-control" placeholder="MDR" value="<?=$merchant->mdr?>" maxlength="2">
                          </div>
                          <span style="color: red;">* Jumlah dalam % (persen)</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Status Type * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-gear"></i>
                            </div>
                          <select name="status" id="status" class="form-control select2" style="width: 100%;">
                              <option value="">--- Status ---</option>
                              <option value="MERCHANT" <?php if($merchant->status=="MERCHANT") echo 'selected="selected"'; ?>>Merchant</option>
                              <option value="LKD" <?php if($merchant->status=="LKD") echo 'selected="selected"'; ?>>LKD</option>
                          </select>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Discount  :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-cut"></i>
                            </div>
                          <select name="discount" id="discount" class="form-control select2" style="width: 100%;">
                              <option value="">--- Discount ---</option>
                            <?php if ($discount) { foreach($discount as $row_discount) { ?>
                            <option value="<?=$row_discount->id?>" <?=$row_discount->id == $merchant->discount?'selected':''?>><?=$row_discount->name?></option>
                            <?php } } ?>
                          </select>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Is Active :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-credit-card"></i>
                            </div>
                          <select name="is_active" id="type" class="form-control select2" style="width: 100%;">
                            <option value="">--- is_active ---</option>
                            <option value="REACTIVE" <?php if($merchant->is_active=="REACTIVE") echo 'selected="selected"'; ?>> REACTIVE </option>
                            <option value="INACTIVE" <?php if($merchant->is_active=="INACTIVE") echo 'selected="selected"'; ?>> INACTIVE </option>
                          </select>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                <em>* Wajib diisi.</em>
                    <div align="center">
                        <button type="reset" class="btn btn-danger" id="reset" onclick="location.href='<?php echo site_url('merchant')?>'"><i class="fa fa-close"></i> Batal</button>
                        <button type="button" class="btn btn-danger" id="simpan"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
     </div>
            
</section>


<!-- Modal Notifikasi -->
<div class="modal fade" id="notifikasiMerchantModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiMerchant">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="labelNotifikasiPesan">Notifikasi</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="statusNotifikasiMerchant" style="display: none;"></span>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiMerchantOK">OK</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ajaxStart(function(){
	$("#loading-data").show();
    }).ajaxStop(function(){
            $("#loading-data").fadeOut("fast");
        $(window).resize();        
    }); 
    
    $("#handphone,#mdr,#rekening,#identitas,#zipcode").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
             // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

/*
function validate(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[0-9]|\./;
    if( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }
*/

$('#nama_merchant').focus();

$("#idbank").change(function(){
      var isi = $('option:selected', $(this)).text();
       $('#getText').val(isi);
    });
    
$('#simpan').click(function(){
    
    if ( $('#id_merchant').val()=='' || $('#id_merchant').val()===null ){
        $('#id_merchant').focus();
        return false;
    }
    if ( $('#nama_merchant').val()=='' ){
        $('#nama_merchant').focus();
        return false;
    }
    if ( $('#handphone').val()=='' ){
        $('#handphone').focus();
        return false;
    }
    
    /*
    if ( !isValidEmailAddress($('#email').val()) ){
        showNotifikasi('notifikasiMerchantModal','Alamat Email tidak valid !');
        $('#email').focus();
        return false;
    }
    */
   
    if ( $('#type').val()=='' ){
        $('#type').focus();
        return false;
    }
    
    if ( $('#identitas').val()=='' ){
        $('#identitas').focus();
        return false;
    }
    
    if ( $('#idbank').val()=='' ){
        $('#idbank').focus();
        return false;
    }
    
    if ( $('#rekening').val()=='' ){
        $('#rekening').focus();
        return false;
    }
    
    if ( $('#nama_rekening').val()=='' ){
        $('#nama_rekening').focus();
        return false;
    }
    
    if ( $('#hubungan').val()=='' ){
        $('#hubungan').focus();
        return false;
    }
    
    if ( $('#mdr').val()=='' ){
        $('#mdr').focus();
        return false;
    }
    
    if ( $('#status').val()=='' ){
        $('#status').focus();
        return false;
    }
   
    
    var url = '<?=site_url('merchant/merchant_edit/'.$merchant->id)?>';       
    //$(this).attr('data-loading-text','Proses Simpan...').button('loading');         
    $.post(url,$('#form-merchant').serialize(),
    function(result){
        var result = eval('('+result+')');
        if ( result.success );  
        //alert(result.Msg);
       showNotifikasi('notifikasiMerchantModal',result.Msg); 
       $('#statusNotifikasiMerchant').val(result.success);
    });   
});

$("#buttonNotifikasiMerchantOK").click(function() {
    if ( $('#statusNotifikasiMerchant').val() ) {
        //window.location.reload();
        window.location = "<?=site_url('merchant')?>";
    } 
});

function isValidEmailAddress(emailAddress){
    var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
    return pattern.test(emailAddress);
}

function showNotifikasi(idModal,pesan){
    $('#'+idModal+' .modal-body').html(pesan);
    $('#'+idModal).modal('show');
    $('body .modal-backdrop').hide();

}

</script>