<!-- <ol class="breadcrumb">
    <li>Merchant </li><li>Generate Code</li>
</ol>

</div>
-->

<div id="content">
<section id="widget-grid" class="">
<br />
<div class="row" align="center">
    <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0">
     <header>
        <span class="widget-icon"> <i class="fa fa-qrcode"></i> </span>
        <h2>Your Qr Code </h2>				
    </header>
    <div class="jarviswidget-editbox">
        <input class="form-control" type="text">
        <span class="note"><i class="fa fa-check text-success"></i> Change title to update and save instantly!</span>
    </div>
    <div>
        <div class="widget-body no-padding">
            <form id="form-qrcode" class="smart-form" novalidate="novalidate" >
                <header>
                    Silahkan Scan Kode Merchant
                </header>
                <fieldset align="center">
                    <div class="row">
                        <section class="col col-md-6">
                            <div id="qrcode" style="text-align: center;">
                                <img src="<?=$qrcode.$merchant->id_merchant.'.png'?>" /> </br></br>
                           </div>
                            <div align="center">
                                <input id="label" value="<?=$merchant->id_merchant?>"  style=" text-align: center;border: none;background: transparent;font-size: 20px;" disabled="disabled">
                            </div>
                           <a href="#"   id="temporary" download="<?=$merchant->id_merchant?>.png" style="display: none;"></a>
                            <div id="img-out" style="display: none">
                              <canvas id="newimage"></canvas>
                           </div> 
                        </section>
                    </div>
                </fieldset>
                <footer>
                    <div align="center">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <button type="button" class="btn btn-primary" id="generate" style="display: none;">Generate</button>
                        <button type="button" class="btn btn-primary" id="saveQr" onclick="save()"><i class="fa fa-download"></i> download</button>
                    </div>
                </footer>
            </form>
        </div>
    </div>
</div>    
 </section>   
	
</div>
