<ol class="breadcrumb">
    <li>Discount </li><li>List</li>
</ol>

</div>

<div id="content">

<section id="widget-grid" class="">
    <div class="right_col" role="main" id="tb">
        <div class="col-xs-12">
             <h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-bar-chart-o"></i> Discount Merchant <span> > List</span></h3>
        </div> 
        <div class="col-xs-12">
            <div  style="margin-bottom: 1px" align="right"> 
                <button type="button" class="btn btn-danger" id="tambahDiscount" title="add Merchant">Tambah <i class="fa fa-plus"></i> </button>
            </div>
        </div> 
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 45px;">
            <!--<div class="table-responsive" style="margin-bottom: auto;  overflow: auto;">	-->
               <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-xxx" style="margin-bottom: 75px;">
                    <header >
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>Table Discount </h2>
                    </header>
                    <div>
                        <div class="widget-body no-padding">	
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" style="margin-bottom: auto; background: #fff; overflow: auto;">
                                <thead>
                                    <tr>
                                        <th data-hide="phone">#</th>
                                        <th data-hide="phone">From Date</th>
                                        <th data-hide="phone">To Date</th>
                                        <th data-class="expand">Discount Name</th>
                                        <th data-hide="phone">Nominal Get Discount</th>
                                        <th>Discount Type</th>
                                        <th>Discount</th>
                                        <th data-hide="phone">Max Discount</th>
                                        <th data-hide="phone">Max Per User</th>
                                        <th data-hide="phone">Budget</th>
                                        <th data-hide="phone">Status</th>
                                        <th data-hide="phone">Apps</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php 
                                $no = 1; 
                                if ( count($content) > 0 ) {
                                foreach ($content as $row){ 
                            ?>
                                <tr>
                                    <td data-title="#"><?=($page*$perpage)+$no;?>&nbsp;</td>
                                    <td data-title="from"><?=$row->from_date;?>&nbsp;</td>
                                    <td data-title="To"><?=$row->to_date;?>&nbsp;</td>
                                    <td data-title="Name"><?=$row->name;?>&nbsp;</td>
                                    <td data-title="Nom Get Discount"><?=$row->nominal_get_discount;?>&nbsp;</td>
                                    <td data-title="Type"><?=$row->type;?>&nbsp;</td>
                                    <td data-title="Discount"><?=$row->discount;?>&nbsp;</td>
                                    <td data-title="Maximum Discount"><?=$row->max_discount;?>&nbsp;</td>
                                    <td data-title="Maximum per User"><?=$row->limit_user;?>&nbsp;</td>
                                    <td data-title="Budget"><?=$row->limit_balance;?>&nbsp;</td>
                                    <td data-title="Status"><?=$row->status;?>&nbsp;</td>
                                    <td data-title="Apps"><?=$row->app_code;?>&nbsp;</td>
                                    <td data-title="Action">
                                    <a class="editDiscount" href="#" title="Ubah" data-discount-name="<?=$row->name;?>" data-discount-id="<?=$row->id;?>" data-toggle="modal" data-target="#dataDiscountModal"><i class="fa fa-edit"></i></a>&nbsp;
                                    <a class="deleteDiscount" href=#" title="Hapus" data-discount-name="<?=$row->name;?>" data-discount-id="<?=$row->id;?>" data-toggle="modal" data-target="#deleteDiscountModall"><i class="fa fa-trash-o" style="color: red;"></i></a>
                                   </td>
                                </tr>                         
                            <?php 
                                $no++; 
                                } } else {
                            ?>
                                <tr>
                                    <td colspan="15"style="text-align: center;">No Data</td>
                                </tr>
                            <?php } ?>
                                </tbody>
                            </table>         
                        </div>	
                    </div>	
               </div>
                <div align="right" style=" margin-top: -25px;">
                    <?//=$pagination;?>
                </div>           
            <!--</div> -->                     
        </div>
    </div>
</section>
    
<section id="widget-grid" class="">
    <div class="right_col" role="main" id="adddiscount" style="display: none;">
        <div class="col-xs-12">
             <h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-bar-chart-o"></i> Discount Merchant <span> > List</span></h3>
        </div> 
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 45px;">
            <div class="jarviswidget jarviswidget-color-blueDark" style="margin-bottom: 75px;" id="wid-id-aaa" data-widget-editbutton="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                    <h2>Form Add Discount</h2>				
                </header>
                <div>
                    <div class="widget-body no-padding">
                        <form enctype="multipart/form-data" id="form-discount" class="smart-form" novalidate="novalidate" method="post">
                            <fieldset>
                                <div class="row">
                                    <section class="col col-4">
                                        <label class="label">Discount Name * :</label>
                                        <label class="input"> <i class="icon-prepend fa fa-user"></i>
                                            <input type="text" id="name" name="name" placeholder="Discount Name" required>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">Nominal Get Discount * :</label>
                                        <label class="input"> <i class="icon-prepend fa fa-user"></i>
                                            <input type="text" name="get_discount" id="get_discount" class="form-control" placeholder="Nominal Get Discount" >
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">Discount Type  * :</label>
                                        <label class="select">
                                           <select name="type" id="type" class="form-control select2" style="width: 100%;">
                                                <option value=""> Discount Type </option>
                                                <option value="Percentage"> Percentage (%) </option>
                                                <option value="Nominal"> Nominal </option>
                                           </select>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                       <label class="label">Discount * :</label>
                                        <label class="input"> <i class="icon-prepend fa fa-envelope-o"></i>
                                            <input type="text" name="discount" id="discount" placeholder="Discount" onkeyup="myFunction()">
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">Maximum Discount * :</label>
                                        <label class="input"> <i class="icon-prepend fa fa-phone"></i>
                                            <input type="text" name="max_discount" id="max_discount" placeholder="Maximum Discount">
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                          <label class="label">Fee * :</label>
                                          <label class="input"> <i class="icon-prepend fa fa-phone"></i>
                                              <input type="text" name="fee" id="fee" placeholder="Fee">
                                          </label>
                                    </section>
                                    <section class="col col-4">
                                          <label class="label">Apps ( App Code Taps ) *:</label>
                                          <label class="input"> <i class="icon-prepend fa fa-phone"></i>
                                              <input type="text" name="app_code" id="app_code" class="form-control" placeholder="Apps">
                                          </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">From Date * :</label>
                                        <label class="input"> <i class="icon-prepend fa fa-calendar-o"></i>
                                            <input class="form-control" placeholder="From Date" style="width: 100%;" name="from_date" id="from_date" required>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">To Date * :</label>
                                        <label class="input"> <i class="icon-prepend fa fa-calendar-o"></i>
                                            <input class="form-control" placeholder="To Date" style="width: 100%;" name="to_date" id="to_date" required>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">Max Per User :</label>
                                        <label class="input"> <i class="icon-prepend fa fa-phone"></i>
                                              <input type="text" name="maxuser" id="maxuser" class="form-control" placeholder="max user">
                                          </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">Budget Balance :</label>
                                        <label class="input"> <i class="icon-prepend fa fa-phone"></i>
                                              <input type="text" name="budget" id="budget" class="form-control" placeholder="Budget Balance">
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">Discount status  * :</label>
                                        <label class="input"> <i class="icon-prepend fa fa-phone"></i>
                                          <select name="status" id="status" class="form-control select2" style="width: 100%;">
                                            <option value="COSTUM"> Costum </option>
                                            <option value="ALL MERCHANT">All Merchant</option>
                                            <option value="EXPIRED"> Expired </option>
                                          </select>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>
                            <footer>
                                <button type="button" class="btn btn-default" id="reset" data-dismiss="modal">Batal</button>
                                <button type="button" class="btn btn-primary" id="simpan" data-user-id="">Simpan</button>
                            </footer>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>                       
</section>


<!-- Modal Hapus 
<div class="modal fade" id="deleteDiscountModal" tabindex="-1" role="dialog" aria-labelledby="labelDeleteMerchant">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="labelDeleteMerchant">Hapus Data Discount</h4>
            </div>
            <div class="modal-body">  
                <i class="fa fa-lg fa-fw fa-warning"></i>Yakin mau hapus data Discount <span id="namaDiscountHapus"></span> ?                  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="deleteDiscountModalYes" data-Discount-id="">Ya</button>
            </div>
        </div>
    </div>
</div>
-->
<!-- Modal Notifikasi -->
<div class="modal fade" id="notifikasiDiscountModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiDiscount">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="labelNotifikasiDiscount">Notifikasi</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="statusNotifikasiDiscount" style="display: none;"></span>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiDiscountOK">OK</button>
            </div>
        </div>
    </div>
</div>

<!--
<script type="text/javascript" src="<?=$js;?>filereader.js"></script>
<script type="text/javascript" src="<?=$js;?>qrcodelib.js"></script>
<script type="text/javascript" src="<?=$js;?>json-min.js"></script>
<script type="text/javascript" src="<?=$js;?>jquery.qrcode.min.js"></script>
-->

<script>
//Date range picker with time picker
//$('#datetime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});

$("#get_discount,#discount,#max_discount,#fee,#budget,#maxuser").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
         // Allow: Ctrl+C
        (e.keyCode == 67 && e.ctrlKey === true) ||
         // Allow: Ctrl+X
        (e.keyCode == 88 && e.ctrlKey === true) ||
         // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

  // START AND FINISH DATE

$('#from_date').datepicker({
        dateFormat : 'yy-mm-dd',
        prevText : '<i class="fa fa-chevron-left"></i>',
        nextText : '<i class="fa fa-chevron-right"></i>',
});

$('#to_date').datepicker({
        dateFormat : 'yy-mm-dd',
        prevText : '<i class="fa fa-chevron-left"></i>',
        nextText : '<i class="fa fa-chevron-right"></i>',
});

$("#tambahDiscount").click(function() {  
    $("#tb").hide();
    $("#adddiscount").show();
});

$(document).ready(function(){
    $("#type").change(function() {  
        if ( $('#type').val()=='Nominal' ){
           $("#discount").attr('maxlength', 7);
        }
        if ( $('#type').val()=='Percentage' ){
           $("#discount").attr('maxlength', 2);
        }
    });
});

$("#reset").click(function() {  
    $("#tb").show();
    $("#adddiscount").hide();
});

$('#simpan').click(function(){
    
    if ( $('#name').val()=='' ){
        alertNotifikasi('Nama Discount Tidak Boleh Kosong');
        $('#name').focus();
        return false;
    }
    
    if ( $('#get_discount').val()=='' || $('#get_discount').val()===null ){
        alertNotifikasi('Nominal Get Discount Tidak Boleh Kosong');
        $('#get_discount').focus();
        return false;
    }
    if ( $('#type').val()=='' ){
        alertNotifikasi('Type Discount Tidak Boleh Kosong');
        $('#type').focus();
        return false;
    }
    if ( $('#discount').val()=='' ){
        alertNotifikasi('Discount Tidak Boleh Kosong');
        $('#discount').focus();
        return false;
    }
    
    if ( $('#max_discount').val()=='' ){
        alertNotifikasi('Max Discount Tidak Boleh Kosong');
        $('#max_discount').focus();
        return false;
    }
    
    if ( $('#fee').val()=='' ){
        alertNotifikasi('Fee Discount Tidak Boleh Kosong');
        $('#fee').focus();
        return false;
    }
    
    if ( $('#app_code').val()=='' ){
        alertNotifikasi('App Code Tidak Boleh Kosong');
        $('#app_code').focus();
        return false;
    }
    
    if ( $('#rekening').val()=='' ){
        alertNotifikasi('Rekening Tidak Boleh Kosong');
        $('#rekening').focus();
        return false;
    }
    
    if ( $('#from_date').val()=='' ){
        alertNotifikasi('Start Date Tidak Boleh Kosong');
        $('#from_date').focus();
        return false;
    }
    if ( $('#to_date').val()=='' ){
        alertNotifikasi('End Date Tidak Boleh Kosong');
        $('#to_date').focus();
        return false;
    }
    if ( $('#maxuser').val()=='' ){
        alertNotifikasi('Max User Tidak Boleh Kosong');
        $('#maxuser').focus();
        return false;
    }
    if ( $('#budget').val()=='' ){
        alertNotifikasi('Budget Tidak Boleh Kosong');
        $('#budget').focus();
        return false;
    }
    var url = '<?=site_url('merchant/discount_add')?>';       
    //$(this).attr('data-loading-text','Proses Simpan...').button('loading');         
    $.post(url,$('#form-discount').serialize(),
    function(result){
        var result = eval('('+result+')');
        smartNotifikasi(result)
        //if ( result.success );  
        //alert(result.Msg);
       //showNotifikasi('notifikasiDiscountModal',result.Msg); 
      // $('#statusNotifikasiDiscount').val(result.success);
    });   
});

$('.editDiscount').click(function() {
    var id = $(this).attr('data-discount-id');
    var url = '<?=site_url('merchant/discount_edit')?>'+'/'+id;
    window.location = url;
});


$(".deleteDiscount").click(function(e) {
    var name = $(this).attr('data-discount-name');
    var id = $(this).attr('data-discount-id');
    var url = '<?=site_url('merchant/discount_delete')?>'+'/'+id;        
    $.SmartMessageBox({
        title : "Confirmation !",
        content : "<i class='fa fa-lg fa-fw fa-warning'></i> Yakin Hapus Data Discount " + name + " ?",
        buttons : '[No][Yes]'
    }, function(ButtonPressed) {
    if (ButtonPressed === "Yes") {
        $.post(url,{action:'delete',id:id},
        function(result){
            var result = eval('('+result+')');

            if(result.success){
                successNotifikasi(result.Msg);
            }else{
                alertNotifikasi(result.Msg);
            }
        }); 

        }
        if (ButtonPressed === "No") {
            $.smallBox({
                title : "Notifikasi ",
                content : "<i class='fa fa-clock-o'></i> <i>You Cancel Process...</i>",
                color : "#C46A69",
                iconSmall : "fa fa-times fa-2x fadeInRight animated",
                timeout : 4000
            });
        }

    });
    e.preventDefault();
})

/*
$(".deleteDiscount").click(function() {
    var name = $(this).attr('data-discount-name');
    var id = $(this).attr('data-discount-id');
	$('#deleteDiscountModalYes').attr('data-discount-id',id);
    $('#namaDiscountHapus').html('<b>'+name+'</b>');
});    

$('#deleteDiscountModalYes').click(function() {
    var id  = $(this).attr('data-discount-id');
    var url = '<?=site_url('merchant/discount_delete')?>'+'/'+id; 
    $(this).attr('data-loading-text','Proses Delete...').button('loading');        
    $.post(url,{action:'delete',id:id},
    function(result){
        var result = eval('('+result+')');
        $('#deleteCodeModal').modal('hide');
        showNotifikasi('notifikasiDiscountModal',result.Msg);
        $('#statusNotifikasiDiscount').val(result.success);
    });        
});
*/
   
$("#buttonNotifikasiDiscountOK").click(function() {
    if ( $('#statusNotifikasiDiscount').val() ) {
        window.location.reload();
        //window.location = "<?=site_url('merchant/discount')?>";
    } 
});

function showNotifikasi(idModal,pesan){
    $('#'+idModal+' .modal-body').html(pesan);
    $('#'+idModal).modal('show');
    $('body .modal-backdrop').hide();

}

</script>