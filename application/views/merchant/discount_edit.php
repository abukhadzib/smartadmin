<ol class="breadcrumb">
	<li>Discount </li><li>Edit</li>
</ol>

</div>

<div id="content">

<section class="content">
    <div class="col-xs-12">
            <h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-bar-chart-o"></i> Discount <span> > Edit Merchant Discount</span></h3>
    </div> 
    
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header">
                  <h3 class="box-title">Edit Data Discount <span id="loading-data" style="display:none;margin-left: 10px;"><img src="<?=base_url('assets/img/Preloader_3.gif')?>" /></span></h3>
                  
                </div>
                <form enctype="multipart/form-data" id="form-edit-discount" class="form-horizontal" method="post" >
                <input type="hidden" name="id" value="<?=$discount->id?>">
                <div class="box-body">
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Discount Name * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-key"></i>
                            </div>
                              <input type="text" name="name" id="name" class="form-control" placeholder="Discount Name" value="<?=$discount->name?>">
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Nominal Get Discount * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-key"></i>
                            </div>
                              <input type="text" name="get_discount" id="get_discount" class="form-control" placeholder="Nominal Get Discount" value="<?=$discount->nominal_get_discount?>">
                          </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Discount Type  * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-credit-card"></i>
                            </div>
                          <select name="type" id="type" class="form-control select2" style="width: 100%;">
                            <option value="">--- Discount Type ---</option>
                            <option value="Percentage" <?php if($discount->type=="Percentage") echo 'selected="selected"'; ?>> Percentage (%) </option>
                            <option value="Nominal" <?php if($discount->type=="Nominal") echo 'selected="selected"'; ?>> Nominal </option>
                          </select>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Discount * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-phone"></i>
                            </div>
                              <input type="text" name="discount" id="discount" class="form-control" placeholder="Discount" value="<?=$discount->discount?>">
                          </div>
                        </div>
                   </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Maximum Discount :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-envelope-o"></i>
                            </div>
                              <input type="text" name="max_discount" id="max_discount" class="form-control" placeholder="Maximum Discount" value="<?=$discount->max_discount?>">
                          </div>
                        </div>
                   </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Fee :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-home"></i>
                            </div>
                              <input type="text" name="fee" id="fee" class="form-control" placeholder="Fee" value="<?=$discount->fee?>">
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Apps ( App Code Taps ) *:</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-home"></i>
                            </div>
                              <input type="text" name="app_code" id="app_code" class="form-control" placeholder="Apps" value="<?=$discount->app_code?>">
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>From Date * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar-o"></i>
                            </div>
                            <input class="form-control" placeholder="From Date" style="width: 100%;" name="from_date" id="from_date" value="<?=$discount->from_date?>" required>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>To Date * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar-o"></i>
                            </div>
                            <input class="form-control" placeholder="To Date" style="width: 100%;" name="to_date" id="to_date" value="<?=$discount->to_date?>" required>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Max Per User :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-user"></i>
                            </div>
                              <input type="text" name="maxuser" id="maxuser" class="form-control" placeholder="max user" value="<?=$discount->limit_user?>">
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Budget Balance :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-user"></i>
                            </div>
                              <input type="text" name="budget" id="budget" class="form-control" placeholder="Budget Balance" value="<?=$discount->limit_balance?>">
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Discount Status  * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-credit-card"></i>
                            </div>
                          <select name="status" id="status" class="form-control select2" style="width: 100%;">
                            <option value="COSTUM" <?php if($discount->status=="COSTUM") echo 'selected="selected"'; ?>> Costum </option>
                            <option value="ALL MERCHANT" <?php if($discount->status=="ALL MERCHANT") echo 'selected="selected"'; ?>> All Merchant </option>
                            <option value="EXPIRED" <?php if($discount->status=="EXPIRED") echo 'selected="selected"'; ?>> Expired </option>
                          </select>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                <em>* Wajib diisi.</em>
                    <div align="center">
                        <button type="reset" class="btn btn-danger" id="reset" onclick="location.href='<?php echo site_url('merchant/discount')?>'"><i class="fa fa-close"></i> Batal</button>
                        <button type="button" class="btn btn-danger" id="saveDiscount"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
     </div>
            
</section>

<!-- Modal Notifikasi -->
<div class="modal fade" id="notifikasiDiscountModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiDiscount">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="labelNotifikasiDiscount">Notifikasi</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="statusNotifikasiDiscount" style="display: none;"></span>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiDiscountOK">OK</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ajaxStart(function(){
	$("#loading-data").show();
    }).ajaxStop(function(){
            $("#loading-data").fadeOut("fast");
        $(window).resize();        
    }); 
    
   $("#get_discount,#discount,#max_discount,#fee").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
         // Allow: Ctrl+C
        (e.keyCode == 67 && e.ctrlKey === true) ||
         // Allow: Ctrl+X
        (e.keyCode == 88 && e.ctrlKey === true) ||
         // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

$(document).ready(function(){
    $("#type").change(function() {  
        if ( $('#type').val()=='Nominal' ){
           $("#discount").attr('maxlength', 7);
        }
        if ( $('#type').val()=='Percentage' ){
           $("#discount").attr('maxlength', 2);
        }
    });
});

$('#from_date, #to_date').datepicker({format: 'yyyy-mm-dd'});

$('#saveDiscount').click(function(){
    
    if ( $('#name').val()=='' ){
        $('#name').focus();
        return false;
    }
    
    if ( $('#get_discount').val()==''){
        $('#get_discount').focus();
        return false;
    }
    if ( $('#type').val()=='' ){
        $('#type').focus();
        return false;
    }
    if ( $('#discount').val()=='' ){
        $('#discount').focus();
        return false;
    }
    
    if ( $('#max_discount').val()=='' ){
        $('#max_discount').focus();
        return false;
    }
    
    if ( $('#fee').val()=='' ){
        $('#fee').focus();
        return false;
    }
    
    if ( $('#app_code').val()=='' ){
        $('#app_code').focus();
        return false;
    }
    
    var url = '<?=site_url('merchant/discount_edit')?>';       
    //$(this).attr('data-loading-text','Proses Simpan...').button('loading');         
    $.post(url,$('#form-edit-discount').serialize(),
    function(result){
        var result = eval('('+result+')');
        if ( result.success );  
        //alert(result.Msg);
       showNotifikasi('notifikasiDiscountModal',result.Msg); 
       $('#statusNotifikasiDiscount').val(result.success);
    });   
});

$("#buttonNotifikasiDiscountOK").click(function() {
    if ( $('#statusNotifikasiDiscount').val() ) {
        //window.location.reload();
        window.location = "<?=site_url('merchant/discount')?>";
    } 
});

function showNotifikasi(idModal,pesan){
    $('#'+idModal+' .modal-body').html(pesan);
    $('#'+idModal).modal('show');
    $('body .modal-backdrop').hide();

}

</script>