<ol class="breadcrumb">
	<li>Merchant </li><li>Generate Code</li>
</ol>

</div>

<div id="content">

<section id="widget-grid" class="">
    <section class="content-header">
        <h1>
          <i class="fa fa-barcode"></i> Generate Kode Merchant
        </h1>
      </section>
<article class="col-xs-12 col-sm-12 col-md-8 col-lg-6">    
    <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0">
         <header>
            <span class="widget-icon"> <i class="fa fa-qrcode"></i> </span>
            <h2>Form Generate Code </h2>				
        </header>
        <div>
            <div class="jarviswidget-editbox">
                <input class="form-control" type="text">
                <span class="note"><i class="fa fa-check text-success"></i> Change title to update and save instantly!</span>
            </div>
            <div class="widget-body">
                <form id="form-qrcode" class="smart-form" novalidate="novalidate" >
                    <header>
                        Silahkan Masukan Kode Merchant
                    </header>
                    <fieldset>
                        <div class="row">
                            <section class="col col-6">
                                <label class="label">Kode Merchant</label>
                                <label class="input"> <i class="icon-append fa fa-qrcode"></i>
                                    <input type="text" name="code" id="code" placeholder="Kode Merchant" maxlength="6">
                                </label>
                                <span style="color:red;">* Maximum Code 6 Digit</span>
                            </section>
                        </div>
                    </fieldset>
                    <footer>
                        <div align="center">
                            <button type="reset" class="btn btn-danger" id="reset"><i class="fa fa-close"></i> Reset</button>
                            <button type="submit" class="btn btn-danger" id="saveProduct"><i class="fa fa-qrcode"></i> Generate</button>
                        </div>
                    </footer>
                </form>
            </div>
        </div>
    </div>  
</article>
</section>
   
<!-- Modal Notifikasi -->
<div class="modal fade" id="notifikasiMerchantModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiMerchant">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="labelNotifikasiPesan">Notifikasi</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="statusNotifikasiMerchant" style="display: none;"></span>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiMerchantOK">OK</button>
            </div>
        </div>
    </div>
</div>

<script>

$('#saveProduct').click(function(){
    
    if ( $('#code').val()=='' ){
        $('#code').focus();
        return false;
    }
   
    var url = '<?=site_url('merchant/qrcode')?>';       
    //$(this).attr('data-loading-text','Generate...').button('loading');         
    $.post(url,$('#form-qrcode').serialize(),
    function(result){
        var result = eval('('+result+')');
        if ( result.success );  
       showNotifikasi('notifikasiMerchantModal',result.Msg); 
       $('#statusNotifikasiMerchant').val(result.success);
    });  
    return false;
});

$("#buttonNotifikasiMerchantOK").click(function() {
    if ( $('#statusNotifikasiMerchant').val() ) {
        window.location="<?=site_url('merchant/showCode/');?>/"+$('#code').val();
    } 
});

     function showNotifikasi(idModal,pesan){
        $('#'+idModal+' .modal-body').html(pesan);
        $('#'+idModal).modal('show');
        $('body .modal-backdrop').hide();
    }
    
    </script>