<ol class="breadcrumb">
	<li>Merchant </li><li>List</li>
</ol>

</div>

<div id="content">

<section id="widget-grid" class="">
<div class="row">
    <div class="col-xs-12">
        <h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-bar-chart-o"></i> Merchant & LKD <span> > List</span></h3>
    </div> 
    
        <div class="box-body">
            <div align="left" style="margin-bottom: 1px">   
                <form enctype="multipart/form-data" id="form-search" class="form-horizontal" method="post" action="<?=site_url('merchant/search_merchant')?>">  
                     <div class="form-group input-group" style="margin: auto;">
                         <input type="text" class="form-control" name="merchant_code" style="width: 200px;" id="idMerchant" placeholder="Id Merchant" value="">
                         <input type="text" class="form-control" name="msisdn" style="width: 200px;" id="msisdn" placeholder="No Telepon" value="">
                         <button class="btn btn-danger" type="submit" title="Search"><i class="fa fa-search"></i></button>
                         <button class="btn btn-danger" type="reset" title="Clear Search" id="resetSearch"><i class="glyphicon glyphicon-remove"></i></button>
                         <button type="button" class="btn btn-danger" id="download" title="Download" data-toggle="modal" data-target="#dataPesanModalAdmin"><i class="fa fa-download"></i> </button>
                         <button type="button" class="btn btn-danger" id="tambahMerchant" title="add Merchant"><i class="fa fa-plus"></i> </button>
                     </div>
                 </form>               
            </div>
        </div> 
    <div class="col-lg-12">
        <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-xxx" >
             <header >
                 <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                 <h2>Table Merchant & LKD </h2>
             </header>
             <div class="jarviswidget-editbox">
                 <input class="form-control" type="text">
                 <span class="note"><i class="fa fa-check text-success"></i> Change title to update and save instantly!</span>
             </div>
             <div>
                <div class="widget-body no-padding">	
                    <div class="table-responsive" style="margin-bottom: 19px; overflow: auto;">	
                        <table id="dt_basic" class="table table-striped table-bordered table-hover"  background: #fff; overflow: auto;">
                            <thead>
                                <tr>
                                    <th data-hide="phone">#</th>
                                    <th data-class="expand">Kode</th>
                                    <th>Merchant</th>
                                    <!--<th data-hide="phone">Alamat</th>-->
                                    <!--<th>Usaha</th>-->
                                    <th data-hide="phone">Telepon</th>
                                    <!--<th>Email</th>-->
                                    <th data-hide="phone">Identitas</th>
                                    <th data-hide="phone">Bank</th>
                                    <th data-hide="phone">Rek</th>
                                    <th data-hide="phone">Pemilik Rek</th>
                                    <!--<th>Balance</th>-->
                                    <!--<th style="width: 30px;">MDR</th>-->
                                    <!--<th>Fee</th>
                                    <th>Withdraw</th>
                                    <th>Cluster</th>-->
                                    <th data-hide="phone">PIC</th>
                                    <th data-hide="phone">Sales</th>
                                    <th data-hide="phone">Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                    <?php 
                            $no = 1; 
                            if ( count($content) > 0 ) {
                            foreach ($content as $row){ 
                        ?>
                            <tr>
                                <td data-title="#"><?=($page*$perpage)+$no;?>&nbsp;</td>
                                <td data-title="Id"><?=$row->id_merchant;?>&nbsp;</td>
                                <td data-title="Name"><?=$row->name;?>&nbsp;</td>
                                <!--<td data-title="Alamat"><?=$row->alamat;?>&nbsp;</td>-->
                                <!--<td data-title="Usaha"><?=$row->jenis_usaha;?>&nbsp;</td>-->
                                <td data-title="Telp"><?=$row->phone;?>&nbsp;</td>
                                <!--<td data-title="Email"><?=$row->email;?>&nbsp;</td>-->
                                <td data-title="Identitas"><?=$row->no_identitas;?>&nbsp;</td>
                                <td data-title="Bank"><?=$row->bank;?>&nbsp;</td>
                                <td data-title="NoRek"><?=$row->rekening;?>&nbsp;</td>
                                <td data-title="NamaRek"><?=$row->nama_rekening;?>&nbsp;</td>
                                <!--<td data-title="Balance"><?=$row->balance;?>&nbsp;</td>-->
                                <!--<td data-title="MDR"><?=$row->mdr;?>%&nbsp;</td>-->
                               <!-- <td data-title="Fee"><?=$row->fee;?>&nbsp;</td>
                                <td data-title="Withdraw"><?=$row->withdraw;?>&nbsp;</td>
                                <td data-title="Cluster"><?=$row->cluster;?>&nbsp;</td> -->
                                <td data-title="PIC"><?=$row->pic;?>&nbsp;</td>
                                <td data-title="Sales"><?=$row->sales;?>&nbsp;</td>
                                <td data-title="Status"><?php if($row->status == 'MERCHANT'){?><span class="label label-sm label-danger">Merchant</span><?php }else{?><span class="label label-sm label-success">LKD</span><?php } ?>&nbsp;</td>
                                <td data-title="Action">
                                <a class="generateCode" href="#" title="QrCode" data-merchant-name="<?=$row->name;?>" data-merchant-id="<?=$row->id;?>" data-toggle="modal" data-target="#dataCodeModal"><i class="fa fa-qrcode" style="color: green;"></i></a>&nbsp;
                                <a class="editMerchant" href="#" title="Ubah" data-merchant-name="<?=$row->name;?>" data-merchant-id="<?=$row->id;?>" data-toggle="modal" data-target="#dataMerchantModal"><i class="fa fa-edit"></i></a>&nbsp;
                                <a class="deleteMerchant" href=#" title="Hapus" data-merchant-name="<?=$row->name;?>" data-merchant-id="<?=$row->id;?>" data-toggle="modal" data-target="#deleteMerchantModal"><i class="fa fa-trash-o" style="color: red;"></i></a>
                               </td>
                            </tr>                         
                        <?php 
                            $no++; 
                            } } else {
                        ?>
                            <tr>
                                <td colspan="15"style="text-align: center;">No Data</td>
                            </tr>
                        <?php } ?>
                            </tbody>
                        </table>            
                    </div>	
                </div>	
           </div>
            <div align="right" style=" margin-top: -25px;">
                <?//=$pagination;?>
            </div>           
        </div>                      
    </div>
</div>                       
</section>


<!-- Modal Data Tambah dan Edit -->
<div class="modal fade" id="dataPesanModalAdmin" tabindex="-1" role="dialog" aria-labelledby="labelDataPesan">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title"><span id="labelDataPesan"></span> Silahkan Pilih Data Yg Mau di Export <span id="loading-data" style="display:none;margin-left: 10px;"><img src="<?=base_url('assets/img/loading.gif')?>" /></span></h4>
            </div>
            <div class="modal-body">
                <div class="col-md-8">
                    <div class="form-group">
                      <label>Status Type * :</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-gear"></i>
                        </div>
                      <select name="status" id="status" class="form-control select2" style="width: 100%;">
                          <option value="">--- Status ---</option>
                          <option value="MERCHANT">Merchant</option>
                          <option value="LKD">LKD</option>
                      </select>
                    </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                      <label>Date * :</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar-o"></i>
                        </div>
                        <input class="form-control" placeholder="Date Range" style="width: 100%;" name="daterange" id="daterange" required>
                    </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="excel" >Export</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Hapus -->
<div class="modal fade" id="deleteMerchantModal" tabindex="-1" role="dialog" aria-labelledby="labelDeleteMerchant">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title" id="labelDeleteMerchant">Hapus Data Merchant</h4>
            </div>
            <div class="modal-body">  
                <i class="fa fa-lg fa-fw fa-warning"></i>Yakin mau hapus data Merchant <span id="namaMerchantHapus"></span> ?                  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="deleteMerchantModalYes" data-Merchant-id="">Ya</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="dataCodeModal" tabindex="-1" role="dialog" aria-labelledby="labelDataCode">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title"><span id="labelDataCode"></span> Your QR Code <span id="loading-data" style="display:none;margin-left: 10px;"><img src="<?=base_url('assets/img/Preloader_3.gif')?>" /></span></h4></h4>
            </div>
            <div class="modal-body">
                <div id="qrcode" style="text-align: center;">
                
                </div>
                 <a href="#"  id="temporary" download="idmerchant.png" style="display: none;"></a>
                <div id="img-out" style="display: none">
                    <canvas id="newimage"></canvas>
                </div> 
            </div>
            <!--
            <div class="modal-footer">
                <span id="dataCodeModalURL" style="display: none;"></span>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="generate" style="display: none;">Generate</button>
                <button type="button" class="btn btn-primary" id="saveQr" onclick="save()" style="display: none;"><i class="fa fa-download"></i> download</button>
            </div>
            -->
        </div>
    </div>
</div>

<!-- Modal Notifikasi -->
<div class="modal fade" id="notifikasiMerchantModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiMerchant">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="labelNotifikasiMerchant">Notifikasi</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="statusNotifikasiMerchant" style="display: none;"></span>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiMerchantOK">OK</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?=$js;?>filereader.js"></script>
<script type="text/javascript" src="<?=$js;?>qrcodelib.js"></script>
<script type="text/javascript" src="<?=$js;?>json-min.js"></script>
<script type="text/javascript" src="<?=$js;?>jquery.qrcode.min.js"></script>
<script src="<?=$js;?>html2canvas.js"></script>
<script src="<?=$js;?>canvas2image.js"></script>

<script>
$(function () {
    $("#tbmerchant").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
  
//$('#daterange').daterangepicker({format: 'YYYY/MM/DD'});

$("#tambahMerchant").click(function() {  
   window.location="<?=site_url('merchant/merchant_add/')?>";
});

$("#resetSearch").click(function() {  
   window.location="<?=site_url('merchant/merchant/')?>";
});

$("#excel").click(function() {  
   var type = $('#status').val();
   if ( $('#status').val()=='' ){
        $('#status').focus();
        return false;
    }
   if ( $('#daterange').val()=='' ){
        $('#daterange').focus();
        return false;
    }
   //window.location='<?=site_url('merchant/export_merchant/')?>'+'/'+type;
   window.location="<?=site_url('merchant/export_merchant/')?>?daterange="+$('#daterange').val()+"&type="+$('#status').val();
});

$('.editMerchant').click(function() {
    var id = $(this).attr('data-merchant-id');
    var url = '<?=site_url('merchant/merchant_edit')?>'+'/'+id;
    window.location = url;
});

$(".deleteMerchant").click(function() {
    var name = $(this).attr('data-merchant-name');
    var id = $(this).attr('data-merchant-id');
	$('#deleteMerchantModalYes').attr('data-merchant-id',id);
    $('#namaMerchantHapus').html('<b>'+name+'</b>');
});    
$('#deleteMerchantModalYes').click(function() {
    var id  = $(this).attr('data-merchant-id');
    var url = '<?=site_url('merchant/merchant_delete')?>'+'/'+id; 
    $(this).attr('data-loading-text','Proses Delete...').button('loading');        
    $.post(url,{action:'delete',id:id},
    function(result){
        var result = eval('('+result+')');
        $('#deleteCodeModal').modal('hide');
        showNotifikasi('notifikasiMerchantModal',result.Msg);
        $('#statusNotifikasiMerchant').val(result.success);
    });        
});

$("#buttonNotifikasiMerchantOK").click(function() {
    if ( $('#statusNotifikasiMerchant').val() ) {
        window.location.reload();
        //window.location = "<?=site_url('merchant')?>";
    } 
});

function showNotifikasi(idModal,pesan){
    $('#'+idModal+' .modal-body').html(pesan);
    $('#'+idModal).modal('show');
    $('body .modal-backdrop').hide();

}

$('.generateCode').click(function() {
        var id = $(this).attr('data-merchant-id');
        var url = '<?=site_url('merchant/generate_code')?>'+'/'+id;
        $('#saveQr').show();
        $('#dataCodeModal .modal-body').html('');
        $('#dataCodeModal .modal-body').load(url);
        $('#dataCodeModalURL').text(url);
});


 function save() {
      
       html2canvas($("#qrcode"), {
            onrendered: function(canvas) {
                theCanvas = canvas;
                document.body.appendChild(canvas);

                // Convert and download as image 
                
                $("#newimage").append(canvas);
                try{
                //Canvas2Image.saveAsPNG(canvas, , ,$('#temporary')[0]); 
                Canvas2Image.saveAsPNG({'canvas':canvas,'ahref':$('#temporary')[0]}); 
            }catch(e){
                alert(e);
            }
                // Clean up 
                //document.body.removeChild(canvas);
            }
        });
    }

</script>