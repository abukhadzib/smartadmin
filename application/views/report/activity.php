<ol class="breadcrumb">
    <li>Log Activity </li><li>Report</li>
</ol>
</div>

<div id="content">
<section id="widget-grid" class="">
<div class="row">
    <div class="col-lg-12">
    <div class="table-responsive" style="margin-bottom: auto;  overflow: auto;">	
       <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-xxx" >
            <header >
                <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                <h2>Table Log Activity </h2>
            </header>
           <!-- widget edit box -->
            <div class="jarviswidget-editbox">
                <input class="form-control" type="text">
                <span class="note"><i class="fa fa-check text-success"></i> Change title to update and save instantly!</span>
            </div>
            <!-- widget div-->
            <div>
                <div class="widget-body no-padding">	
                    <table id="dt_basic" class="table table-striped table-bordered table-hover" style="margin-bottom: auto; background: #fff; overflow: auto;">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tanggal</th>
                                <th>User</th>
                                <th>Action</th>
                                <th>Modul</th>
                                <th>IP</th>
                                <th>User Agent</th>
                            </tr>
        		</thead>
        		<tbody>
        			<?php 
                        $no = 1; 
                        if ( count($content) > 0 ) {
                        foreach ($content as $row){ 
                    ?>
                        <tr>
                            <td><?=($page*$perpage)+$no;?></td>
                            <td><?=date("d-m-Y H:i:s", strtotime($row->created_date));?></td>
                            <td><?=$row->user_name;?></td>
                            <td><?=$row->action;?></td>
                            <td><?=$row->modul;?></td>
                            <td><?=$row->ip;?></td>
                            <td><?=$row->ua;?></td>
                        </tr>                         
                    <?php 
                        $no++; 
                        } } else {
                    ?>
                        <tr>
                            <td colspan="7"style="text-align: center;">No Data</td>
                        </tr>
                    <?php } ?>
        		</tbody>
                    </table>         
                </div>	
            </div>	
       </div>
        <div align="right" style=" margin-top: -25px;">
            <?//=$pagination;?>
        </div>           
    </div>                      
</div>
</div>
 </section>

<!-- Modal Hapus -->
<div class="modal fade" id="deletePesanModal" tabindex="-1" role="dialog" aria-labelledby="labelDeletePesan">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title" id="labelDeletePesan">Hapus Data Pesan</h4>
            </div>
            <div class="modal-body">  
                <i class="fa fa-lg fa-fw fa-warning"></i>Yakin mau hapus data Pesan <span id="namaPesanHapus"></span> ?                  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="deletePesanModalYes" data-Pesan-id="">Ya</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Data Tambah dan Edit -->
<div class="modal fade" id="dataPesanModal" tabindex="-1" role="dialog" aria-labelledby="labelDataPesan">
    <div class="modal-dialog" role="document" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <!--button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title"><span id="labelDataPesan"></span> Data Pesan <span id="loading-data" style="display:none;margin-left: 10px;"><img src="<?=base_url('assets/img/loading.gif')?>" /></span></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="dataPesanModalURL" style="display: none;"></span>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="simpanPesanModal" data-Pesan-id="">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Notifikasi -->
<div class="modal fade" id="notifikasiPesanModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiPesan">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="labelNotifikasiPesan">Notifikasi</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="statusNotifikasiPesan" style="display: none;"></span>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiPesanOK">OK</button>
            </div>
        </div>
    </div>
</div>
