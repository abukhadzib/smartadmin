<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en-us">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="">
	<meta name="author" content="">			
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title><?=$title;?></title>
    
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?=$css?>bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?=$css?>font-awesome.min.css">

    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?=$css?>smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?=$css?>smartadmin-skins.min.css">

    <link rel="icon" href="<?=$img;?>logo-o.png" type="image/x-icon">  
    <link rel="shortcut icon" href="<?=$img;?>logo-o.png" type="image/x-icon">
    
	<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?=$css?>demo.min.css">
	
		 
    <!-- #GOOGLE FONT 
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
	-->
	
    <!-- #APP SCREEN / ICONS -->
    <!-- Specifying a Webpage Icon for Web Clip 
    Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
    <link rel="apple-touch-icon" href="<?=$img;?>splash/sptouch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?=$img;?>splash/touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?=$img;?>splash/touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?=$img;?>splash/touch-icon-ipad-retina.png">

    <!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!-- Startup image for web apps -->
    <link rel="apple-touch-startup-image" href="<?=$img;?>splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
    <link rel="apple-touch-startup-image" href="<?=$img;?>splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
    <link rel="apple-touch-startup-image" href="<?=$img;?>splash/iphone.png" media="screen and (max-device-width: 320px)">

    <!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
    <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> -->
    <script>
        if (!window.jQuery) {
            document.write('<script src="<?=$js;?>libs/jquery-2.0.2.min.js"><\/script>');
        }
    </script>
    <!--<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>-->
    <script>
        if (!window.jQuery.ui) {
            document.write('<script src="<?=$js;?>libs/jquery-ui-1.10.3.min.js"><\/script>');
        }
    </script>
    
    <!--
    <script type="text/javascript" src="<?=$js;?>jquery-1.8.0.min.js"></script> 
    -->
    <script type="text/javascript" src="<?=$js;?>/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
    <script type="text/javascript" src="<?=$js;?>/plugin/fuelux/wizard/wizard.min.js"></script>
    
</head>

<body class="desktop-detected pace-done fixed-header">
    <header id="header">
        <div id="logo-group">
            <span id="logo"> <a href="<?=site_url('home')?>" title="Dashboard"><img src="<?=$img;?>logo-blacknwhite.png" alt="SmartAdmin"> </a></span>
        </div>
        
        <div class="pull-right">
            <div id="hide-menu" class="btn-header pull-right">
                <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
            </div>
            <div id="logout" class="btn-header transparent pull-right">
                <span> <a href="<?=site_url('logout')?>" title="Sign Out" data-action="userLogout" data-logout-msg="You can improve your security further after logging out by closing this opened browser"><i class="fa fa-sign-out"></i></a> </span>
            </div>
            <div id="search-mobile" class="btn-header transparent pull-right">
                <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
            </div>
            <form action="#" class="header-search pull-right">
                <input id="search-fld" type="text" name="param" placeholder="Find reports and more">
                <button type="submit">
                    <i class="fa fa-search"></i>
                </button>
                <a href="javascript:void(0);" id="cancel-search-js" title="Cancel Search"><i class="fa fa-times"></i></a>
            </form>
            <div id="fullscreen" class="btn-header transparent pull-right">
                <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
            </div>
        </div>

    </header>
    <aside id="left-panel">
        <div class="login-info">
            <span> 
                <a href="#" id="show-shortcut" data-action="toggleShortcut">
                    <img src="<?=$img;?>avatars/sunny.png" alt="me" class="online" /> 
                    <span>
                            <?=$name; ?>
                    </span>
                    <i class="fa fa-plus-square-o"></i>
                </a> 
            </span>
        </div>

    <?php $this->load->view('menu',$menu_role); ?>

    </aside>
    <div id="main" role="main">
        <div id="ribbon">

            <span class="ribbon-button-alignment"> 
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh">  </i>
                </span>
            </span>

        <?php  if (!empty($view_content)) $this->load->view($view_content,isset($content)?$content:''); ?>

        </div>
    </div>

    <!-- PAGE FOOTER -->
    <div class="page-footer" style="margin-top: 50px;">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <span class="txt-color-white">SmartAdmin WebApp © <?php echo date('Y'); ?></span>
            </div>
        </div>
    </div>
    <!-- END PAGE FOOTER -->

    <div id="shortcut">
        <ul>
            <li>
                   <a href="#" id="user-password" data-toggle="modal" class="jarvismetro-tile big-cubes bg-color-greenLight" data-target="#dataPasswordModal"> <span class="iconbox"> <i class="fa fa-lock fa-4x"></i> <span>Password </span> </span> </a>
            </li>
            <li>
                <a href="#" id="user-profile" data-toggle="modal" class="jarvismetro-tile big-cubes selected bg-color-red" data-target="#dataProfileModal"><span class="iconbox"> <i class="fa fa-user fa-4x"></i><span>My Profile </span> </span></a>
            </li>
        </ul>
    </div>

    <!-- Modal Profile -->
    <div class="modal fade" id="dataProfileModal" tabindex="-1" role="dialog" aria-labelledby="labelDataProfile">
    <div class="modal-dialog" role="document" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <!--button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title"><span id="labelDataProfile"></span> User Profile <span id="loading-data" style="display:none;margin-left: 10px;"><img src="<?=base_url('assets/img/loading.gif')?>" /></span></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="dataProfileModalURL" style="display: none;"></span>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="simpanProfileModal" data-profile-id="">Simpan</button>
            </div>
        </div>
    </div>
    </div>

    <!-- Modal Notifikasi -->
    <div class="modal fade" id="notifikasiProfileModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiUser">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="labelNotifikasiProfile">Notifikasi</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="statusNotifikasiProfile" style="display: none;"></span>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiProfileOK">OK</button>
            </div>
        </div>
    </div>
    </div>    
    
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">
                        <img src="<?=$img;?>logo.png" width="150" alt="SmartAdmin">
                    </h4>
                </div>
                <div class="modal-body no-padding">
                    <form id="login-form" class="smart-form">
                        <fieldset>
                            <section>
                                <div class="row">
                                    <label class="label col col-2">Username</label>
                                    <div class="col col-10">
                                        <label class="input"> <i class="icon-append fa fa-user"></i>
                                            <input type="email" name="email">
                                        </label>
                                    </div>
                                </div>
                            </section>
                            <section>
                                <div class="row">
                                    <label class="label col col-2">Password</label>
                                    <div class="col col-10">
                                        <label class="input"> <i class="icon-append fa fa-lock"></i>
                                            <input type="password" name="password">
                                        </label>
                                        <div class="note">
                                            <a href="javascript:void(0)">Forgot password?</a>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section>
                                <div class="row">
                                    <div class="col col-2"></div>
                                    <div class="col col-10">
                                        <label class="checkbox">
                                            <input type="checkbox" name="remember" checked="">
                                            <i></i>Keep me logged in
                                        </label>
                                    </div>
                                </div>
                            </section>
                        </fieldset>
                        <footer>
                            <button type="submit" class="btn btn-primary">
                                Sign in
                            </button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                Cancel
                            </button>
                        </footer>
                    </form>						
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Modal Password -->
    <div class="modal fade" id="dataPasswordModal" tabindex="-1" role="dialog" aria-labelledby="labelDataPassword">
    <div class="modal-dialog" role="document" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <!--button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title"><span id="labelDataPassword"></span> User Password <span id="loading-data" style="display:none;margin-left: 10px;"><img src="<?=base_url('assets/img/loading.gif')?>" /></span></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="dataPasswordModalURL" style="display: none;"></span>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="simpanPasswordModal" data-profile-id="">Simpan</button>
            </div>
        </div>
    </div>
    </div>

    <!-- Modal Notifikasi Password -->
    <div class="modal fade" id="notifikasiPasswordModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiUser">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="labelNotifikasiPassword">Notifikasi</h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <span id="statusNotifikasiPassword" style="display: none;"></span>
                    <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiPasswordOK">OK</button>
                </div>
            </div>
        </div>
    </div>

<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?=$js;?>plugin/pace/pace.min.js"></script>
    
<!-- IMPORTANT: APP CONFIG -->
<script src="<?=$js;?>app.config.js"></script>
<script src="<?=$js;?>plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
<script src="<?=$js;?>bootstrap/bootstrap.min.js"></script>
<script src="<?=$js;?>notification/SmartNotification.min.js"></script>
<!-- JARVIS WIDGETS -->
<script src="<?=$js;?>smartwidgets/jarvis.widget.min.js"></script>
<script src="<?=$js;?>plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
<script src="<?=$js;?>plugin/sparkline/jquery.sparkline.min.js"></script>
<script src="<?=$js;?>plugin/jquery-validate/jquery.validate.min.js"></script>
<script src="<?=$js;?>plugin/masked-input/jquery.maskedinput.min.js"></script>
<script src="<?=$js;?>plugin/select2/select2.min.js"></script>
<script src="<?=$js;?>plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
<script src="<?=$js;?>plugin/msie-fix/jquery.mb.browser.min.js"></script>
<script src="<?=$js;?>plugin/fastclick/fastclick.min.js"></script>    
<script src="<?=$js;?>plugin/clockpicker/clockpicker.min.js"></script>
<script src="<?=$js;?>plugin/jquery-form/jquery-form.min.js"></script>
<script src="<?=$js;?>plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?=$js;?>plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?=$js;?>plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?=$js;?>plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?=$js;?>plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script src="<?=$js;?>jquery.form.js"></script>
<script src="<?=$js;?>app.min.js"></script>
<script src="<?=$js;?>speech/voicecommand.min.js"></script>

<!-- 
<script src="<?=$js;?>demo.min.js"></script>
-->

<!--[if IE 8]>

<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

<![endif]-->

<script type="text/javascript">

$(document).ready(function() {

    pageSetUp();

    /* BASIC ;*/
    var responsiveHelper_dt_basic = undefined;
    var responsiveHelper_datatable_fixed_column = undefined;
    var responsiveHelper_datatable_col_reorder = undefined;
    var responsiveHelper_datatable_tabletools = undefined;

    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };

    $('#dt_basic').dataTable({
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
                "t"+
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        "autoWidth" : false,
        "preDrawCallback" : function() {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_dt_basic) {
                responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
            }
        },
        "rowCallback" : function(nRow) {
            responsiveHelper_dt_basic.createExpandIcon(nRow);
        },
        "drawCallback" : function(oSettings) {
            responsiveHelper_dt_basic.respond();
        }
    });
    /* END BASIC */

    /* COLUMN SHOW - HIDE */
    $('#datatable_col_reorder').dataTable({
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
                "t"+
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "autoWidth" : true,
        "preDrawCallback" : function() {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_datatable_col_reorder) {
                responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
            }
        },
        "rowCallback" : function(nRow) {
            responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
        },
        "drawCallback" : function(oSettings) {
            responsiveHelper_datatable_col_reorder.respond();
        }			
    });
    /* END COLUMN SHOW - HIDE */

});

</script>

<!-- Your GOOGLE ANALYTICS CODE Below -->
<script type="text/javascript">
    var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
        _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();

</script>

        
<script>

// Home Profile
$('#user-profile').click(function() {
    var url = '<?=site_url('user_management/profile')?>';
    $('#labelDataProfile').text('');
    $('#dataProfileModal .modal-body').html('');
    $('#dataProfileModal .modal-body').load(url);
    $('#dataProfileModalURL').text(url);
});
$('#simpanProfileModal').click(function(){
    if ( $('#name_profile').val()=='' ){
        $('#name_profile').focus();
        return false;
    }
    if ( $('#no_identitas_profile').val()=='' ){
        $('#no_identitas_profile').focus();
        return false;
    }
    if ( !$.isNumeric( $('#no_identitas_profile').val() ) ){
        showNotifikasi('notifikasiProfileModal','No Identitas harus angka !');
        $('#no_identitas_profile').focus();
        return false;
    }
    if ( $('#phone_profile').val()=='' ){
        $('#phone_profile').focus();
        return false;
    }
    if ( !$.isNumeric( $('#phone_profile').val() ) ){
        showNotifikasi('notifikasiProfileModal','No Telepon harus angka !');
        $('#phone_profile').focus();
        return false;
    }
    if ( $('#email_profile').val()=='' ){
        $('#email_profile').focus();
        return false;
    }

    if ( $('#username_profile').val()=='' ){
        $('#username_profile').focus();
        return false;
    }
    var url = $('#dataProfileModalURL').text();        
    $(this).attr('data-loading-text','Proses Simpan...').button('loading');         
    $.post(url,$('#form-profile').serialize(),
    function(result){
        var result = eval('('+result+')');
        if ( result.success ) $('#dataProfileModal').modal('hide');  
        showNotifikasi('notifikasiProfileModal',result.Msg); 
        $('#statusNotifikasiProfile').val(result.success);
    });   
});
$("#buttonNotifikasiProfileOK").click(function() {
    if ( $('#statusNotifikasiProfile').val() ) {
        window.location.href = '<?=site_url('home')?>';
    } else {
        $('#simpanProfileModal').button('reset'); 
    }
    });        
// End Home Profile

// Home Password
$('#user-password').click(function() {
    var url = '<?=site_url('user_management/password')?>';
    $('#labelDataPassword').text('');
    $('#dataPasswordModal .modal-body').html('');
    $('#dataPasswordModal .modal-body').load(url);
    $('#dataPasswordModalURL').text(url);
});
$('#simpanPasswordModal').click(function(){        
    if ( $('#password_old').val()=='' ){
        $('#password_old').focus();
        return false;
    }
    if ( $('#password_new').val()=='' ){
        $('#password_new').focus();
        return false;
    } 
    if ( $('#password_konfirm').val()=='' ){
        $('#password_konfirm').focus();
        return false;
    }
    if ( $('#password_konfirm').val()!=$('#password_new').val() ){
        showNotifikasi('notifikasiPasswordModal','Password dan Konfirmasi Password tidak sama !');
        $('#password_konfirm').focus();
        return false;
    }
    var url = $('#dataPasswordModalURL').text();        
    $(this).attr('data-loading-text','Proses Simpan...').button('loading');         
    $.post(url,$('#form-password').serialize(),
    function(result){
        var result = eval('('+result+')');
        if ( result.success ) $('#dataPasswordModal').modal('hide');  
        showNotifikasi('notifikasiPasswordModal',result.Msg); 
        $('#statusNotifikasiPassword').val(result.success);
    });   
});
$("#buttonNotifikasiPasswordOK").click(function() {
    if ( $('#statusNotifikasiPassword').val() ) {
        window.location.href = '<?=site_url('logout')?>';
    } else {
        $('#simpanPasswordModal').button('reset'); 
    }
    });        
// End Home Password    

function showNotifikasi(idModal,pesan){
    $('#'+idModal+' .modal-body').html(pesan);
    $('#'+idModal).modal('show');
}


function smartNotifikasi(result){
    if(result.success){
        $.SmartMessageBox({
            title : "Notifikasi Success!",
            content : result.Msg,
            buttons : '[OK]'
        }, function(ButtonPressed) {
            if (ButtonPressed === "OK") {
                window.location.reload(); 
            }

        });
    }else{
        alertNotifikasi(result.Msg);
    }
}

function successNotifikasi(pesan){
    $.smallBox({
        title : "Notifikasi Success",
        content : pesan,
        color : "#659265",
        iconSmall : "fa fa-check fa-2x fadeInRight animated",
        timeout : 2000
    });
    setTimeout(function(){
        window.location.reload(1);
    }, 2000);

}

function alertNotifikasi(pesan){
    $.smallBox({
        title : "Alert",
        content : pesan,
        color : "#C46A69",
        iconSmall : "fa fa-times fa-2x fadeInRight animated",
        timeout : 3000
    });
}

</script>
        
</body>

</html>