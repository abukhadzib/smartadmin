<ol class="breadcrumb">
	<li>User Management </li><li>MainMenu</li>
</ol>

</div>

<div id="content">
    
<section class="content">
<div class="right_col" role="main">
	<div class="">

<div class="row">
	<div class="col-xs-12">
		<h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-user"></i> User Management <span> > Menu </span></h3>
	</div>
    <div class="col-xs-12">
        <div class="pull-right" style="margin-bottom: 14px;">
            <button type="button" class="btn btn-primary" id="tambahMenu" data-toggle="modal" data-target="#dataMenuModal"><i class="fa fa-plus"></i> Tambah Data Menu</button>
        </div>                
    </div> 
    <div class="col-lg-12">
    <div class="table-responsive" style="margin-bottom: auto;  overflow: auto;">	
       <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-xxx" >
            <header >
                <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                <h2>Table Menu </h2>
            </header>
            <!-- widget div-->
            <div>
                <div class="widget-body no-padding">	
                    <table id="dt_basic" class="table table-striped table-bordered table-hover" style="margin-bottom: auto; background: #fff; overflow: auto;">
                        <thead>
                            <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Tipe</th>
                            <th>Header</th>
                            <th>Url</th>
                            <th>Icon</th>
                            <th>Sort</th>
                            <th style="width: 5%;">Action</th>
                                    </tr>
                        </thead>
                        <tbody>
                                    <?php 
                            $no = 1; 
                            foreach ($content as $row){ 
                        ?>
                            <tr>
                                <td data-title="#"><?=($page*$perpage)+$no;?></td>
                                <td data-title="Name"><?=$row->name;?>&nbsp;</td>
                                <td data-title="Tipe"><?=$row->tipe;?>&nbsp;</td>
                                <td data-title="Header"><?=$row->header;?>&nbsp;</td>
                                <td data-title="Url"><?=$row->url;?>&nbsp;</td>
                                <td data-title="Icon"><?=$row->icon;?>&nbsp;</td>
                                <td data-title="Sort"><?=$row->sort;?>&nbsp;</td>
                                <td  data-title="Action">
                                <a class="editMenu" href="#" title="Edit" data-menu-name="<?=$row->name;?>" data-menu-id="<?=$row->id;?>" data-toggle="modal" data-target="#dataMenuModal"><i class="fa fa-edit"></i></a>&nbsp;
                                <a class="deleteMenu" href=#" title="Remove" data-menu-name="<?=$row->name;?>" data-menu-id="<?=$row->id;?>" data-toggle="modal" data-target="#deleteMenuModal"><i class="glyphicon glyphicon-remove"></i></a>
                                </td>
                            </tr>                         
                        <?php 
                            $no++; 
                            }
                        ?>
                            </tbody>
                    </table>               
                </div>	
            </div>	
       </div>
        <div align="right" style=" margin-top: -25px;">
            <?//=$pagination;?>
        </div>           
    </div>                      
</div>
    
</div>                       
</div>
</div>
    </section>
    
<!-- Modal Hapus -->
<div class="modal fade" id="deleteMenuModal" tabindex="-1" role="dialog" aria-labelledby="labelDeleteMenu">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title" id="labelDeleteMenu">Hapus Data Menu</h4>
            </div>
            <div class="modal-body">  
                <i class="fa fa-lg fa-fw fa-warning"></i>Yakin mau hapus data Menu <span id="namaMenuHapus"></span> ?                  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="deleteMenuModalYes" data-menu-id="">Ya</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Data Tambah dan Edit -->
<div class="modal fade" id="dataMenuModal" tabindex="-1" role="dialog" aria-labelledby="labelDataMenu">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title"><span id="labelDataMenu"></span> Data Menu <span id="loading-data" style="display:none;margin-left: 10px;"><img src="<?=base_url('assets/img/loading.gif')?>" /></span></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="dataMenuModalURL" style="display: none;"></span>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="simpanMenuModal" data-menu-id="">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Notifikasi -->
<div class="modal fade" id="notifikasiMenuModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiMenu">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="labelNotifikasiMenu">Notifikasi</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="statusNotifikasiMenu" style="display: none;"></span>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiMenuOK">OK</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    // User
    $('#tambahMenu').click(function() {
        var url = '<?=site_url('user_management/mainmenu_add')?>';
        $('#labelDataMenu').text('Tambah');  
        $('#dataMenuModal .modal-body').html('');
        $('#dataMenuModal .modal-body').load(url);
        $('#dataMenuModalURL').text(url);
    });
    $('.editMenu').click(function() {
        var id = $(this).attr('data-menu-id');
		var url = '<?=site_url('user_management/mainmenu_edit')?>'+'/'+id;
        $('#labelDataMenu').text('Rubah');
        $('#dataMenuModal .modal-body').html('');
        $('#dataMenuModal .modal-body').load(url);
        $('#dataMenuModalURL').text(url);
    });
    $(".deleteMenu").click(function() {
        var name = $(this).attr('data-menu-name');
        var id = $(this).attr('data-menu-id');
		$('#deleteMenuModalYes').attr('data-menu-id',id);
        $('#namaMenuHapus').html('<b>'+name+'</b>');
	});    
    $('#deleteMenuModalYes').click(function() {
        var id  = $(this).attr('data-menu-id');
        var url = '<?=site_url('user_management/mainmenu_delete')?>'+'/'+id;        
        $.post(url,{action:'delete',id:id},
        function(result){
            var result = eval('('+result+')');
            $('#deleteMenuModal').modal('hide');
            showNotifikasi('notifikasiMenuModal',result.Msg);
            $('#statusNotifikasiMenu').val(result.success);
        });        
	});
        
    function isValidEmailAddress(emailAddress){
        var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
        return pattern.test(emailAddress);
    }
    
    $('#simpanMenuModal').click(function(){
        
        if ( $('#name').val()=='' ){
            $('#name').focus();
            return false;
        }
        if ( $('#tipe').val()=='' ){
            $('#tipe').focus();
            return false;
        }
        
        if ( $('#header').val()=='' ){
            $('#header').focus();
            return false;
        }
        /*
        if ( $('#url').val()=='' ){
            $('#url').focus();
            return false;
        }
       */
        if ( $('#icon').val()=='' ){
            $('#icon').focus();
            return false;
        }
        if ( $('#sort').val()=='' ){
            $('#sort').focus();
            return false;
        }
        
        var url = $('#dataMenuModalURL').text();        
        $(this).attr('data-loading-text','Proses Simpan...').button('loading');         
        $.post(url,$('#form-menu').serialize(),
        function(result){
            var result = eval('('+result+')');
            if ( result.success ) $('#dataMenuModal').modal('hide');  
            //alert(result.Msg);
           showNotifikasi('notifikasiMenuModal',result.Msg); 
           $('#statusNotifikasiMenu').val(result.success);
        });   
    });
    $("#buttonNotifikasiMenuOK").click(function() {
        if ( $('#statusNotifikasiMenu').val() ) {
            window.location.reload();
        } else {
            $('#simpanMenuModal').button('reset'); 
        }
	});        
        
        
    function showNotifikasi(idModal,pesan){
        $('#'+idModal+' .modal-body').html(pesan);
        $('#'+idModal).modal('show');
        $('body .modal-backdrop').hide();
    }
    // End User
</script>