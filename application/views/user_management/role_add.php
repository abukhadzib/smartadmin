<br />
<div class="row">
	<div class="col-lg-12">		 
		<form enctype="multipart/form-data" id="form-role" class="form-horizontal" method="post">
            <div class="form-group">
                <label for="name" class="col-xs-2 control-label">Role</label>
                <div class="col-xs-6">
                    <input type="text" class="form-control" id="role_name" name="role_name" placeholder="Nama Role" autofocus>
                </div>
                <div class="col-xs-4">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="is_admin"> Is Admin
                        </label>
                    </div>
                </div>
            </div> 
              
            <div class="row">												
            	<table class="table table-bordered table-hover">
            		<thead>
                        <tr>
            				<th style="text-align:center;">Menu</th>
            				<!--<th style="text-align:center;">#</th>-->
                            <th style="text-align:center;">Create</th>
                            <th style="text-align:center;">Read</th>
                            <th style="text-align:center;">Update</th>
                            <th style="text-align:center;">Delete</th>
            			</tr>
            		</thead>
            		<tbody>
            			<?php foreach ($menu as $row){ ?>
                        <tr<?=$row->tipe=='H' ? ' style="background-color:#f0f0f0;"' : ''?>>
                            <td><?=($row->tipe=='H' ? '' : '&nbsp;&nbsp;&nbsp;&nbsp;') . $row->name;?></td>
                            <!--<td style="text-align:center;">    
                                <? if($row->tipe!='H') { ?>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="checkAll">
                                    </label>
                                </div>
                                <? } ?>
                            </td>-->
                            <td style="text-align:center;">    
                               <? if($row->tipe!='H') { ?>
                               <section class="col col-5">
                                    <label class="toggle state-success">
                                        <input type="checkbox" name="create[]" value="<?=$row->id;?>">
                                        <i data-swchon-text="ON" data-swchoff-text="OFF"></i>
                                    </label>
                                </section>
                                <? } ?>
                            </td>
                            <td style="text-align:center;">    
                                <? if($row->tipe!='H') { ?>
                                <section class="col col-5">
                                    <label class="toggle state-success">
                                        <input type="checkbox" name="read[]" value="<?=$row->id;?>">
                                        <i data-swchon-text="ON" data-swchoff-text="OFF"></i>
                                    </label>
                                </section>
                                <? } ?>
                            </td>
                            <td style="text-align:center;">    
                               <? if($row->tipe!='H') { ?>
                                <section class="col col-5">
                                    <label class="toggle state-success">
                                        <input type="checkbox" name="update[]" value="<?=$row->id;?>">
                                        <i data-swchon-text="ON" data-swchoff-text="OFF"></i>
                                    </label>
                                </section>
                                <? } ?>
                            </td>
                            <td style="text-align:center;">    
                                <? if($row->tipe!='H') { ?>
                                <section class="col col-5">
                                    <label class="toggle state-success">
                                        <input type="checkbox" name="delete[]" value="<?=$row->id;?>">
                                        <i data-swchon-text="ON" data-swchoff-text="OFF"></i>
                                    </label>
                                </section>
                                <? } ?>
                            </td>
                        </tr>                         
                        <?php } ?>
            		</tbody>
            	</table>        	
            </div>
            </form>      
	</div>  
</div>
