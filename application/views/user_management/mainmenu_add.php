<br />
<div class="row">
	<div class="col-lg-8">
        <form enctype="multipart/form-data" id="form-menu" class="form-horizontal" method="post" >
			
            <div class="form-group">
                <label for="name" class="col-xs-4 control-label">Nama *</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control has-feedback-left" required="required" id="name" name="name" placeholder="Nama" autofocus>
                  
                </div>
            </div>
            <div class="form-group">
                <label for="Tipe" class="col-xs-4 control-label">Tipe *</label>
                <div class="col-xs-7">
                    <select name="tipe" required="required" class="form-control" id="tipe">
                        <option value="" selected=""> --Tipe--</option>
                        <option value="H">Menu</option>
                        <option value="S">Submenu</option>
                       <option value="SS">Sub Submenu</option>
                        </select>
                </div>
            </div> 
            <!--
            <div class="form-group">
                <label for="Tipe" class="col-xs-4 control-label">Tipe *</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control" required="required" id="tipe" name="tipe" placeholder="Tipe">
                </div>
            </div> 
            
            <div class="form-group">
                <label for="address1" class="col-xs-4 control-label">Header</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control" id="header" name="header" placeholder="Header">
                </div>
            </div>-->
            <div class="form-group">
                <label for="Header" class="col-xs-4 control-label">Header *</label>
                <div class="col-xs-7">
                    <select name="header" required="required" class="form-control" id="header">
                        <option value="0" selected=""> --Header--</option>
                        <? if ($menu) { foreach($menu as $row_menu) { ?>
                        <option value="<?=$row_menu->id?>"><?=$row_menu->name?></option>
                        <? } } ?>
                        </select>
                </div>
            </div> 
            <div class="form-group">
                <label for="Url" class="col-xs-4 control-label">Url *</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control has-feedback-left" id="url" name="url" placeholder="Url">
                   
                </div>
            </div> 
            <div class="form-group">               
                <label for="Icon" class="col-xs-4 control-label">Icon *</label>
                <div class="col-xs-7">
                    <!--<input type="email" class="form-control" id="email" name="email" placeholder="Email">-->
                    <input type="text" class="form-control has-feedback-left" name="icon" id="icon" placeholder="Icon">
                   
                </div>
            </div> 
            <div class="form-group">
                <label for="Sort" class="col-xs-4 control-label">Sort *</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control" id="sort" name="sort" placeholder="Sort">
                </div>
            </div> 
            
            <em>*) Wajib diisi.</em>
		</form>
	</div>  
</div>
