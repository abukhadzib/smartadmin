<br />
<div class="row">
	<div class="col-lg-8">
        <form enctype="multipart/form-data" id="form-user" class="form-horizontal" method="post" >
			<div class="form-group">
                <label for="name" class="col-xs-4 control-label">Role *</label>
                <div class="col-xs-7">
                    <select name="role" required="required" class="form-control" id="role">
						<option value="" selected="" disabled="" > --Role--</option>
                        <? if ($role) { foreach($role as $row_role) { ?>
						<option value="<?=$row_role->id?>"><?=$row_role->name?></option>
                        <? } } ?>
					</select>
                </div>
            </div> 
            <div class="form-group">
                <label for="name" class="col-xs-4 control-label">Nama *</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control has-feedback-left" required="required" id="name" name="name" placeholder="Nama" autofocus>
                  
                </div>
            </div>
            <div class="form-group">
                <label for="no_identitas" class="col-xs-4 control-label">No Identitas *</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control" required="required" id="no_identitas" name="no_identitas" placeholder="No Identitas">
                </div>
            </div> 
            <div class="form-group">
                <label for="address1" class="col-xs-4 control-label">Alamat</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control" id="address1" name="address1" placeholder="Alamat">
                </div>
            </div>
            <div class="form-group">
                <label for="phone" class="col-xs-4 control-label">No Telpon *</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control has-feedback-left" id="phone" name="phone" placeholder="No Telpon">
                   
                </div>
            </div> 
            <div class="form-group">               
                <label for="email" class="col-xs-4 control-label">Email *</label>
                <div class="col-xs-7">
                    <!--<input type="email" class="form-control" id="email" name="email" placeholder="Email">-->
                    <input type="email" class="form-control has-feedback-left" name="email" id="email" placeholder="Email">
                   
                </div>
            </div> 
            <div class="form-group">
                <label for="username" class="col-xs-4 control-label">Username *</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Username">
                </div>
            </div> 
            <div class="form-group">
                <label for="password" class="col-xs-4 control-label">Password *</label>
                <div class="col-xs-7">
                    <input type="password" class="form-control" id="passwordAdd" name="password" placeholder="Password">
                </div>
            </div> 
            <div class="form-group">
                <label for="passwordConfirm" class="col-xs-4 control-label">Konfirmasi *</label>
                <div class="col-xs-7">
                    <input type="password" class="form-control" id="passwordConfirmAdd" name="passwordConfirm" placeholder="Konfirmasi Password">
                </div>
            </div>  
            <em>*) Wajib diisi.</em>
		</form>
	</div>  
</div>
