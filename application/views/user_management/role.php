<ol class="breadcrumb">
    <li>User Management </li><li>Role</li>
</ol>

</div>

<div id="content">
<section id="widget-grid" class="">
    <div class="right_col" role="main" id="tbRole">
        <div class="col-xs-12">
            <h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-user"></i> User Management <span> > Role </span></h3>
        </div>
        <div class="col-xs-12">
            <div class="pull-right" style="margin-bottom: 14px;">
                <button type="button" class="btn btn-primary" id="tambahRole" data-toggle="modal" data-target="#dataRoleModal"><i class="fa fa-plus"></i> Tambah Data Role</button>
            </div>                
        </div>    
        <div class="col-xs-12">     
           <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0">
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Table Role </h2>
                </header>
                <div>
                    <div class="widget-body no-padding">	
                        <table id="datatable_col_reorder" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th style="text-align: center;width: 4%;">#</th>
                                    <th style="text-align: center;width: 20%;">Name</th>
                                    <th style="text-align: center;width: 4%;">Admin</th>
                                    <th style="text-align: center;width: 6%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                    <?php 
                            $no = 1;
                            foreach ($content as $row){ 
                                ?>
                                    <tr>
                                        <td><?=($page*$perpage)+$no;?></td>
                                        <td><?=$row->name;?></td>
                                        <td><?=$row->is_admin;?></td>
                                        <td style="text-align: center;">
                                            <a class="editRole" href="#" title="Edit" data-role-name="<?=$row->name;?>" data-role-id="<?=$row->id;?>" data-toggle="modal" data-target="#dataRoleModal"><i class="fa fa-edit"></i></a>&nbsp;
                                            <a class="deleteRole" href="#" title="Remove" data-role-name="<?=$row->name;?>" data-role-id="<?=$row->id;?>" data-toggle="modal" data-target="#deleteRoleModal"><i class="glyphicon glyphicon-remove"></i></a>
                                        </td>
                                    </tr>                         
                                <?php 
                                    $no++; 
                                    }
                                ?>
                            </tbody>
                        </table> 
                    </div>	
                </div>	
           </div>
            <div align="right" style=" margin-top: -25px;">
                <?//=$pagination;?>
            </div>           
        </div>    
    </div>                       
</section>
    
 <section id="widget-grid" class="">
    <div class="right_col" role="main" id="addRole" style="display: none;">
        <div class="row" >
            <div class="col-xs-12">
                <h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-user"></i> User Management <span> > Role </span></h3>
            </div>
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Form Add Role</h2>				
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <form id="form-user" class="smart-form" novalidate="novalidate">
                                <fieldset>
                                    <div class="row">
                                        <div class="col-lg-12">	
                                            <fieldset>
                                                <div class="row">
                                                    <section class="col col-4">
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Role</label>
                                                            <div class="col-md-8">
                                                                <input class="form-control" id="role_name" placeholder="Nama Role" type="text" required autofocus>
                                                            </div>
                                                        </div>
                                                    </section>
                                                    <section class="col col-2">
                                                        <label class="toggle state-success">
                                                            <input type="checkbox" name="is_admin">
                                                            <i data-swchon-text="ON" data-swchoff-text="OFF"></i>Is Admin</label>
                                                    </section>
                                                </div>
                                            </fieldset>
                                                <div class="widget-body no-padding">	
                                                    <table id="datatable_col_reorder" class="table table-striped table-bordered table-hover" width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th style="text-align:center;">Menu</th>
                                                                <th style="text-align:center;">Create</th>
                                                                <th style="text-align:center;">Read</th>
                                                                <th style="text-align:center;">Update</th>
                                                                <th style="text-align:center;">Delete</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php foreach ($menu as $row){ ?>
                                                            <tr<?=$row->tipe=='H' ? ' style="background-color:#f0f0f0;"' : ''?>>
                                                                <td><?=($row->tipe=='H' ? '' : '&nbsp;&nbsp;&nbsp;') . $row->name;?></td>
                                                                <td style="text-align:center;">    
                                                                   <? if($row->tipe!='H') { ?>
                                                                   <section class="col col-4">
                                                                        <label class="toggle state-success">
                                                                            <input type="checkbox" name="create[]" value="<?=$row->id;?>">
                                                                            <i data-swchon-text="ON" data-swchoff-text="OFF"></i>
                                                                        </label>
                                                                    </section>
                                                                    <? } ?>
                                                                </td>
                                                                <td style="text-align:center;">    
                                                                    <? if($row->tipe!='H') { ?>
                                                                    <section class="col col-5">
                                                                        <label class="toggle state-success">
                                                                            <input type="checkbox" name="read[]" value="<?=$row->id;?>">
                                                                            <i data-swchon-text="ON" data-swchoff-text="OFF"></i>
                                                                        </label>
                                                                    </section>
                                                                    <? } ?>
                                                                </td>
                                                                <td style="text-align:center;">    
                                                                   <? if($row->tipe!='H') { ?>
                                                                    <section class="col col-5">
                                                                        <label class="toggle state-success">
                                                                            <input type="checkbox" name="update[]" value="<?=$row->id;?>">
                                                                            <i data-swchon-text="ON" data-swchoff-text="OFF"></i>
                                                                        </label>
                                                                    </section>
                                                                    <? } ?>
                                                                </td>
                                                                <td style="text-align:center;">    
                                                                    <? if($row->tipe!='H') { ?>
                                                                    <section class="col col-5">
                                                                        <label class="toggle state-success">
                                                                            <input type="checkbox" name="delete[]" value="<?=$row->id;?>">
                                                                            <i data-swchon-text="ON" data-swchoff-text="OFF"></i>
                                                                        </label>
                                                                    </section>
                                                                    <? } ?>
                                                                </td>
                                                            </tr>                         
                                                            <?php } ?>
                                                            </tbody>
                                                    </table>        	
                                                </div>  
                                        </div>  
                                    </div>
                                </fieldset>
                                <footer>
                                    <button type="button" class="btn btn-default" id="reset" data-dismiss="modal">Batal</button>
                                    <button type="button" class="btn btn-primary" id="simpanUserModal" data-user-id="">Simpan</button>
                                </footer>
                            </form>
                        </div>
                    </div>
                </div>
            </article>
        </div>                       
    </div>
</section>
    
<!-- Modal Hapus -->
<div class="modal fade" id="deleteRoleModal" tabindex="-1" role="dialog" aria-labelledby="labelDeleteRole">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title" id="labelDeleteRole">Hapus Data Role</h4>
            </div>
            <div class="modal-body">  
                <i class="fa fa-lg fa-fw fa-warning"></i>Yakin mau hapus data Role <span id="namaRoleHapus"></span> ?                  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="deleteRoleModalYes" data-role-id="">Ya</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Data Tambah dan Edit -->
<div class="modal fade" id="dataRoleModal1" tabindex="-1" role="dialog" aria-labelledby="labelDataRole">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title"><span id="labelDataRole"></span> Data Role <span id="loading-data" style="display:none;margin-left: 10px;"><img src="<?=base_url('assets/img/loading.gif')?>" /></span></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="dataRoleModalURL" style="display: none;"></span>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="simpanRoleModal" data-role-id="">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Notifikasi -->
<div class="modal fade" id="notifikasiRoleModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiRole">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="labelNotifikasiRole">Notifikasi</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="statusNotifikasiRole" style="display: none;"></span>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiRoleOK">OK</button>
            </div>
        </div>
    </div>
</div>


<script>
    $("#tambahRole").click(function() {  
    $("#tbRole").hide();
    $("#addRole").show();
});

$("#reset").click(function() {  
    $("#tbRole").show();
    $("#addRole").hide();
});


    /*
    $('#tambahRole').click(function() {
        var url = '<?=site_url('user_management/role_add')?>';
        $('#labelDataRole').text('Tambah');  
        $('#dataRoleModal .modal-body').html('');
        $('#dataRoleModal .modal-body').load(url);
        $('#dataRoleModalURL').text(url);
    });
    */
    $('.editRole').click(function() {
        var id = $(this).attr('data-role-id');
		var url = '<?=site_url('user_management/role_edit')?>'+'/'+id;
        $('#labelDataRole').text('Rubah');
        $('#dataRoleModal .modal-body').html('');
        $('#dataRoleModal .modal-body').load(url);
        $('#dataRoleModalURL').text(url);
    });
    $(".deleteRole").click(function() {
        var name = $(this).attr('data-role-name');
        var id = $(this).attr('data-role-id');
		$('#deleteRoleModalYes').attr('data-role-id',id);
        $('#namaRoleHapus').html('<b>'+name+'</b>');
	});    
    $('#deleteRoleModalYes').click(function() {
        var id  = $(this).attr('data-role-id');
        var url = '<?=site_url('user_management/role_delete')?>'+'/'+id;        
        $.post(url,{action:'delete',id:id},
        function(result){
            var result = eval('('+result+')');
            $('#deleteRoleModal').modal('hide');
            showNotifikasi('notifikasiRoleModal',result.Msg);
            $('#statusNotifikasiRole').val(result.success);
        });        
	});
    $('body').on('click', '.checkRead', function(){
        if($(this).prop('checked')){
            $(this).parents('tr').find('.checkRole').removeAttr('disabled');
            $(this).parents('tr').find('.checkRole').not('.readCheck').not('.createCheck').prop('checked', true);
        }else{
            $(this).parents('tr').find('.checkRole').not('.readCheck').not('.createCheck').attr('disabled', 'disabled');
            $(this).parents('tr').find('.checkRole').not('.readCheck').not('.createCheck').prop('checked', false);
        } 
    });
    $('body').on('click', '.checkAll', function(){
        if ($(this).prop('checked')) {
            $(this).parents('tr').find('.checkRole').removeAttr('disabled');
            $(this).parents('tr').find('.checkRole').prop('checked', true);
        }else{
            $(this).parents('tr').find('.checkRole').prop('checked',false);
            $(this).parents('tr').find('.checkRole').not('.readCheck').not('.createCheck').attr('disabled', 'disabled');
        }
    });
    $('#simpanRoleModal').click(function(){
        if ( $('#role_name').val()=='' ){
            $('#role_name').focus();
            return false;
        }
        var url = $('#dataRoleModalURL').text();        
        $(this).attr('data-loading-text','Proses Simpan...').button('loading');         
        $.post(url,$('#form-role').serialize(),
        function(result){
            var result = eval('('+result+')');
            $('#dataRoleModal').modal('hide');
            showNotifikasi('notifikasiRoleModal',result.Msg);
            $('#statusNotifikasiRole').val(result.success);
        });   
    });
    $("#buttonNotifikasiRoleOK").click(function() {
        if ( $('#statusNotifikasiRole').val() ) {
            window.location.reload();
        } else {
            $('#simpanRoleModal').button('reset'); 
        }
	});        
    // End Role
    
    
     function showNotifikasi(idModal,pesan){
        $('#'+idModal+' .modal-body').html(pesan);
        $('#'+idModal).modal('show');
        $('body .modal-backdrop').hide();
    }
    
    
</script>