<ol class="breadcrumb">
    <li>User Management </li><li>User</li>
</ol>

</div>

<div id="content">

<section id="widget-grid" class="" >
    <div class="right_col" role="main" id="tbUser">
        <div class="col-xs-12">
            <h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-user"></i> User Management <span> > User </span></h3>
        </div>
        <div class="col-xs-12">
            <div class="pull-right" style="margin-bottom: 14px;">
                <button type="button" class="btn btn-primary" id="tambahUser" data-toggle="modal" data-target="#dataUserModal"><i class="fa  fa-plus-square-o"> Add User</i></button>
            </div>                
        </div>    
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 45px;">     
            <div class="jarviswidget jarviswidget-color-blueDark" style="margin-bottom: 75px;" id="wid-id-0" data-widget-editbutton="false">
                <header>
                   <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                   <h2>Table User </h2>
               </header>
                <div>
                    <div class="widget-body no-padding">	
                        <table id="datatable_col_reorder" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th data-hide="phone">#</th>
                                    <th data-class="expand">Name</th>
                                    <th data-hide="phone">Username</th>
                                    <th>Email</th>
                                    <th data-hide="phone">Role</th>
                                    <th style="width: 5%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no = 1; 
                                foreach ($content as $row){ 
                                ?>
                                <tr>
                                    <td><?=($page*$perpage)+$no;?></td>
                                    <td><?=$row->name;?></td>
                                    <td><?=$row->username;?></td>
                                    <td><?=$row->email;?></td>
                                    <td><?=$row->role_name;?></td>
                                    <td>
                                    <a class="editUser" href="#" title="Edit" data-user-name="<?=$row->name;?>" data-user-id="<?=$row->id;?>" data-toggle="modal" data-target="#dataUserModal"><i class="fa fa-edit"></i></a>&nbsp;
                                    <a class="deleteUser" href=#" title="Remove" data-user-name="<?=$row->name;?>" data-user-id="<?=$row->id;?>" data-toggle="modal" data-target="#deleteUserModall"><i class="glyphicon glyphicon-remove"></i></a>
                                    </td>
                                </tr>                         
                            <?php 
                                $no++; 
                                }
                            ?>
                            </tbody>
                        </table> 
                    </div>			
                </div>  
            </div>	
            <div align="right" style=" margin-top: -25px;">
                <?//=$pagination;?>
            </div>           
        </div>   
    </div>
</section>
    
<section id="widget-grid" class="">
    <div class="right_col" role="main" id="addUser" style="display: none;">
        <div class="row" >
            <div class="col-xs-12">
                <h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-user"></i> User Management <span> > User </span></h3>
            </div>
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Form Add User</h2>				
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <form id="form-user" class="smart-form" novalidate="novalidate">
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-4">
                                            <label class="label">Role</label>
                                            <label class="select">
                                                <select name="role" id="role">
                                                    <option value="" selected="" disabled="" > Role User</option>
                                                    <? if ($role) { foreach($role as $row_role) { ?>
                                                    <option value="<?=$row_role->id?>"><?=$row_role->name?></option>
                                                    <? } } ?>
                                                </select> <i></i> 
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Name</label>
                                            <label class="input"> <i class="icon-prepend fa fa-user"></i>
                                                <input type="text" id="name" name="name" placeholder="Nama" required>
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">No Identitas</label>
                                            <label class="input"> <i class="icon-prepend fa fa-credit-card "></i>
                                                <input type="text" id="no_identitas" name="no_identitas" placeholder="No Identitas">
                                            </label>
                                        </section>
                                    </div>
                                    <div class="row">
                                        <section class="col col-4">
                                            <label class="label">Email</label>
                                            <label class="input"> <i class="icon-prepend fa fa-envelope-o"></i>
                                                <input type="email" name="email" id="email" placeholder="E-mail">
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Telepon</label>
                                            <label class="input"> <i class="icon-prepend fa fa-phone"></i>
                                                <input type="tel" id="phone" name="phone" placeholder="Phone" >
                                            </label>
                                        </section>
                                    </div>
                                    <section>
                                        <label class="label">Alamat</label>
                                        <label class="textarea"> 										
                                            <textarea rows="3" id="address1" name="address1" placeholder="Address"></textarea> 
                                        </label>
                                    </section>
                                    <div class="row">
                                        <section class="col col-4">
                                            <label class="label">Username</label>
                                            <label class="input"><i class="icon-prepend fa fa-user"></i>
                                                <input type="text" id="username" name="username" placeholder="Username">
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Password</label>
                                            <label class="input"> <i class="icon-prepend fa fa-key"></i>
                                                <input type="password" id="passwordAdd" name="password" placeholder="Password">
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Password Confirm</label>
                                            <label class="input"> <i class="icon-prepend fa fa-key"></i>
                                                <input type="password" id="passwordConfirmAdd" name="passwordConfirm" placeholder="Konfirmasi Password">
                                            </label>
                                        </section>
                                    </div>
                                </fieldset>
                                <footer>
                                    <button type="button" class="btn btn-default" id="reset" data-dismiss="modal">Batal</button>
                                    <button type="button" class="btn btn-primary" id="simpanUserModal" data-user-id="">Simpan</button>
                                </footer>
                            </form>
                        </div>
                    </div>
                </div>
            </article>
        </div>                       
    </div>
</section>
    
<script type="text/javascript">

$("#tambahUser").click(function() {  
    $("#tbUser").hide();
    $("#addUser").show();
});

$("#reset").click(function() {  
    $("#tbUser").show();
    $("#addUser").hide();
});

// User
$('#tambahUser').click(function() {
    var url = '<?=site_url('user_management/user_add')?>';
    $('#labelDataUser').text('Tambah');  
    $('#dataUserModal .modal-body').html('');
    $('#dataUserModal .modal-body').load(url);
    $('#dataUserModalURL').text(url);
});

$('.editUser').click(function() {
    var id = $(this).attr('data-user-id');
    var url = '<?=site_url('user_management/user_edit')?>'+'/'+id;
    window.location = url; 
});

$(".deleteUser").click(function(e) {
    var name = $(this).attr('data-user-name');
    var id = $(this).attr('data-user-id');
    var url = '<?=site_url('user_management/user_delete')?>'+'/'+id;        
    $.SmartMessageBox({
        title : "Hapus Data User !",
        content : "<i class='fa fa-lg fa-fw fa-warning'></i> Yakin Hapus Data User " + name + " ?",
        buttons : '[No][Yes]'
    }, function(ButtonPressed) {
    if (ButtonPressed === "Yes") {
        $.post(url,{action:'delete',id:id},
        function(result){
            var result = eval('('+result+')');

            if(result.success){
                successNotifikasi(result.Msg);
            }else{
                alertNotifikasi(result.Msg);
            }
        }); 

        }
        if (ButtonPressed === "No") {
            $.smallBox({
                title : "Notifikasi ",
                content : "<i class='fa fa-clock-o'></i> <i>You Cancel Process...</i>",
                color : "#C46A69",
                iconSmall : "fa fa-times fa-2x fadeInRight animated",
                timeout : 4000
            });
        }

    });
    e.preventDefault();
})

function isValidEmailAddress(emailAddress){
    var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
    return pattern.test(emailAddress);
}

$('#simpanUserModal').click(function(){
    if ( $('#role').val()=='' || $('#role').val()===null ){
        alertNotifikasi('Role Harus Di Isi!');
        $('#role').focus();
        return false;
    }
    if ( $('#name').val()=='' ){
        $('#name').focus();
        return false;
    }
    if ( $('#no_identitas').val()=='' ){
        $('#no_identitas').focus();
        return false;
    }
    if ( !$.isNumeric( $('#no_identitas').val() ) ){
        alertNotifikasi('No Identitas harus angka !');
        $('#no_identitas').focus();
        return false;
    }
    if ( $('#phone').val()=='' ){
        $('#phone').focus();
        return false;
    }
    if ( !$.isNumeric( $('#phone').val() ) ){
        alertNotifikasi('No Telepon harus angka !');
        $('#phone').focus();
        return false;
    }
    if ( $('#email').val()=='' ){
        $('#email').focus();
        return false;
    }
    if ( !isValidEmailAddress($('#email').val()) ){
        alertNotifikasi('Alamat Email tidak valid !');
        $('#email').focus();
        return false;
    }
    if ( $('#username').val()=='' ){
        $('#username').focus();
        return false;
    }

    if ( $('#passwordAdd').val()=='' ){
        $('#passwordAdd').focus();
        return false;
    }
    if ( $('#passwordConfirmAdd').val()=='' ){
        $('#passwordConfirmAdd').focus();
        return false;
    }

    if ( $('#passwordConfirmAdd').val()!=$('#passwordAdd').val() ){
        alertNotifikasi('Password dan Konfirmasi Password tidak sama !');
        $('#passwordConfirmAdd').focus();
        return false;
    }

    var url = '<?=site_url('user_management/user_add')?>';        
    //$(this).attr('data-loading-text','Proses Simpan...').button('loading');         
    $.post(url,$('#form-user').serialize(),
    function(result){
        var result = eval('('+result+')');
        //if ( result.success ) $('#dataUserModal').modal('hide');  
        smartNotifikasi(result)
    });   
});      
</script>