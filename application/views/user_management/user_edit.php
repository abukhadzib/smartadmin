<ol class="breadcrumb">
    <li>User Management </li><li>Edit</li>
</ol>

</div>

<div id="content">
    
    <section id="widget-grid" class="">
        <div class="right_col" role="main">
            <div class="col-xs-12">
                <h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-user"></i> User Management <span> > Edit User </span></h3>
            </div>
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2>Form Edit User</h2>				
                        </header>
                        <div>
                            <div class="widget-body no-padding">
                                <form id="form-user" class="smart-form" novalidate="novalidate">
                                    <input type="hidden" name="user_id" value="<?=$user->id?>">
                                    <fieldset>
                                        <div class="row">
                                            <section class="col col-4">
                                                <label class="label">Role</label>
                                                <label class="select">
                                                    <select name="role" id="role">
                                                        <option value="" selected="" disabled="" > Role User</option>
                                                        <? if ($role) { foreach($role as $row_role) { ?>
                                                            <option value="<?=$row_role->id?>"<?=$row_role->id==$user->role_id?' selected="selected"':''?>><?=$row_role->name?></option>
                                                        <? } } ?>
                                                    </select> <i></i> 
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Name</label>
                                                <label class="input"> <i class="icon-prepend fa fa-user"></i>
                                                    <input type="text" id="name" name="name" placeholder="Nama" value="<?=$user->name?>" autofocus>
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">No Identitas</label>
                                                <label class="input"> <i class="icon-prepend fa fa-credit-card "></i>
                                                    <input type="text" id="no_identitas" name="no_identitas" placeholder="No Identitas" value="<?=$user->no_identitas?>">
                                                </label>
                                            </section>
                                        </div>
                                        <div class="row">
                                            <section class="col col-4">
                                                <label class="label">Email</label>
                                                <label class="input"> <i class="icon-prepend fa fa-envelope-o"></i>
                                                    <input type="email" id="email" name="email" placeholder="Email" value="<?=$user->email?>">
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Telepon</label>
                                                <label class="input"> <i class="icon-prepend fa fa-phone"></i>
                                                   <input type="text" id="phone" name="phone" placeholder="No Telpon" value="<?=$user->phone?>">
                                                </label>
                                            </section>
                                        </div>
                                        <section>
                                            <label class="label">Alamat</label>
                                            <label class="textarea"> 										
                                                <textarea rows="3" id="address1" name="address1" placeholder="Alamat"><?=$user->address1?></textarea> 
                                            </label>
                                        </section>
                                        <div class="row">
                                            <section class="col col-4">
                                                <label class="label">Username</label>
                                                <label class="input"><i class="icon-prepend fa fa-user"></i>
                                                    <input type="text" id="username" name="username" placeholder="Username" value="<?=$user->username?>">
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Password</label>
                                                <label class="input"> <i class="icon-prepend fa fa-key"></i>
                                                    <input type="password" id="password" name="password" placeholder="Password">
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Password Confirm</label>
                                                <label class="input"> <i class="icon-prepend fa fa-key"></i>
                                                    <input type="password" id="passwordConfirm" name="passwordConfirm" placeholder="Konfirmasi Password">
                                                </label>
                                            </section>
                                        </div>
                                    </fieldset>
                                    <footer>
                                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="location.href='<?php echo site_url('user_management/user')?>'">Batal</button>
                                        <button type="button" class="btn btn-primary" id="simpanUser" data-user-id="">Simpan</button>

                                    </footer>
                                </form>
                            </div>
                        </div>
                    </div>
                </article>
            </div>                       
        </div>
    </section>

<script type="text/javascript">
    function isValidEmailAddress(emailAddress){
        var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
        return pattern.test(emailAddress);
    }
    
    $('#simpanUser').click(function(){
        if ( $('#role').val()=='' || $('#role').val()===null ){
            alertNotifikasi('Role Harus Di Isi!');
            $('#role').focus();
            return false;
        }
        if ( $('#name').val()=='' ){
            $('#name').focus();
            return false;
        }
        if ( $('#no_identitas').val()=='' ){
            $('#no_identitas').focus();
            return false;
        }
        if ( !$.isNumeric( $('#no_identitas').val() ) ){
            alertNotifikasi('No Identitas harus angka !');
            $('#no_identitas').focus();
            return false;
        }
        if ( $('#phone').val()=='' ){
            $('#phone').focus();
            return false;
        }
        if ( !$.isNumeric( $('#phone').val() ) ){
            alertNotifikasi('No Telepon harus angka !');
            $('#phone').focus();
            return false;
        }
        if ( $('#email').val()=='' ){
            $('#email').focus();
            return false;
        }
        if ( !isValidEmailAddress($('#email').val()) ){
            alertNotifikasi('Alamat Email tidak valid !');
            $('#email').focus();
            return false;
        }
        if ( $('#username').val()=='' ){
            $('#username').focus();
            return false;
        }
        
        if ( $('#passwordConfirm').val()!=$('#password').val() ){
            alertNotifikasi('Password dan Konfirmasi Password tidak sama !');
            $('#passwordConfirm').focus();
            return false;
        }
        var url = '<?=site_url('user_management/user_edit')?>';        
        //$(this).attr('data-loading-text','Proses Simpan...').button('loading');         
        $.post(url,$('#form-user').serialize(),
        function(result){
            var result = eval('('+result+')');
            sendNotifikasi(result)
        });   
    });
    
function sendNotifikasi(result){
    var url = '<?=site_url('user_management/user')?>';
        
    if(result.success){
        $.SmartMessageBox({
            title : "Notifikasi !",
            content : result.Msg,
            buttons : '[OK]'
        }, function(ButtonPressed) {
            if (ButtonPressed === "OK") {
                window.location = url; 
            }

        });
    }else{
        alertNotifikasi(result.Msg);
    }
}
</script>