<section class="content">
    <section class="content-header">
        <h1>
          <i class="fa fa-user"></i> Data User Profile
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-user"></i> User</a></li>
          <li><a href="#">Profile</a></li>
        </ol>
      </section>

    
  <!-- Main content -->
  <section class="content">
    <div class="row">
            <div class="col-md-8">
            <div class="box box-primary">
               <div class="box-header with-border">
                <h3 class="box-title">User Profile</h3>
               </div>
                <div class="box-body">
                  <div class="col-lg-8">
                     <form enctype="multipart/form-data" id="form-profile" class="form-horizontal" method="post">
                        <div class="form-group">
                            <label for="name" class="col-xs-4 control-label">Role *</label>
                            <div class="col-xs-8">
                                <select name="role" class="form-control" id="role" disabled="">
                                                            <option value="" selected="" disabled="" > --Role--</option>
                                    <? if ($role) { foreach($role as $row_role) { ?>
                                                            <option value="<?=$row_role->id?>"<?=$row_role->id==$user->role_id?' selected="selected"':''?>><?=$row_role->name?></option>
                                    <? } } ?>
                                </select>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label for="name" class="col-xs-4 control-label">Nama *</label>
                            <div class="col-xs-8">
                                <input type="text" class="form-control" id="name_profile" name="name_profile" placeholder="Nama" value="<?=$user->name?>" autofocus>
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <label for="no_identitas" class="col-xs-4 control-label">No Identitas *</label>
                            <div class="col-xs-8">
                                <input type="text" class="form-control" id="no_identitas_profile" name="no_identitas_profile" placeholder="No Identitas" value="<?=$user->no_identitas?>">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label for="address1" class="col-xs-4 control-label">Alamat</label>
                            <div class="col-xs-8">
                                <input type="text" class="form-control" id="address1_profile" name="address1_profile" placeholder="Alamat" value="<?=$user->address1?>">
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="phone" class="col-xs-4 control-label">No Telpon *</label>
                            <div class="col-xs-8">
                                <input type="text" class="form-control" id="phone_profile" name="phone_profile" placeholder="No Telpon" value="<?=$user->phone?>">
                            </div>
                        </div> 
                         <div class="form-group">
                            <label for="email" class="col-xs-4 control-label">Email *</label>
                            <div class="col-xs-8">
                                <input type="email" class="form-control" id="email_profile" name="email_profile" placeholder="Email" value="<?=$user->email?>">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label for="name" class="col-xs-4 control-label">Kota</label>
                            <div class="col-xs-8">
                                <input type="text" class="form-control" id="city_profile" name="city_profile" placeholder="Kota" value="<?=$user->city?>">
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="username" class="col-xs-4 control-label">Username *</label>
                            <div class="col-xs-8">
                                <input type="text" class="form-control" id="username_profile" name="username_profile" placeholder="Username" value="<?=$user->username?>">
                            </div>
                        </div>   
                        <em>*) Wajib diisi.</em>
                     </form>
                    </div>
                    </div>
                <div class="box-footer">
                    <span id="dataProfileModalURL" style="display: none;"></span>
                    <button type ="reset" class="btn btn-default" onclick="location.href='<?php echo site_url('home')?>'">Back</button>
                    <button type="button" class="btn btn-success" id="simpanProfileModal" data-profile-id=""><i class="fa fa-floppy-o"></i> Simpan</button>
                </div>
                </div>
                
            </div>
        </div>
        
</section>
</section>

