<br /> 
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <form enctype="multipart/form-data" id="form-subprogram" class="form-horizontal" method="post">
			<div class="form-group">
                <label for="name" class="col-xs-2 control-label">Sub Program *</label>
                <div class="col-xs-10">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Sub Program" autofocus>
                </div>
            </div>              
            <div class="form-group">
                <label for="zipcode" class="col-xs-2 control-label">Description</label>
                <div class="col-xs-10">
                    <input type="text" class="form-control" id="description" name="description" placeholder="Description">
                </div>
            </div> 
            <div class="form-group">
                <label for="keyword" class="col-xs-2 control-label">Keyword *</label>
                <div class="col-xs-10">
                    <input type="text" class="form-control" id="keyword" name="keyword" placeholder="Keyword">
                </div>
            </div>              
            <div class="form-group">
                <label for="name" class="col-xs-2 control-label">Partner *</label>
                <div class="col-xs-10">
                    <select name="partner" class="form-control" id="partner">
						<option value="" selected="" disabled="" > --Partner--</option>
                        <? if ($partner) { foreach($partner as $row_partner) { ?>
						<option value="<?=$row_partner->id?>"><?=$row_partner->name?></option>
                        <? } } ?>
					</select>
                </div>
            </div>               
            <div class="form-group">
                <label for="name" class="col-xs-2 control-label">Program *</label>
                <div class="col-xs-10">
                    <select name="program" class="form-control" id="program">
						<option value="" selected="" disabled="" > --Program--</option>
                        <? if ($program) { foreach($program as $row_program) { ?>
						<option value="<?=$row_program->id?>"><?=$row_program->name?></option>
                        <? } } ?>
					</select>
                </div>
            </div>             
            <div class="form-group">
                <label for="sms_response" class="col-xs-2 control-label">Response</label>
                <div class="col-xs-10">
                    <textarea class="form-control" id="sms_response" name="sms_response" placeholder="SMS Response"></textarea>
                    <em id="sms_response_count">0</em>
                </div>
            </div> 
            <em>*) Wajib diisi.</em>
		</form>
	</div>  
</div>
