<br />
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <form enctype="multipart/form-data" id="form-partner" class="form-horizontal" method="post">
			<input type="hidden" name="partner_id" value="<?=$partner->id?>">
            <div class="form-group">
                <label for="name" class="col-xs-2 control-label">Nama *</label>
                <div class="col-xs-4">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Nama" value="<?=$partner->name?>" autofocus>
                </div>
                <label for="address1" class="col-xs-2 control-label">Alamat</label>
                <div class="col-xs-4">
                    <input type="text" class="form-control" id="address1" name="address1" placeholder="Alamat" value="<?=$partner->address1?>">
                </div>
            </div>              
            <div class="form-group">
                <label for="zipcode" class="col-xs-2 control-label">Kode Pos</label>
                <div class="col-xs-4">
                    <input type="text" class="form-control" id="zipcode" name="zipcode" placeholder="Kode Pos" value="<?=$partner->zip?>">
                </div>
                <label for="name" class="col-xs-2 control-label">Kota</label>
                <div class="col-xs-4">
                    <input type="text" class="form-control" id="city" name="city" placeholder="Kota" value="<?=$partner->city?>">
                </div>
            </div> 
            <div class="form-group">
                <label for="address2" class="col-xs-2 control-label">Propinsi</label>
                <div class="col-xs-4">
                    <input type="text" class="form-control" id="address2" name="address2" placeholder="Propinsi" value="<?=$partner->address2?>">
                </div>
                <label for="name" class="col-xs-2 control-label">Negara</label>
                <div class="col-xs-4">
                    <input type="text" class="form-control" id="country" name="country" placeholder="Negara" value="<?=$partner->country?>">
                </div>
            </div>              
            <div class="form-group">
                <label for="npwp" class="col-xs-2 control-label">NPWP *</label>
                <div class="col-xs-4">
                    <input type="text" class="form-control" id="npwp" name="npwp" placeholder="NPWP" value="<?=$partner->npwp?>">
                </div>
                <label for="phone" class="col-xs-2 control-label">No Telpon *</label>
                <div class="col-xs-4">
                    <input type="text" class="form-control" id="phone" name="phone" placeholder="No Telpon" value="<?=$partner->phone?>">
                </div>
            </div>          
            <div class="form-group">
                <label for="keyword" class="col-xs-2 control-label">Keyword</label>
                <div class="col-xs-4">
                    <input type="text" class="form-control" id="keyword" name="keyword" placeholder="Keyword" value="<?=$partner->keyword?>">
                </div>
                <label for="description" class="col-xs-2 control-label">Deskripsi</label>
                <div class="col-xs-4">
                    <input type="text" class="form-control" id="description" name="description" placeholder="Deskripsi" value="<?=$partner->description?>">
                </div>
            </div>        
            <div class="form-group">
                <label for="sms_response" class="col-xs-2 control-label">Response</label>
                <div class="col-xs-10">
                    <textarea class="form-control" id="sms_response" name="sms_response" placeholder="SMS Response"><?=$partner->sms_response?></textarea>
                    <em id="sms_response_count"><?=strlen($partner->sms_response)?></em>
                </div>
            </div> 
            <em>*) Wajib diisi.</em>
		</form>
	</div>  
</div>
