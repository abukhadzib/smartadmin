<br />
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <form enctype="multipart/form-data" id="form-program" class="form-horizontal" method="post">
			<input type="hidden" name="program_id" value="<?=$program->id?>">
            <div class="form-group">
                <label for="name" class="col-xs-2 control-label">Program *</label>
                <div class="col-xs-10">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Program" value="<?=$program->name?>" autofocus>
                </div>
            </div>              
            <div class="form-group">
                <label for="zipcode" class="col-xs-2 control-label">Description</label>
                <div class="col-xs-10">
                    <input type="text" class="form-control" id="description" name="description" placeholder="Description" value="<?=$program->description?>">
                </div>
            </div>
            <div class="form-group">
                <label for="keyword" class="col-xs-2 control-label">Keyword *</label>
                <div class="col-xs-10">
                    <input type="text" class="form-control" id="keyword" name="keyword" placeholder="Keyword" value="<?=$program->keyword?>">
                </div>
            </div> 
            <div class="form-group">
                <label for="name" class="col-xs-2 control-label">Partner *</label>
                <div class="col-xs-10">
                    <select name="partner" class="form-control" id="partner">
						<option value="" selected="" disabled="" > --Partner--</option>
                        <? if ($partner) { foreach($partner as $row_partner) { ?>
						<option value="<?=$row_partner->id?>"<?=$row_partner->id==$program->partner_id?' selected="selected"':''?>><?=$row_partner->name?></option>
                        <? } } ?>
					</select>
                </div>
            </div>           
            <div class="form-group">
                <label for="sms_response" class="col-xs-2 control-label">Response</label>
                <div class="col-xs-10">
                    <textarea class="form-control" id="sms_response" name="sms_response" placeholder="SMS Response"><?=$program->sms_response?></textarea>
                    <em id="sms_response_count"><?=strlen($program->sms_response)?></em>
                </div>
            </div> 
            <em>*) Wajib diisi.</em>
		</form>
	</div>  
</div>
