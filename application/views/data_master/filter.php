<!-- breadcrumb -->
<ol class="breadcrumb">
	<li>User Management </li><li>Role</li>
</ol>
<!-- end breadcrumb -->

</div>
<!-- END RIBBON -->

<!-- MAIN CONTENT -->
<div id="content">

<div class="row">
	<div class="col-xs-12">
		<h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-table"></i> Data Master <span> > Filter Words </span></h3>
	</div>
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-4">
                <form enctype="multipart/form-data" id="form-search" class="form-horizontal" method="post">  
                    <div class="form-group input-group" style="margin: auto;">
                        <input type="text" class="form-control" name="search" id="search" placeholder="Pencarian" value="<?=isset($search) && !empty($search) ? $search : ''?>">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit" title="Search"><i class="fa fa-search"></i></button>
                            <button class="btn btn-default" type="reset" title="Clear Search" id="resetSearchFilter"><i class="glyphicon glyphicon-remove"></i></button>
                        </span>
                    </div>
                </form>               
            </div>
            <div class="col-xs-4"></div>  
            <div class="col-xs-4">
                <div class="pull-right" style="margin-bottom: 14px;">
                    <button type="button" class="btn btn-primary" id="tambahFilter" data-toggle="modal" data-target="#dataFilterModal"><i class="fa fa-plus"></i> Tambah Data Filter</button>
                </div>                
            </div>  
        </div>                
    </div>    
    <div class="col-xs-12">                     
        <div class="table-responsive">												
        	<table class="table table-bordered table-hover table-striped" style="margin-bottom: auto;">
        		<thead>
        			<tr>
        				<th style="width: 5%;">#</th>
        				<th style="width: 90%;">Word</th>
                        <th style="width: 5%;">Action</th>
        			</tr>
        		</thead>
        		<tbody>
        			<?php 
                        $no = 1; 
                        foreach ($content as $row){ 
                    ?>
                        <tr>
                            <td><?=($page*$perpage)+$no;?></td>
                            <td><?=isset($search) && !empty($search) ? str_ireplace($search,'<b style="background-color: yellow;">'.$search.'</b>',$row->word) : $row->word;?>
                            <td>
                                <a class="editFilter" href="#" title="Edit" data-filter-name="<?=$row->word;?>" data-filter-id="<?=$row->id;?>" data-toggle="modal" data-target="#dataFilterModal"><i class="fa fa-edit"></i></a>&nbsp;
                                <a class="deleteFilter" href=#" title="Remove" data-filter-name="<?=$row->word;?>" data-filter-id="<?=$row->id;?>" data-toggle="modal" data-target="#deleteFilterModal"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>                         
                    <?php 
                        $no++; 
                        }
                    ?>
        		</tbody>
        	</table>        	
        </div>       
        <div align="right" style=" margin-top: -25px;">
            <?=$pagination;?>
        </div>           
    </div>    
</div>                       

<!-- Modal Hapus -->
<div class="modal fade" id="deleteFilterModal" tabindex="-1" role="dialog" aria-labelledby="labelDeleteFilter">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title" id="labelDeleteFilter">Hapus Data Filter</h4>
            </div>
            <div class="modal-body">  
                <i class="fa fa-lg fa-fw fa-warning"></i>Yakin mau hapus data Filter <span id="namaFilterHapus"></span> ?                  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="deleteFilterModalYes" data-Filter-id="">Ya</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Data Tambah dan Edit -->
<div class="modal fade" id="dataFilterModal" tabindex="-1" role="dialog" aria-labelledby="labelDataFilter">
    <div class="modal-dialog" role="document" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <!--button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title"><span id="labelDataFilter"></span> Data Filter <span id="loading-data" style="display:none;margin-left: 10px;"><img src="<?=base_url('assets/img/loading.gif')?>" /></span></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="dataFilterModalURL" style="display: none;"></span>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="simpanFilterModal" data-Filter-id="">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Notifikasi -->
<div class="modal fade" id="notifikasiFilterModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiFilter">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="labelNotifikasiFilter">Notifikasi</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="statusNotifikasiFilter" style="display: none;"></span>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiFilterOK">OK</button>
            </div>
        </div>
    </div>
</div>
