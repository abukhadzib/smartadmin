<!-- breadcrumb -->
<ol class="breadcrumb">
	<li>User Management </li><li>Role</li>
</ol>
<!-- end breadcrumb -->

</div>
<!-- END RIBBON -->

<!-- MAIN CONTENT -->
<div id="content">

<div class="row">
	<div class="col-xs-12">
		<h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-table"></i> Data Master <span> > Program </span></h3>
	</div>
    <div class="col-xs-12">
        <div class="pull-right" style="margin-bottom: 14px;">
            <button type="button" class="btn btn-primary" id="tambahProgram" data-toggle="modal" data-target="#dataProgramModal"><i class="fa fa-plus"></i> Tambah Data Program</button>
        </div>                
    </div>    
    <div class="col-xs-12">
        <form enctype="multipart/form-data" id="form-search" class="form-inline" method="post">
            <div class="panel panel-default">
                <div class="panel-heading">Filter & Search</div>
                <div class="panel-body">     
                    <div class="form-group">
                        <input type="text" class="form-control" id="search_name" name="search_name" placeholder="Name" value="<?=isset($name_view_program_mesinpesan) && !empty($name_view_program_mesinpesan) ? $name_view_program_mesinpesan : ''?>">
                    </div>                   
                    <div class="form-group">
                        <input type="text" class="form-control" id="search_description" name="search_description" placeholder="Description" value="<?=isset($description_view_program_mesinpesan) && !empty($description_view_program_mesinpesan) ? $description_view_program_mesinpesan : ''?>">
                    </div>                   
                    <div class="form-group">
                        <input type="text" class="form-control" id="search_keyword" name="search_keyword" placeholder="Keyword" value="<?=isset($keyword_view_program_mesinpesan) && !empty($keyword_view_program_mesinpesan) ? $keyword_view_program_mesinpesan : ''?>">
                    </div>                   
                    <div class="form-group">
                        <select name="search_partner" class="form-control" id="search_partner">
    						<option value=""<?=isset($partner_view_program_mesinpesan) && empty($partner_view_program_mesinpesan) ? ' selected="selected"' : ''?>> --Partner--</option>
                            <? if ($partner) { foreach($partner as $row_partner) { ?>
    						<option value="<?=$row_partner->id?>"<?=isset($partner_view_program_mesinpesan) && $partner_view_program_mesinpesan==$row_partner->id ? ' selected="selected"' : ''?>><?=$row_partner->name?></option>
                            <? } } ?>
    					</select>
                    </div>  
                    <div class="form-group">
                        <input type="text" class="form-control" id="search_response" name="search_response" placeholder="response" value="<?=isset($response_view_program_mesinpesan) && !empty($response_view_program_mesinpesan) ? $response_view_program_mesinpesan : ''?>">
                    </div>       
                </div>
                <div class="panel-footer" style="text-align: right;">
                    <button type="submit" class="btn btn-primary">Search</button>
                    <button type="button" class="btn btn-default" id="clear-search-program-view">Clear Search</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-xs-12">                     
        <div class="table-responsive">												
        	<table class="table table-bordered table-hover table-striped" style="margin-bottom: auto;">
        		<thead>
        			<tr>
        				<th>#</th>
        				<th>Name</th>
        				<th>Description</th>
        				<th>Keyword</th>
        				<th>Partner</th>
        				<th>SMS Response</th>
        				<th>Key</th>
                        <th style="width: 5%;">Action</th>
        			</tr>
        		</thead>
        		<tbody>
        			<?php 
                        $no = 1;  
                        if ( count($content) > 0 ) {
                        foreach ($content as $row){ 
                    ?>
                        <tr>
                            <td><?=($page*$perpage)+$no;?></td>
                            <td><?=$row->name;?></td>
                            <td><?=$row->description;?></td>
                            <td><?=$row->keyword;?></td>
                            <td><?=$row->partner_name;?></td>
                            <td><?=$row->sms_response;?></td>
                            <td><?=$row->secure_key;?></td>
                            <td>
                                <a class="editProgram" href="#" title="Edit" data-program-name="<?=$row->name;?>" data-program-id="<?=$row->id;?>" data-toggle="modal" data-target="#dataProgramModal"><i class="fa fa-edit"></i></a>&nbsp;
                                <a class="deleteProgram" href=#" title="Remove" data-program-name="<?=$row->name;?>" data-program-id="<?=$row->id;?>" data-toggle="modal" data-target="#deleteProgramModal"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>                         
                    <?php 
                        $no++; 
                        } } else {
                    ?>
                        <tr>
                            <td colspan="7"style="text-align: center;">No Data</td>
                        </tr>
                    <?php } ?>
        		</tbody>
        	</table>        	
        </div>       
        <div align="right" style=" margin-top: -25px;">
            <?=$pagination;?>
        </div>           
    </div>    
</div>                       

<!-- Modal Hapus -->
<div class="modal fade" id="deleteProgramModal" tabindex="-1" role="dialog" aria-labelledby="labelDeleteProgram">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title" id="labelDeleteProgram">Hapus Data Program</h4>
            </div>
            <div class="modal-body">  
                <i class="fa fa-lg fa-fw fa-warning"></i>Yakin mau hapus data Program <span id="namaProgramHapus"></span> ?                  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="deleteProgramModalYes" data-program-id="">Ya</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Data Tambah dan Edit -->
<div class="modal fade" id="dataProgramModal" tabindex="-1" role="dialog" aria-labelledby="labelDataProgram">
    <div class="modal-dialog" role="document" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <!--button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title"><span id="labelDataProgram"></span> Data Program <span id="loading-data" style="display:none;margin-left: 10px;"><img src="<?=base_url('assets/img/loading.gif')?>" /></span></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="dataProgramModalURL" style="display: none;"></span>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="simpanProgramModal" data-program-id="">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Notifikasi -->
<div class="modal fade" id="notifikasiProgramModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiProgram">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="labelNotifikasiProgram">Notifikasi</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="statusNotifikasiProgram" style="display: none;"></span>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiProgramOK">OK</button>
            </div>
        </div>
    </div>
</div>
