<br />
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <form enctype="multipart/form-data" id="form-filter" class="form-horizontal" method="post">
			<input type="hidden" name="filter_id" value="<?=$filter->id?>">
            <div class="form-group">
                <label for="word" class="col-xs-2 control-label">Word *</label>
                <div class="col-xs-10">
                    <input type="text" class="form-control" id="word" name="word" placeholder="Word" value="<?=$filter->word?>" autofocus>
                </div>
            </div>
            <em>*) Wajib diisi.</em>
		</form>
	</div>  
</div>
