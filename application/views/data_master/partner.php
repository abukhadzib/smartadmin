<!-- breadcrumb -->
<ol class="breadcrumb">
	<li>User Management </li><li>Role</li>
</ol>
<!-- end breadcrumb -->

</div>
<!-- END RIBBON -->

<!-- MAIN CONTENT -->
<div id="content">

<div class="row">
	<div class="col-xs-12">
		<h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-table"></i> Data Master <span> > Partner </span></h3>
	</div>
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-4">
                <form enctype="multipart/form-data" id="form-search" class="form-horizontal" method="post">  
                    <div class="form-group input-group" style="margin: auto;">
                        <input type="text" class="form-control" name="search" id="search" placeholder="Pencarian" value="<?=isset($search) && !empty($search) ? $search : ''?>">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit" title="Search"><i class="fa fa-search"></i></button>
                            <button class="btn btn-default" type="reset" title="Clear Search" id="resetSearchPartner"><i class="glyphicon glyphicon-remove"></i></button>
                        </span>
                    </div>
                </form>               
            </div>
            <div class="col-xs-4"></div>  
            <div class="col-xs-4">
                <div class="pull-right" style="margin-bottom: 14px;">
                    <button type="button" class="btn btn-primary" id="tambahPartner" data-toggle="modal" data-target="#dataPartnerModal"><i class="fa fa-plus"></i> Tambah Data Partner</button>
                </div>                
            </div>  
        </div>                
    </div>  
    <div class="col-xs-12">                     
        <div class="table-responsive">												
        	<table class="table table-bordered table-hover table-striped" style="margin-bottom: auto;">
        		<thead>
        			<tr>
        				<th>#</th>
        				<th>Name</th>
        				<th>Keyword</th>
        				<th>Telepon</th>
        				<th>Alamat</th>
        				<th>SMS Response</th>
        				<th>Key</th>
                        <th style="width: 5%;">Action</th>
        			</tr>
        		</thead>
        		<tbody>
        			<?php 
                        $no = 1; 
                        foreach ($content as $row){ 
                    ?>
                        <tr>
                            <td><?=($page*$perpage)+$no;?></td>
                            <td><?=isset($search) && !empty($search) ? str_ireplace($search,'<b style="background-color: yellow;">'.$search.'</b>',$row->name) : $row->name;?>
                            <td><?=isset($search) && !empty($search) ? str_ireplace($search,'<b style="background-color: yellow;">'.$search.'</b>',$row->keyword) : $row->keyword;?>
                            <td><?=isset($search) && !empty($search) ? str_ireplace($search,'<b style="background-color: yellow;">'.$search.'</b>',$row->phone) : $row->phone;?>
                            <td><?=isset($search) && !empty($search) ? str_ireplace($search,'<b style="background-color: yellow;">'.$search.'</b>',$row->address1) : $row->address1;?>
                            <td><?=isset($search) && !empty($search) ? str_ireplace($search,'<b style="background-color: yellow;">'.$search.'</b>',$row->sms_response) : $row->sms_response;?>
                            <td><?=$row->secure_key;?></td>
                            <td>
                                <a class="editPartner" href="#" title="Edit" data-Partner-name="<?=$row->name;?>" data-Partner-id="<?=$row->id;?>" data-toggle="modal" data-target="#dataPartnerModal"><i class="fa fa-edit"></i></a>&nbsp;
                                <a class="deletePartner" href=#" title="Remove" data-Partner-name="<?=$row->name;?>" data-Partner-id="<?=$row->id;?>" data-toggle="modal" data-target="#deletePartnerModal"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>                         
                    <?php 
                        $no++; 
                        }
                    ?>
        		</tbody>
        	</table>        	
        </div>       
        <div align="right" style=" margin-top: -25px;">
            <?=$pagination;?>
        </div>           
    </div>    
</div>                       

<!-- Modal Hapus -->
<div class="modal fade" id="deletePartnerModal" tabindex="-1" role="dialog" aria-labelledby="labelDeletePartner">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title" id="labelDeletePartner">Hapus Data Partner</h4>
            </div>
            <div class="modal-body">  
                <i class="fa fa-lg fa-fw fa-warning"></i>Yakin mau hapus data Partner <span id="namaPartnerHapus"></span> ?                  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="deletePartnerModalYes" data-Partner-id="">Ya</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Data Tambah dan Edit -->
<div class="modal fade" id="dataPartnerModal" tabindex="-1" role="dialog" aria-labelledby="labelDataPartner">
    <div class="modal-dialog" role="document" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <!--button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title"><span id="labelDataPartner"></span> Data Partner <span id="loading-data" style="display:none;margin-left: 10px;"><img src="<?=base_url('assets/img/loading.gif')?>" /></span></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="dataPartnerModalURL" style="display: none;"></span>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="simpanPartnerModal" data-Partner-id="">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Notifikasi -->
<div class="modal fade" id="notifikasiPartnerModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiPartner">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="labelNotifikasiPartner">Notifikasi</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="statusNotifikasiPartner" style="display: none;"></span>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiPartnerOK">OK</button>
            </div>
        </div>
    </div>
</div>
