<ol class="breadcrumb">
	<li>CTA </li><li>Product</li><li>Edit</li>
</ol>

</div>

<div id="content">

<section class="content">
    <div class="col-xs-12">
            <h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-bar-chart-o"></i> Product <span> > Edit Product Discount</span></h3>
    </div> 
    
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header">
                  <h3 class="box-title">Edit Data Product <span id="loading-data" style="display:none;margin-left: 10px;"><img src="<?=base_url('assets/img/Preloader_3.gif')?>" /></span></h3>
                  
                </div>
                <form enctype="multipart/form-data" id="form-edit-product" class="form-horizontal" method="post" >
                <input type="hidden" name="id" value="<?=$product->id?>">
                <div class="box-body">
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Kode Produk * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-key"></i>
                            </div>
                              <input type="text" name="code" id="code" class="form-control" placeholder="Kode Produk" value="<?=$product->code?>">
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Nama Produk * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-key"></i>
                            </div>
                              <input type="text" name="name" id="name" class="form-control" placeholder="Nama Produk" value="<?=$product->name?>">
                          </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Fee :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-home"></i>
                            </div>
                              <input type="text" name="fee" id="fee" class="form-control" placeholder="Fee" value="<?=$product->fee?>">
                          </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                <em>* Wajib diisi.</em>
                    <div align="center">
                        <button type="reset" class="btn btn-danger" id="reset" onclick="location.href='<?php echo site_url('cta/product')?>'"><i class="fa fa-close"></i> Batal</button>
                        <button type="button" class="btn btn-danger" id="saveProduct"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
     </div>
            
</section>

<!-- Modal Notifikasi -->
<div class="modal fade" id="notifikasiProductModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiDiscount">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="labelNotifikasiProduct">Notifikasi</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="statusNotifikasiProduct" style="display: none;"></span>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiProductOK">OK</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ajaxStart(function(){
	$("#loading-data").show();
    }).ajaxStop(function(){
            $("#loading-data").fadeOut("fast");
        $(window).resize();        
    }); 
    
   $("#fee").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
         // Allow: Ctrl+C
        (e.keyCode == 67 && e.ctrlKey === true) ||
         // Allow: Ctrl+X
        (e.keyCode == 88 && e.ctrlKey === true) ||
         // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

$('#saveProduct').click(function(){
    
    if ( $('#code').val()=='' ){
        $('#code').focus();
        return false;
    }
    
    if ( $('#name').val()==''){
        $('#name').focus();
        return false;
    }
    if ( $('#fee').val()=='' ){
        $('#fee').focus();
        return false;
    }
    
    var url = '<?=site_url('cta/product_edit')?>';       
    //$(this).attr('data-loading-text','Proses Simpan...').button('loading');         
    $.post(url,$('#form-edit-product').serialize(),
    function(result){
        var result = eval('('+result+')');
        if ( result.success );  
        //alert(result.Msg);
       showNotifikasi('notifikasiProductModal',result.Msg); 
       $('#statusNotifikasiProduct').val(result.success);
    });   
});

$("#buttonNotifikasiProductOK").click(function() {
    if ( $('#statusNotifikasiProduct').val() ) {
        //window.location.reload();
        window.location = "<?=site_url('cta/product')?>";
    } 
});

function showNotifikasi(idModal,pesan){
    $('#'+idModal+' .modal-body').html(pesan);
    $('#'+idModal).modal('show');
    $('body .modal-backdrop').hide();

}

</script>