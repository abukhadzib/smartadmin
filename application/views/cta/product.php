<ol class="breadcrumb">
	<li>CTA </li><li>Product</li><li>list</li>
</ol>

</div>

<div id="content">

<section class="content">
<div class="row">
    <div class="col-xs-12">
         <h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-bar-chart-o"></i> Data Product <span> > List</span></h3>
    </div> 
    
        <div class="box-body">
            <div  style="margin-bottom: 1px" align="right"> 
                <button type="button" class="btn btn-danger" id="tambahProduct" title="add Product">Tambah <i class="fa fa-plus"></i> </button>
            </div>
        </div> 
        <div class="col-xs-12" id="tb">
            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title"><b>Data Product </b> <span id="loading-data" style="display:none;margin-left: 10px;"><img src="<?=base_url('assets/img/Preloader_3.gif')?>" /></span></h3>
                  
                </div>
                <div class="box-body">
            <section id="no-more-tables">
            <div class="table-responsive">												
                    <table id="tbproduct" class="table table-bordered table-hover table-striped" style="margin-bottom: auto; background: #fff; overflow: auto;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>Fee</th>
                                    <th>created_at</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                    <?php 
                            $no = 1; 
                            if ( count($content) > 0 ) {
                            foreach ($content as $row){ 
                        ?>
                            <tr>
                                <td data-title="#"><?=($page*$perpage)+$no;?>&nbsp;</td>
                                <td data-title="Code"><?=$row->code;?>&nbsp;</td>
                                <td data-title="Name"><?=$row->name;?>&nbsp;</td>
                                <td data-title="Fee"><?=$row->fee;?>&nbsp;</td>
                                <td data-title="Created_at"><?=$row->created_at;?>&nbsp;</td>
                                <td data-title="Action">
                                <a class="editProduct" href="#" title="Ubah" data-product-name="<?=$row->name;?>" data-product-id="<?=$row->id;?>" data-toggle="modal" data-target="#dataProductModal"><i class="fa fa-edit"></i></a>&nbsp;
                                <a class="deleteProduct" href=#" title="Hapus" data-product-name="<?=$row->name;?>" data-product-id="<?=$row->id;?>" data-toggle="modal" data-target="#deleteProductModal"><i class="fa fa-trash-o" style="color: red;"></i></a>
                               </td>
                            </tr>                         
                        <?php 
                            $no++; 
                            } } else {
                        ?>
                            <tr>
                                <td colspan="15"style="text-align: center;">No Data</td>
                            </tr>
                        <?php } ?>
                            </tbody>
                    </table>        	
            </div> 
            </section>
            <div align="right" style=" margin-top: -25px;">
                <?//=$pagination;?>
            </div>  
        </div>
        </div>    
    </div>
    <div class="col-xs-12" id="addProduct" style="display: none;">
            <div class="box box-danger">
                <div class="box-header">
                  <h3 class="box-title">Input Data Product  <span id="loading-data" style="display:none;margin-left: 10px;"><img src="<?=base_url('assets/img/Preloader_3.gif')?>" /></span></h3>
                  
                </div>
                <form enctype="multipart/form-data" id="form-product" class="form-horizontal" method="post" >
                <div class="box-body">
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Kode * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-key"></i>
                            </div>
                              <input type="text" name="code" id="code" class="form-control" placeholder="Kode Produk" >
                          </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Name Product* :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-key"></i>
                            </div>
                              <input type="text" name="name" id="name" class="form-control" placeholder="Nama Produk" >
                          </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Fee * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-home"></i>
                            </div>
                              <input type="text" name="fee" id="fee" class="form-control" placeholder="Fee">
                          </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                <em>* Wajib diisi.</em>
                    <div align="center">
                        <button type="reset" class="btn btn-danger" id="reset"><i class="fa fa-close"></i> Batal</button>
                        <button type="button" class="btn btn-danger" id="simpanProduct"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
</div>                       
</section>


<!-- Modal Hapus -->
<div class="modal fade" id="deleteProductModal" tabindex="-1" role="dialog" aria-labelledby="labelDeleteProduct">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title" id="labelDeleteMerchant">Hapus Data Product</h4>
            </div>
            <div class="modal-body">  
                <i class="fa fa-lg fa-fw fa-warning"></i>Yakin mau hapus data Product <span id="namaProductHapus"></span> ?                  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="deleteProductModalYes" data-Product-id="">Ya</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Notifikasi -->
<div class="modal fade" id="notifikasiProductModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiProduct">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="labelNotifikasiProduct">Notifikasi</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="statusNotifikasiProduct" style="display: none;"></span>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiProductOK">OK</button>
            </div>
        </div>
    </div>
</div>

<script>

$("#fee").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
         // Allow: Ctrl+C
        (e.keyCode == 67 && e.ctrlKey === true) ||
         // Allow: Ctrl+X
        (e.keyCode == 88 && e.ctrlKey === true) ||
         // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});
    
$(function () {
    $("#tbproduct").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
  
$("#tambahProduct").click(function() {  
    $("#tb").hide();
    $("#addProduct").show();
});


$("#reset").click(function() {  
    $("#tb").show();
    $("#addProduct").hide();
});

$('#simpanProduct').click(function(){
    
    if ( $('#code').val()=='' ){
        $('#code').focus();
        return false;
    }
    
    if ( $('#name').val()=='' ){
        $('#name').focus();
        return false;
    }
    if ( $('#fee').val()=='' ){
        $('#fee').focus();
        return false;
    }
    
    var url = '<?=site_url('cta/product_add')?>'; 
    //$(this).attr('data-loading-text','Proses Simpan...').button('loading');         
    $.post(url,$('#form-product').serialize(),
    function(result){
        var result = eval('('+result+')');
        if ( result.success );  
        //alert(result.Msg);
       showNotifikasi('notifikasiProductModal',result.Msg); 
       $('#statusNotifikasiProduct').val(result.success);
    });   
});

$('.editProduct').click(function() {
    var id = $(this).attr('data-product-id');
    var url = '<?=site_url('cta/product_edit')?>'+'/'+id;
    window.location = url;
});

$(".deleteProduct").click(function() {
    var name = $(this).attr('data-product-name');
    var id = $(this).attr('data-product-id');
	$('#deleteProductModalYes').attr('data-product-id',id);
    $('#namaProductHapus').html('<b>'+name+'</b>');
});    

$('#deleteProductModalYes').click(function() {
    var id  = $(this).attr('data-product-id');
    var url = '<?=site_url('cta/product_delete')?>'+'/'+id; 
    $(this).attr('data-loading-text','Proses Delete...').button('loading');        
    $.post(url,{action:'delete',id:id},
    function(result){
        var result = eval('('+result+')');
        $('#deleteCodeModal').modal('hide');
        showNotifikasi('notifikasiProductModal',result.Msg);
        $('#statusNotifikasiProduct').val(result.success);
    });        
});

$("#buttonNotifikasiProductOK").click(function() {
    if ( $('#statusNotifikasiProduct').val() ) {
        window.location.reload();
        //window.location = "<?=site_url('cta/product')?>";
    } 
});

function showNotifikasi(idModal,pesan){
    $('#'+idModal+' .modal-body').html(pesan);
    $('#'+idModal).modal('show');
    $('body .modal-backdrop').hide();

}

</script>