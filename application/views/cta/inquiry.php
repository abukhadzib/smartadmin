<ol class="breadcrumb">
	<li>CTA </li><li>Inquiry</li>
</ol>

</div>

<div id="content">

<section class="content">
<div class="right_col" role="main">
	<div class="">

<div class="row">
	<div class="col-xs-12">
		<h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-barcode"></i> Report <span> > CTA Inquiry </span></h3>
	</div>
    <div class="box-body">
        <div class="col-xs-2">
            <input class="form-control" placeholder="Date Range" style="width: 200px;" name="daterange" id="daterange" required>
        </div>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-danger" id="excel" title="Download"><i class="fa fa-download"> </i> download </button>
    </div> 

   <div class="col-xs-12"> 
       <div class="box box-danger">
        <div class="box-header">
            <h3 class="box-title"><b>Data Inquiry </b> <span id="loading-data" style="display:none;margin-left: 10px;"><img src="<?=base_url('assets/img/Preloader_3.gif')?>" /></span></h3>

        </div>
        <div class="box-body">
                <section id="no-more-tables">
                 <div class="table-responsive">												
                         <table id="tbinquiry" class="table table-bordered table-hover table-striped" style="margin-bottom: auto; background: #fff; overflow: auto;">
                                 <thead>
                                 <tr>
                                 <th>#</th>
                                 <th>date</th>
                                 <th>RequestId </th>
                                 <th>Amount </th>
                                 <th>Fee </th>
                                 <th>Total </th>
                                 <th>Kode Bank</th>
                                 <th>No Rekening</th>
                                 <th>Hp Penerima</th>
                                 <th>Nama Penerima</th>
                                 <th>status</th>
                                 <th>message</th>
                                 </tr>
                                 </thead>
                                 <tbody>
                                         <?php 
                                 $no = 1; 
                                 foreach ($content as $row){ 
                             ?>
                                 <tr>
                                     <td data-title="#"><?=($page*$perpage)+$no;?></td>
                                     <td data-title="Date"><?=$row->created_at;?></td>
                                     <td data-title="RequestId"><?=$row->requestId;?></td>
                                     <td data-title="Amount"><?=$row->amount;?></td>
                                     <td data-title="Fee"><?=$row->fee;?></td>
                                     <td data-title="Total"><?=$row->total;?></td>
                                     <td data-title="Kode Bank"><?=$row->kode_bank;?></td>
                                     <td data-title="No Rekening"><?=$row->no_rekening;?></td>
                                     <td data-title="No Penerima"><?=$row->nohp_penerima;?></td>
                                     <td data-title="Nama Penerima"><?=$row->nama_penerima;?></td>
                                     <td data-title="status"><?=$row->status;?></td>
                                     <td data-title="Message"><?=$row->message;?></td>
                                 </tr>                         
                             <?php 
                                 $no++; 
                                 }
                             ?>
                                 </tbody>
                         </table>        	
                 </div>  
                 </section>
            <!--
                <div align="right" style=" margin-top: -25px;">
                    <?//=$pagination;?>
                </div>   
            -->
        </div>
    </div>      
    </div>                       
</div>
</div>
</section>
  
<!-- Modal Hapus -->
<div class="modal fade" id="deleteCodeModal" tabindex="-1" role="dialog" aria-labelledby="labelDeleteCode">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title" id="labelDeleteCode">Hapus Data Product</h4>
            </div>
            <div class="modal-body">  
                <i class="fa fa-lg fa-fw fa-warning"></i>Yakin mau hapus data Product <span id="namaCodeHapus"></span> ?                  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="deleteCodeModalYes" data-code-id="">Ya</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal Notifikasi -->
<div class="modal fade" id="notifikasiCodeModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiCode">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="labelNotifikasiCode">Notifikasi</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="statusNotifikasiCode" style="display: none;"></span>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiCodeOK">OK</button>
            </div>
        </div>
    </div>
</div>

<script>
$(function () {
    $("#tbinquiry").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
    
$('#daterange').daterangepicker({format: 'YYYY/MM/DD'});

$("#excel").click(function() {   
    if ( $('#daterange').val()=='' ){
        $('#daterange').focus();
        return false;
    }
    
   window.location="<?=site_url('cta/export_inquiry/')?>?daterange="+$('#daterange').val();
});
</script>