<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en-us" id="extr-page">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="">
	<meta name="author" content="">			
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title><?=$title;?></title>
    
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?=$css?>bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?=$css?>font-awesome.min.css">

    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
	<link rel="stylesheet" type="text/css" media="screen" href="<?=$css?>smartadmin-production.min.css">
	<link rel="stylesheet" type="text/css" media="screen" href="<?=$css?>smartadmin-skins.min.css">

	<link rel="icon" href="<?=$images;?>favicon.ico" type="image/x-icon">  
    <link rel="shortcut icon" href="<?=$images;?>favicon.ico" type="image/x-icon">
    
    <!-- #GOOGLE FONT -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
	
    <!-- #APP SCREEN / ICONS -->
	<!-- Specifying a Webpage Icon for Web Clip 
		 Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
	<link rel="apple-touch-icon" href="<?=$img;?>splash/sptouch-icon-iphone.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?=$img;?>splash/touch-icon-ipad.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?=$img;?>splash/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?=$img;?>splash/touch-icon-ipad-retina.png">
	
	<!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	
	<!-- Startup image for web apps -->
	<link rel="apple-touch-startup-image" href="<?=$img;?>splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
	<link rel="apple-touch-startup-image" href="<?=$img;?>splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
	<link rel="apple-touch-startup-image" href="<?=$img;?>splash/iphone.png" media="screen and (max-device-width: 320px)">
	
    <!--
    <script type="text/javascript" src="<?=$js;?>jquery-1.8.0.min.js"></script>       
    <script type="text/javascript" src="<?=$js;?>bootstrap.min.js"></script>    
    <script type="text/javascript" src="<?=$js;?>bootstrap-datepicker.js"></script>    
    <script type="text/javascript" src="<?=site_url('reports/js')?>"></script>  
    -->
    
</head>

<body class="animated fadeInDown">

	<header id="header">

		<div id="logo-group">
                    <span id="logo"><a href="<?=site_url('login')?>" title="Dashboard"> <img src="<?=$img;?>logo-blacknwhite.png" alt="SmartAdmin"></a> </span>
		</div>

		<span id="extr-page-header-space"> <span class="hidden-mobile">Need an account?</span> <a href="register.html" class="btn btn-danger">Create account</a> </span>

	</header>

	<div id="main" role="main">

		<!-- MAIN CONTENT -->
		<div id="content" class="container">

			<div class="row">
				
				<div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
					<div class="well no-padding">
						<div action="<?=site_url('forgot_password/doforgot')?>" id="login-form" class="smart-form client-form" method="post">
							<header>
								Signup Success
							</header>
                            
							<fieldset>
								
								<section>
									<label class="label">
                                        You have successfully signup. <br />    
                                        Before you can login, you must activate your account <br />
                                        by clicking the url sent to your email.           
                                    </label>
								</section>
                                
							</fieldset>
							<footer>
                                <a class="btn btn-primary" href="<?=site_url('login')?>">Login</a>
							</footer>
						</div>

					</div>
										
				</div>
			</div>
		</div>

	</div>
	

</body>

</html>