<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en-us" id="extr-page">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="">
	<meta name="author" content="">			
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title><?=$title;?></title>
    
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?=$css?>bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?=$css?>font-awesome.min.css">

    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
	<link rel="stylesheet" type="text/css" media="screen" href="<?=$css?>smartadmin-production.min.css">
	<link rel="stylesheet" type="text/css" media="screen" href="<?=$css?>smartadmin-skins.min.css">

	<link rel="icon" href="<?=$images;?>favicon.ico" type="image/x-icon">  
    <link rel="shortcut icon" href="<?=$images;?>favicon.ico" type="image/x-icon">
    
    <!-- #GOOGLE FONT -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
	
    <!-- #APP SCREEN / ICONS -->
	<!-- Specifying a Webpage Icon for Web Clip 
		 Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
	<link rel="apple-touch-icon" href="<?=$img;?>splash/sptouch-icon-iphone.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?=$img;?>splash/touch-icon-ipad.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?=$img;?>splash/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?=$img;?>splash/touch-icon-ipad-retina.png">
	
	<!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	
	<!-- Startup image for web apps -->
	<link rel="apple-touch-startup-image" href="<?=$img;?>splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
	<link rel="apple-touch-startup-image" href="<?=$img;?>splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
	<link rel="apple-touch-startup-image" href="<?=$img;?>splash/iphone.png" media="screen and (max-device-width: 320px)">
	
    <!--
    <script type="text/javascript" src="<?=$js;?>jquery-1.8.0.min.js"></script>       
    <script type="text/javascript" src="<?=$js;?>bootstrap.min.js"></script>    
    <script type="text/javascript" src="<?=$js;?>bootstrap-datepicker.js"></script>    
    <script type="text/javascript" src="<?=site_url('reports/js')?>"></script>  
    -->
    
</head>

<body class="animated fadeInDown">

	<header id="header">

		<div id="logo-group">
                    <span id="logo"><a href="<?=site_url('login')?>" title="Dashboard"> <img src="<?=$img;?>logo-blacknwhite.png" alt="SmartAdmin"></a> </span>
		</div>
        
	</header>

	<div id="main" role="main">

		<!-- MAIN CONTENT -->
		<div id="content" class="container">

			<div class="row">
				 
				<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
						<div class="well no-padding">

							<form enctype="multipart/form-data" action="<?=site_url('signup/dosignup')?>" id="form-signup" class="smart-form client-form" method="post">
								<header>
									Sign Up
								</header>
                                
								<fieldset>
        
                                    <? if ( isset($error_signup) && !empty($error_signup) ) { ?>
                                    <div class="alert alert-warning fade in">
        								<button class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
        								<i class="fa-fw fa fa-warning"></i>
        								<strong>Warning</strong> <?=$error_signup?>
        							</div>
                                    <? } ?>
                                    
       	                            <div class="row">
										<section class="col col-6">
											<label class="select">
												<select name="type_signup" id="type_signup">
													<option value="0" selected="" disabled="">Type Signup</option>
													<option value="Advertiser">Advertiser</option>
													<option value="Publisher">Publisher</option>
												</select> <i></i> 
                                            </label>
										</section>
										<section class="col col-6">
											<label class="select">
												<select name="status_signup">
													<option value="0" selected="" disabled="">Status Signup</option>
													<option value="Active">Active</option>
													<option value="Inactive">Inactive</option>
												</select> <i></i> 
                                            </label>
										</section>
									</div>
                                </fieldset>
                                
                                <fieldset>
									<div class="row">
                                   	    <section class="col col-6">
    										<label class="input"> <i class="icon-append fa fa-user"></i>
   											<input type="text" name="name" placeholder="Name">
    										<b class="tooltip tooltip-bottom-right">Needed to show name on the website</b> </label>
    									</section>
                                   	    <section class="col col-6">
    										<label class="input"> <i class="icon-append fa fa-phone"></i>
   											<input type="text" name="phone" placeholder="Phone">
    										<b class="tooltip tooltip-bottom-right">Needed to give information by phone</b> </label>
    									</section>
                                    </div>
                                    <div class="row">
                                   	    <section class="col col-6">
    										<label class="input"> <i class="icon-append fa fa-home"></i>
   											<input type="text" name="address1" placeholder="Address 1">
    										<b class="tooltip tooltip-bottom-right">Needed to show Address 1 on the website</b> </label>
    									</section>
                                   	    <section class="col col-6">
    										<label class="input"> <i class="icon-append fa fa-fax"></i>
   											<input type="text" name="fax" placeholder="Fax">
    										<b class="tooltip tooltip-bottom-right">Needed to give information by fax</b> </label>
    									</section>
                                    </div>
                                    <div class="row">
                                   	    <section class="col col-6">
    										<label class="input"> <i class="icon-append fa fa-home"></i>
   											<input type="text" name="address2" placeholder="Address 2">
    										<b class="tooltip tooltip-bottom-right">Needed to show Address 2 on the website</b> </label>
    									</section>
                                   	    <section class="col col-6">
    										<label class="input"> <i class="icon-append fa fa-info"></i>
   											<input type="text" name="ref_id" placeholder="Customer Ref ID">
    										<b class="tooltip tooltip-bottom-right">Needed to show Customer Ref ID</b> </label>
    									</section>
                                    </div>
                                    <div class="row">
                                        <div class="col col-6">
                                            <div class="row">
                                                <section class="col col-6">
            										<label class="input"> <i class="icon-append fa fa-send"></i>
           											<input type="text" name="zipcode" placeholder="ZIP Code">
            										<b class="tooltip tooltip-bottom-right">Needed to show ZIP Code</b> </label>
            									</section>
                                                <section class="col col-6">
            										<label class="input"> <i class="icon-append fa fa-globe"></i>
           											<input type="text" name="city" placeholder="City">
            										<b class="tooltip tooltip-bottom-right">Needed to show City</b> </label>
            									</section>
                                            </div>
                                        </div>                                   	    
                                   	    <section class="col col-6">
    										<label class="input"> <i class="icon-append fa fa-list"></i>
   											<input type="text" name="description" placeholder="Description">
    										<b class="tooltip tooltip-bottom-right">Needed to show Description</b> </label>
    									</section>
                                    </div>
                                    <div class="row">
                                   	    <section class="col col-6">
    										<label class="input"> <i class="icon-append fa fa-globe"></i>
   											<input type="text" name="country" placeholder="Country">
    										<b class="tooltip tooltip-bottom-right">Needed to show country on the website</b> </label>
    									</section>
                                   	    <section class="col col-6">
    										<label class="input"> <i class="icon-append fa fa-external-link"></i>
   											<input type="text" name="url" placeholder="URL">
    										<b class="tooltip tooltip-bottom-right">Needed to show URL</b> </label>
    									</section>
                                    </div>
								</fieldset>
                                
                                <fieldset id="field_publisher" style="display: none;">
									<div class="row">
                                   	    <section class="col col-6">
    										<label class="input"> <i class="icon-append fa fa-briefcase"></i>
   											<input type="text" name="company" placeholder="Company">
    										<b class="tooltip tooltip-bottom-right">Needed to show company</b> </label>
    									</section>
                                        <section class="col col-6">
    										<label class="input"> <i class="icon-append fa fa-external-link"></i>
   											<input type="text" name="web_url" placeholder="Web URL">
    										<b class="tooltip tooltip-bottom-right">Needed to show company web url</b> </label>
    									</section>
                                    </div>
                                    <div class="row">
                                   	    <section class="col col-6">
                                            <label class="select">
												<select name="category">
													<option value="0" selected="" disabled="">Category</option>
													<option value="News">News</option>
													<option value="Etcetera">Etcetera</option>
												</select> <i></i> 
                                            </label>
    									</section>
                                        <section class="col col-6">
    										<label class="input"> <i class="icon-append fa fa-external-link"></i>
   											<input type="text" name="mobile_site" placeholder="Mobile Site">
    										<b class="tooltip tooltip-bottom-right">Needed to show mobile site</b> </label>
    									</section>
                                    </div>
									<div class="row">
                                   	    <section class="col col-6">
    										<label class="input"> <i class="icon-append fa fa-bank"></i>
   											<input type="text" name="bank_account" placeholder="Bank Account">
    										<b class="tooltip tooltip-bottom-right">Needed to show bank account</b> </label>
    									</section>
                                        <section class="col col-6">
    										<label class="input"> <i class="icon-append fa fa-certificate"></i>
   											<input type="text" name="npwp" placeholder="NPWP">
    										<b class="tooltip tooltip-bottom-right">Needed to show npwp</b> </label>
    									</section>
                                    </div>
									<div class="row">
                                   	    <section class="col col-6">
    										<label class="input"> <i class="icon-append fa fa-certificate"></i>
   											<input type="text" name="pageview" placeholder="Last Month Pageview">
    										<b class="tooltip tooltip-bottom-right">Needed to show last month pageview</b> </label>
    									</section>
                                        <section class="col col-6">
        									<div class="input input-file">
        										<span class="button">
                                                    <input id="screnshoot" type="file" name="screnshoot" onchange="$('#label_screenshot').val(this.value)">Browse
                                                </span>
                                                <input type="text" id="label_screenshot" placeholder="Goggle Analytics Screenshot" readonly="">
        									</div>
        								</section>
                                    </div>
								</fieldset>
                                
                                <fieldset>
									<div class="row">
                                   	    <section class="col col-6">
    										<label class="input"> <i class="icon-append fa fa-user"></i>
   											<input type="text" name="username" placeholder="Username">
    										<b class="tooltip tooltip-bottom-right">Needed to enter the website</b> </label>
    									</section>
                                        <section class="col col-6">
    										<label class="input"> <i class="icon-append fa fa-envelope"></i>
   											<input type="email" name="email" placeholder="Email address">
    										<b class="tooltip tooltip-bottom-right">Needed to verify your account</b> </label>
    									</section>
                                    </div>
                                    <div class="row">
                                   	    <section class="col col-6">
    										<label class="input"> <i class="icon-append fa fa-lock"></i>
											<input type="password" name="password" placeholder="Password" id="password">
											<b class="tooltip tooltip-bottom-right">Don't forget your password</b> </label>
    									</section>
                                        <section class="col col-6">
    										<label class="input"> <i class="icon-append fa fa-lock"></i>
											<input type="password" name="passwordConfirm" placeholder="Confirm password">
											<b class="tooltip tooltip-bottom-right">Don't forget confirm your password</b> </label>
    									</section>
                                    </div>
								</fieldset>
                                
								<footer>
									<button type="submit" class="btn btn-primary">
										Register
									</button>
								</footer>

								<div class="message">
									<i class="fa fa-check"></i>
									<p>
										Thank you for your registration!
									</p>
								</div>
							</form>

						</div>
                        
					</div>
										
				</div>
			</div>
		</div>

	</div>

	<!--================================================== -->	

	<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
	<script src="<?=$js;?>plugin/pace/pace.min.js"></script>

    <!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script> if (!window.jQuery) { document.write('<script src="js/libs/jquery-2.0.2.min.js"><\/script>');} </script>

    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
	<script> if (!window.jQuery.ui) { document.write('<script src="js/libs/jquery-ui-1.10.3.min.js"><\/script>');} </script>

	<!-- IMPORTANT: APP CONFIG -->
	<script src="<?=$js;?>app.config.js"></script>

	<!-- JS TOUCH : include this plugin for mobile drag / drop touch events 		
	<script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

	<!-- BOOTSTRAP JS -->		
	<script src="<?=$js;?>bootstrap/bootstrap.min.js"></script>

	<!-- JQUERY VALIDATE -->
	<script src="<?=$js;?>plugin/jquery-validate/jquery.validate.min.js"></script>
	
	<!-- JQUERY MASKED INPUT -->
	<script src="<?=$js;?>plugin/masked-input/jquery.maskedinput.min.js"></script>
	
	<!--[if IE 8]>
		
		<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
		
	<![endif]-->

	<!-- MAIN APP JS FILE -->
	<script src="<?=$js;?>app.min.js"></script>

	<script type="text/javascript">
		runAllForms();

		$(function() {
			// Validation
			$("#form-signup").validate({
				// Rules for form validation
				rules : {
					type_signup : {
						required : true
					},
                    status_signup : {
						required : true
					},
                    name : {
						required : true
					},
					phone : {
						required : true
					},
					username : {
						required : true
					},
					email : {
						required : true,
						email : true
					},
					password : {
						required : true,
						minlength : 3,
						maxlength : 20
					},
                    passwordConfirm : {
						required : true,
						minlength : 3,
						maxlength : 20,
                        equalTo : "#password"
					}
				},

				// Messages for form validation
				messages : {
					type_signup : {
						required : 'Please enter type of signup'
					},
					status_signup : {
						required : 'Please enter status of signup'
					},
					name : {
						required : 'Please enter your name'
					},
					phone : {
						required : 'Please enter your phone'
					},
					username : {
						required : 'Please enter your username'
					},
					email : {
						required : 'Please enter your email',
						email : 'Please enter a VALID email address'
					},
					password : {
						required : 'Please enter your password'
					},
                    passwordConfirm : {
						required : 'Please enter your konfirmasi password',
                        equalTo : 'Please enter same password and konfirmasi password.'
					}
				},

				// Do not change code below
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
			});
		});
	</script>
    
    <script type="text/javascript">
        $("#type_signup").change(function() {
    		if ( $(this).val() == 'Publisher' )  {
                $('#field_publisher').show();
    		} else {
  		        $('#field_publisher').hide();
    		}
    	});	
    </script>

</body>

</html>