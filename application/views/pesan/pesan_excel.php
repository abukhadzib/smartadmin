<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en-us">
<head>
</head>
<body>
    <table class="table table-bordered table-hover table-striped">
    	<thead>
			<tr>
				<th>#</th>
				<th>Tanggal</th>
				<th>MSISDN</th>
				<th>Partner</th>
				<th>Program</th>
				<th>Sub Program</th>
				<th>Pesan</th>
				<th>Pesan Filter</th>
			</tr>
		</thead>
		<tbody>
			<?php 
                $no = 1; 
                if ( count($content) > 0 ) {
                foreach ($content as $row){ 
            ?>
                <tr>
                    <td><?=$no;?></td>
                    <td><?=date("d-m-Y H:i:s", strtotime($row->created_date));?></td>
                    <td><?="'".$row->msisdn;?></td>
                    <td><?=$row->partner_name;?></td>
                    <td><?=$row->program_name;?></td>
                    <td><?=$row->subprogram_name;?></td>
                    <td><?=$row->pesan;?></td>
                    <td><?=$row->pesan_filter;?></td>
                </tr>                         
            <?php 
                $no++; 
                } } else {
            ?>
                <tr>
                    <td colspan="9"style="text-align: center;">No Data</td>
                </tr>
            <?php } ?>
		</tbody>
    </table>   
</body>