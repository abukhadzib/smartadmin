<br />
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <form enctype="multipart/form-data" id="form-pesan" class="form-horizontal" method="post">
			<input type="hidden" name="pesan_id" value="<?=$pesan->id?>">
            <div class="form-group">
                <label for="tgl" class="col-xs-2 control-label">Tanggal *</label>
                <div class="col-xs-4">
                    <input type="text" class="form-control" id="tgl" name="tgl" placeholder="Tanggal" value="<?=date("d-m-Y H:i:s", strtotime($pesan->created_date))?>" disabled>
                </div>
                <label for="msisdn" class="col-xs-2 control-label">MSISDN</label>
                <div class="col-xs-4">
                    <input type="text" class="form-control" id="msisdn" name="msisdn" placeholder="MSISDN" value="<?=$pesan->msisdn?>" disabled>
                </div>
            </div>              
            <div class="form-group">
                <label for="partner" class="col-xs-2 control-label">Partner</label>
                <div class="col-xs-10">
                    <input type="text" class="form-control" id="partner" name="partner" placeholder="Partner" value="<?=$pesan->partner_name?>" disabled>
                </div>
            </div>               
            <div class="form-group">
                <label for="program" class="col-xs-2 control-label">Program</label>
                <div class="col-xs-4">
                    <input type="text" class="form-control" id="program" name="program" placeholder="Program" value="<?=$pesan->program_name?>" disabled>
                </div>
                <label for="subprogram" class="col-xs-2 control-label">Sub Program</label>
                <div class="col-xs-4">
                    <input type="text" class="form-control" id="subprogram" name="subprogram" placeholder="Sub Program" value="<?=$pesan->subprogram_name?>" disabled>
                </div>
            </div>     
            <div class="form-group">
                <label for="pesan" class="col-xs-2 control-label">Pesan</label>
                <div class="col-xs-10">
                    <textarea class="form-control" id="pesan" name="pesan" placeholder="Pesan" disabled><?=$pesan->pesan?></textarea>
                    <em><?=strlen($pesan->pesan)?></em>
                </div>
            </div>      
            <div class="form-group">
                <label for="pesan_filter" class="col-xs-2 control-label">Pesan Filter</label>
                <div class="col-xs-10">
                    <textarea class="form-control" id="pesan_filter" name="pesan_filter" placeholder="Pesan Filter"><?=$pesan->pesan_filter?></textarea>
                    <em id="sms_response_count"><?=strlen($pesan->pesan)?></em>
                </div>
            </div> 
            <em>*) Wajib diisi.</em>
		</form>
	</div>  
</div>
