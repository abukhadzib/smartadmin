
<!-- breadcrumb -->
<ol class="breadcrumb">
	<li>User Management </li><li>Role</li>
</ol>
<!-- end breadcrumb -->

</div>
<!-- END RIBBON -->

<!-- MAIN CONTENT -->
<div id="content">

<div class="row">
	<div class="col-xs-12">
		<h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-envelope"></i> Pesan <span> > Lihat pesan</span></h3>
	</div>
    <div class="col-xs-12">
        <form enctype="multipart/form-data" id="form-search" class="form-inline" method="post">
            <div class="panel panel-default">
                <div class="panel-heading">Filter & Search</div>
                <div class="panel-body">                      
                    <div class="form-group">
                        <input type="text" class="form-control" id="date1" name="date1" placeholder="Tgl Awal" style="width: 100px;" value="<?=isset($date1_view_pesan_mesinpesan) && !empty($date1_view_pesan_mesinpesan) ? $date1_view_pesan_mesinpesan : ''?>" readonly="readonly">
                        <span class="add-on">to</span>
                        <input type="text" class="form-control" id="date2" name="date2" placeholder="Tgl Akhir" style="width: 100px;" value="<?=isset($date2_view_pesan_mesinpesan) && !empty($date2_view_pesan_mesinpesan) ? $date2_view_pesan_mesinpesan : ''?>" readonly="readonly">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="msisdn" name="msisdn" placeholder="MSISDN" value="<?=isset($msisdn_view_pesan_mesinpesan) && !empty($msisdn_view_pesan_mesinpesan) ? $msisdn_view_pesan_mesinpesan : ''?>" style="width: 110px;">
                    </div>                    
                    <div class="form-group">
                        <select name="partner" class="form-control" id="partner">
    						<option value=""<?=isset($partner_view_pesan_mesinpesan) && empty($partner_view_pesan_mesinpesan) ? ' selected="selected"' : ''?>> --Partner--</option>
                            <? if ($partner) { foreach($partner as $row_partner) { ?>
    						<option value="<?=$row_partner->id?>"<?=isset($partner_view_pesan_mesinpesan) && $partner_view_pesan_mesinpesan==$row_partner->id ? ' selected="selected"' : ''?>><?=$row_partner->name?></option>
                            <? } } ?>
    					</select>
                    </div>                                        
                    <div class="form-group">
                        <select name="program" class="form-control" id="program">
    						<option value=""<?=isset($program_view_pesan_mesinpesan) && empty($program_view_pesan_mesinpesan) ? ' selected="selected"' : ''?>> --Program--</option>
                            <? if ($program) { foreach($program as $row_program) { ?>
    						<option value="<?=$row_program->id?>"<?=isset($program_view_pesan_mesinpesan) && $program_view_pesan_mesinpesan==$row_program->id ? ' selected="selected"' : ''?>><?=$row_program->name?></option>
                            <? } } ?>
    					</select>
                    </div>             
                    <div class="form-group">                       
                        <select name="subprogram" class="form-control" id="subprogram">
    						<option value=""<?=isset($subprogram_view_pesan_mesinpesan) && empty($subprogram_view_pesan_mesinpesan) ? ' selected="selected"' : ''?>> --Sub Program--</option>
                            <? if ($subprogram) { foreach($subprogram as $row_subprogram) { ?>
    						<option value="<?=$row_subprogram->id?>"<?=isset($subprogram_view_pesan_mesinpesan) && $subprogram_view_pesan_mesinpesan==$row_subprogram->id ? ' selected="selected"' : ''?>><?=$row_subprogram->name?></option>
                            <? } } ?>
    					</select>
                    </div>  
                    <div class="form-group">
                        <input type="text" class="form-control" id="pesan" name="pesan" placeholder="Pesan" value="<?=isset($pesan_view_pesan_mesinpesan) && !empty($pesan_view_pesan_mesinpesan) ? $pesan_view_pesan_mesinpesan : ''?>" style="width: 160px;">
                    </div>       
                </div>
                <div class="panel-footer" style="text-align: right;">
                    <button type="button" class="btn btn-success" id="pesan-excel">Export to Excel</button>
                    <button type="submit" class="btn btn-primary">Search</button>
                    <button type="button" class="btn btn-info" id="clear-search-pesan-view">Clear Search</button>
                </div>
            </div>
        </form>
    </div> 
    <div class="col-xs-12">                     
        <div class="table-responsive">												
        	<table class="table table-bordered table-hover table-striped" style="margin-bottom: auto;">
        		<thead>
        			<tr>
        				<th>#</th>
        				<th>Tanggal</th>
        				<th>MSISDN</th>
        				<th>Partner</th>
        				<th>Program</th>
        				<th>Sub Program</th>
        				<th>Pesan</th>
        				<th>Pesan Filter</th>
                        <th style="width: 5%;">Action</th>
        			</tr>
        		</thead>
        		<tbody>
        			<?php 
                        $no = 1; 
                        if ( count($content) > 0 ) {
                        foreach ($content as $row){ 
                    ?>
                        <tr>
                            <td><?=($page*$perpage)+$no;?></td>
                            <td><?=date("d-m-Y H:i:s", strtotime($row->created_date));?></td>
                            <td><?=$row->msisdn;?></td>
                            <td><?=$row->partner_name;?></td>
                            <td><?=$row->program_name;?></td>
                            <td><?=$row->subprogram_name;?></td>
                            <td><?=$row->pesan;?></td>
                            <td><?=$row->pesan_filter;?></td>
                            <td style="text-align: center;">
                                <a class="editPesan" href="#" title="Edit" data-pesan-id="<?=$row->id;?>" data-toggle="modal" data-target="#dataPesanModal"><i class="fa fa-edit"></i></a>
                            </td>
                        </tr>                         
                    <?php 
                        $no++; 
                        } } else {
                    ?>
                        <tr>
                            <td colspan="9"style="text-align: center;">No Data</td>
                        </tr>
                    <?php } ?>
        		</tbody>
        	</table>        	
        </div>       
        <div align="right" style=" margin-top: -25px;">
            <?=$pagination;?>
        </div>           
    </div>    
</div>                       

<!-- Modal Hapus -->
<div class="modal fade" id="deletePesanModal" tabindex="-1" role="dialog" aria-labelledby="labelDeletePesan">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title" id="labelDeletePesan">Hapus Data Pesan</h4>
            </div>
            <div class="modal-body">  
                <i class="fa fa-lg fa-fw fa-warning"></i>Yakin mau hapus data Pesan <span id="namaPesanHapus"></span> ?                  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="deletePesanModalYes" data-Pesan-id="">Ya</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Data Tambah dan Edit -->
<div class="modal fade" id="dataPesanModal" tabindex="-1" role="dialog" aria-labelledby="labelDataPesan">
    <div class="modal-dialog" role="document" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <!--button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title"><span id="labelDataPesan"></span> Data Pesan <span id="loading-data" style="display:none;margin-left: 10px;"><img src="<?=base_url('assets/img/loading.gif')?>" /></span></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="dataPesanModalURL" style="display: none;"></span>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="simpanPesanModal" data-Pesan-id="">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Notifikasi -->
<div class="modal fade" id="notifikasiPesanModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiPesan">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="labelNotifikasiPesan">Notifikasi</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="statusNotifikasiPesan" style="display: none;"></span>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiPesanOK">OK</button>
            </div>
        </div>
    </div>
</div>

<script>
    setInterval(function(){ location.reload(); }, 30000);	   
</script>