
        <nav>
            <ul>
                <? foreach($menu_role as $menu_row){ ?>  
					
                <li>
                    <a href="<?=isset($menu_row['submenu'])?'#':site_url($menu_row['menu']->url)?>">
                        <i class="<?=$menu_row['menu']->icon;?>"></i> 
                        <span class="menu-item-parent"><?=$menu_row['menu']->name;?></span>
                        <? if ($menu_row['menu']->notifikasi>0){?>
                        &nbsp;&nbsp;<span class="badge"> <?=$menu_row['menu']->notifikasi;?> </span>
                        <? } ?>
                    </a>
                    <? if ( isset($menu_row['submenu']) ) { ?>
                    <ul>
                    <?    foreach($menu_row['submenu'] as $submenu_row){ ?>
                        <li>
							<a href="<?=site_url($submenu_row->url)?>"><i class="<?=$submenu_row->icon;?>"></i><?=$submenu_row->name;?>
                                <? if ($submenu_row->notifikasi>0){?>
                                <span class="badge bg-color-greenLight pull-right inbox-badge"> <?=$submenu_row->notifikasi;?> </span>
                                <? } ?>
                            </a>                            
						</li>
                    <? } ?> 
                    </ul>
                    <? } ?>
				</li>
				<? } ?>
            </ul>
		</nav>
		<span class="minifyme" data-action="minifyMenu"> 
			<i class="fa fa-arrow-circle-left hit"></i> 
		</span>
        