<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['IpWhiteList'] = array(
    
    // Localhost
    "127.0.0.1" => "Localhost",
    "::1" => "Localhost",
    
    // IP TAPS Core
    "192.168.70.102" => "TAPS Core",    
    
);