<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

include('httpful-0.2.0.phar');

class api {

    
     //protected $url            = "http://124.66.160.98/api_remitance";
     protected $url            = "http://localhost/api_remitance";
	

	
        public function inqKirim($data){
		try{
			$result = \Httpful\Request::post($this->url."/kirimuang/inquiry")
			->addHeader('Content-Type', 'application/x-www-form-urlencoded')
                        ->body(http_build_query($data))->send();
			log_message('debug', 'Inquiry: ' .$result);
			$response = $result->body;
			return $response;
		} catch (Exception $e) {
        	//log_message('error', $e->getMessage());
        	//$this->show_error($e->getMessage());
        	return 'FAILED';
        }

	}
        
        public function payKirim($data){
		try{
			$result = \Httpful\Request::post($this->url."/kirimuang/payment")
			->addHeader('Content-Type', 'application/x-www-form-urlencoded')
                        ->body(http_build_query($data))->send();
			log_message('debug', 'Payment: ' .$result);
			$response = $result->body;
			return $response;
		} catch (Exception $e) {
        	//log_message('error', $e->getMessage());
        	//$this->show_error($e->getMessage());
        	return 'FAILED';
        }

	}
        
        public function inqAmbil($data){
		try{
			$result = \Httpful\Request::post($this->url."/ambiluang/inquiry")
			->addHeader('Content-Type', 'application/x-www-form-urlencoded')
                        ->body(http_build_query($data))->send();
			log_message('debug', 'Inquiry: ' .$result);
			$response = $result->body;
			return $response;
		} catch (Exception $e) {
        	//log_message('error', $e->getMessage());
        	//$this->show_error($e->getMessage());
        	return 'FAILED';
        }

	}
        
        public function payAmbil($data){
		try{
			$result = \Httpful\Request::post($this->url."/ambiluang/payment")
			->addHeader('Content-Type', 'application/x-www-form-urlencoded')
                        ->body(http_build_query($data))->send();
			log_message('debug', 'Payment: ' .$result);
			$response = $result->body;
			return $response;
		} catch (Exception $e) {
        	//log_message('error', $e->getMessage());
        	//$this->show_error($e->getMessage());
        	return 'FAILED';
        }

	}
        
	public function sendSMS($data){
		$result = \Httpful\Request::post("http://mc-stars.co.id/sms/index.php/sendSMS")
			->addHeader('Content-Type', 'application/x-www-form-urlencoded')
			->body(http_build_query($data))
			->send();
			return true;
		
	}
		

}
