<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

include('httpful-0.2.0.phar');

class Apimerchant {

    
     protected $url            = "http://localhost/api_remitance";
	

	public function regMerchant($nama,$msisdn,$usaha,$type,$identitas,$alamat,$kodeMerchant,$bank,$rekening,$kodePos,$email){
        $CI =& get_instance();
		
        if(substr($msisdn, 0, 1) === "0" && substr($msisdn, 0, 2) !== "62") {
            $msisdn = substr_replace($msisdn,"62",0,1);
        }
        
        $time = time();
        $webPin = $CI->config->item('merchantWebPin');
        $pin    = $CI->config->item('merchantPin');
        $secret = strrev($webPin).strrev($pin)."koesplus".$time;
        $secure = md5($secret);
        
	$text = "RM.".$nama.'.'.$msisdn.'."'.$email.'".'.$usaha.'.'.$type.'.'.$identitas.'."'.$alamat.'".'.$kodeMerchant.'.'.$bank.'.'.$rekening.'.'.$kodePos.'.'.md5($CI->config->item('merchantWebPin')).'.'.md5($CI->config->item('merchantPin'));
        $smscid = 'IN%2DGPRS';
        $param = "secure=".$secure."&Text=".urlencode($text)."&hair=".$time."&msisdn=".$msisdn."&SMSCID=".$smscid."&PhoneNumber=".$msisdn;	
        
        /*
        log_message('error', 'TIME : ' .$time);
        log_message('error', 'WebPIN : ' .strrev($webPin));
        log_message('error', 'PIN : ' .strrev($pin));
        log_message('error', 'SECRET : ' .$secret);
        log_message('error', 'SECURE : ' .$secure);
        log_message('error', 'TEXT: ' .$text);
        log_message('error', 'PARAM : ' .$param);
        */
        
            try{
                    $url = $CI->config->item('merchantHost').$param;
                    $result = \Httpful\Request::get($CI->config->item('merchantHost').$param)
                    ->authenticateWith('username','password')
                    ->addHeader('Content-Type', 'application/x-www-form-urlencoded')
                    ->send();
                    log_message('error', '========= START REGISTER Merchant ========== ');
                    log_message('error', 'SEND TO ST24 : ' .$url);
                    log_message('error', 'RECV FROM ST24: ' .$result);
                    log_message('error', '========= FINISH REGISTER Merchant ========== ');
                    return $result;
            } catch (Exception $e) {
                    log_message('error', $e->getMessage());
                    $this->show_error($e->getMessage());
                    return false;
            }

	}
        
        public function regLKD($nama,$msisdn,$usaha,$type,$identitas,$alamat,$kodeMerchant,$bank,$rekening,$kodePos,$email){
        $CI =& get_instance();
		
        if(substr($msisdn, 0, 1) !== "0" && substr($msisdn, 0, 2) === "62") {
            $msisdn = substr_replace($msisdn,"0",0,2);
        }
        
        $time = time();
        $user   = $CI->config->item('merchantName');
        $webPin = $CI->config->item('merchantWebPin');
        $pin    = $CI->config->item('merchantPin');
        $secret = strrev($webPin).strrev($pin)."koesplus".$time;
        $secure = md5($secret);
        $smscid = 'IN%2DGPRS';
        
	//$text = "RM.".$nama.'.'.$msisdn.'."'.$email.'".'.$usaha.'.'.$type.'.'.$identitas.'."'.$alamat.'".'.$kodeMerchant.'.'.$bank.'.'.$rekening.'.'.$kodePos.'.'.md5($CI->config->item('merchantWebPin')).'.'.md5($CI->config->item('merchantPin'));
        $text = "R.".$nama.'.'.$msisdn.'.AP2.'.md5($CI->config->item('merchantWebPin')).'.'.md5($CI->config->item('merchantPin'));
        
        //$param = "secure=".$secure."&Text=".$text."&hair=".$time."&msisdn=".$msisdn."&SMSCID=".$smscid."&PhoneNumber=".$msisdn; 
        $param = "secure=".$secure."&Text=".$text."&hair=".$time."&msisdn=".$user."&SMSCID=".$smscid."&PhoneNumber=".$user;	
        
        
        /*
        log_message('error', 'TIME : ' .$time);
        log_message('error', 'WebPIN : ' .strrev($webPin));
        log_message('error', 'PIN : ' .strrev($pin));
        log_message('error', 'SECRET : ' .$secret);
        log_message('error', 'SECURE : ' .$secure);
        log_message('error', 'TEXT: ' .$text);
        log_message('error', 'PARAM : ' .$param);
        */
        
            try{
                    $url = $CI->config->item('merchantHost').$param;
                    $result = \Httpful\Request::get($CI->config->item('merchantHost').$param)
                    ->addHeader('Content-Type', 'application/x-www-form-urlencoded')
                    ->send();
                    log_message('error', '========= START REGISTER LKD ========== ');
                    log_message('error', 'SEND TO ST24 : ' .$url);
                    log_message('error', 'RECV FROM ST24: ' .$result);
                    log_message('error', '========= FINISH REGISTER LKD ========== ');
                    return $result;
            } catch (Exception $e) {
                    log_message('error', $e->getMessage());
                    $this->show_error($e->getMessage());
                    return false;
            }
	
	}
        
        public function transferMerchant($nominal,$msisdn){
        $CI =& get_instance();
		
        if(substr($msisdn, 0, 1) === "0" && substr($msisdn, 0, 2) !== "62") {
            $msisdn = substr_replace($msisdn,"62",0,1);
        }
        
        $time       = time();
        $username   = $CI->config->item('merchantName');
        $webPin     = $CI->config->item('merchantWebPin');
        $pin        = $CI->config->item('merchantPin');
        $secret = strrev($webPin).strrev($pin)."koesplus".$time;
        $secure = md5($secret);
	$text       = "TRF.".$nominal.'.'.$msisdn.'.'.md5($CI->config->item('merchantWebPin')).'.'.md5($CI->config->item('merchantPin'));
        $smscid     = 'IN%2DGPRS';
        $param      = "secure=".$secure."&Text=".$text."&hair=".$time."&msisdn=".$username."&SMSCID=".$smscid."&PhoneNumber=".$username;	
        
        /*
        log_message('error', 'TIME : ' .$time);
        log_message('error', 'WebPIN : ' .$webPin);
        log_message('error', 'PIN : ' .$pin);
        log_message('error', 'md5 WebPIN : ' .md5($webPin));
        log_message('error', ' md5 PIN : ' .md5($pin));
        log_message('error', 'SECRET : ' .$secret);
        log_message('error', 'SECURE : ' .$secure);
        log_message('error', 'TEXT: ' .$text);
        log_message('error', 'PARAM : ' .$param);
        */
        
            try{
                    $url = $CI->config->item('merchantHost').$param;
                    $result = \Httpful\Request::get($CI->config->item('merchantHost').$param)
                    ->addHeader('Content-Type', 'application/x-www-form-urlencoded')
                    ->send();
                    log_message('error', '========= START TRANSFER ========== ');
                    log_message('error', 'SEND TO ST24 : ' .$url);
                    log_message('error', 'RECV FROM ST24: ' .$result);
                    log_message('error', '========= FINISH TRANSFER ========== ');
                    return $result;
            } catch (Exception $e) {
                    log_message('error', $e->getMessage());
                    $this->show_error($e->getMessage());
                    return false;
            }
	
	}

        public function paymentMerchant($nominal,$msisdn,$requestid){
        $CI =& get_instance();
		
        if(substr($msisdn, 0, 1) === "0" && substr($msisdn, 0, 2) !== "62") {
            $msisdn = substr_replace($msisdn,"62",0,1);
        }
        
        $time       = time();
        $username   = $CI->config->item('merchantName');
        $webPin     = $CI->config->item('merchantWebPin');
        $pin        = $CI->config->item('merchantPin');
        $secret = strrev($webPin).strrev($pin)."koesplus".$time;
        $secure = md5($secret);
	//$text       = "BAYAR.".$nominal.".".$msisdn.".1234.".$CI->config->item('merchantPin');
        $text       = "BAYAR.".$nominal.'.'.$msisdn.'."'.$requestid.'".'.md5($CI->config->item('merchantWebPin')).'.'.md5($CI->config->item('merchantPin'));
        $smscid     = 'IN%2DGPRS';
        $param      = "secure=".$secure."&Text=".$text."&hair=".$time."&msisdn=".$username."&SMSCID=".$smscid."&PhoneNumber=".$username;	
       
        /*
        log_message('error', 'TIME : ' .$time);
        log_message('error', 'WebPIN : ' .$webPin);
        log_message('error', 'PIN : ' .$pin);
        log_message('error', 'md5 WebPIN : ' .md5($webPin));
        log_message('error', ' md5 PIN : ' .md5($pin));
        log_message('error', 'SECRET : ' .$secret);
        log_message('error', 'SECURE : ' .$secure);
        log_message('error', 'TEXT: ' .$text);
        log_message('error', 'PARAM : ' .$param);
        */
        
            try{
                    $url = $CI->config->item('merchantHost').$param;
                    $result = \Httpful\Request::get($CI->config->item('merchantHost').$param)
                    ->addHeader('Content-Type', 'application/x-www-form-urlencoded')
                    ->send();
                    log_message('error', '========= START PEMBAYARAN ========== ');
                    log_message('error', 'SEND TO ST24 : ' .$url);
                    log_message('error', 'RECV FROM ST24: ' .$result);
                    log_message('error', '========= FINISH PEMBAYARAN ========== ');
                    return $result;
            } catch (Exception $e) {
                    log_message('error', $e->getMessage());
                    $this->show_error($e->getMessage());
                    return false;
            }
	
	}
}
