<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function filter_words($text, $bad_words, $mask_char='*')
{
	if(is_array($bad_words))
	{
		$filter_terms = array();
		
		foreach($bad_words as $bw)
		{
			$filter_terms[] = '\b' . $bw . '\b'; //$bw; //'\b' . $bw . '\b';
		}
	}
	
    $filtered_text = $text;
	
    foreach($filter_terms as $word)
    {
        $match_count = preg_match_all('/' . $word . '/i', $text, $matches);
        //$match_count = preg_match_all('/' . $word . '/', $text, $matches);
        
        for($i = 0; $i < $match_count; $i++)
		{
			$bwstr = trim($matches[0][$i]);
			$filtered_text = preg_replace('/\b' . $bwstr . '\b/', substr($bwstr, 0, 1) . str_repeat($mask_char, (strlen($bwstr) - 2)) . substr($bwstr, (strlen($bwstr) - 1), 1), $filtered_text);
            //$filtered_text = preg_replace('/' . $bwstr . '/', substr($bwstr, 0, 1) . str_repeat($mask_char, (strlen($bwstr) - 2)) . substr($bwstr, (strlen($bwstr) - 1), 1), $filtered_text);
		}
    }
	
    return $filtered_text;
}

function unique_id($l = 20) 
{
    return substr(md5(uniqid(mt_rand(), true)), 0, $l);
}

function replaceMsg($msg)
{
	$msg = str_replace("&amp;"," ",$msg) ;
	$msg = str_replace("&nbsp;"," ",$msg) ;
	$msg = str_replace("&quot;"," ",$msg) ;
	$msg = str_replace("&#039;"," ",$msg) ;
	$msg = str_replace("&lt;"," ",$msg) ;
	$msg = str_replace("&gt;"," ",$msg) ;
	$msg = str_replace("&#59;"," ",$msg) ;
	$msg = str_replace("&#96;"," ",$msg) ;
	return $msg;
}

function createXML($data=array()){
	$strxml	= "<?xml version='1.0' encoding='UTF-8'?>";
	$strxml	= $strxml."<MSTARS>";
	$strxml	= $strxml."<MSISDN>".(isset($data['msisdn']) ? $data['msisdn'] : "")."</MSISDN>";
	$strxml	= $strxml."<RESPONSE>1</RESPONSE>";
	$strxml	= $strxml."<OPTION>".(isset($data['option']) ? $data['option'] : "")."</OPTION>";
	$strxml	= $strxml."<CHARGE>".(isset($data['charged']) ? $data['charged'] : "")."</CHARGE>";
	$strxml	= $strxml."<APPID>".(isset($data['program_id']) ? $data['program_id'] : "")."</APPID>";
	$strxml	= $strxml."<PARTNERID>".(isset($data['partner']) ? $data['partner'] : "")."</PARTNERID>";
	$strxml	= $strxml."<MEDIAID>".(isset($data['mediaId']) ? $data['mediaId'] : "")."</MEDIAID>";
	$strxml	= $strxml."<TRXID>".(isset($data['idmessage']) ? $data['idmessage'] : "")."</TRXID>";
	$strxml	= $strxml."<HPTYPE>".(isset($data['hpType']) ? $data['hpType'] : "ALL")."</HPTYPE>";
	$strxml	= $strxml."<SHORTNAME>".(isset($data['shortname']) ? $data['shortname'] : "")."</SHORTNAME>";
	$strxml	= $strxml."<CONTENTTYPE>".(isset($data['content_type']) ? $data['content_type'] : "")."</CONTENTTYPE>";
	$strxml	= $strxml."<PRIORITY>1</PRIORITY>";
	$strxml	= $strxml."<CONTENTID>".(isset($data['content_id']) ? $data['content_id'] : "")."</CONTENTID>";
	$strxml	= $strxml."<DESC>".(isset($data['desc']) ? $data['desc'] : "")."</DESC>";
	$strxml	= $strxml."<CONTENT>";
	$strxml	= $strxml."<SMS NUMBER='1'>".(isset($data['content_text']) ? $data['content_text'] : "")."</SMS>";
	$strxml	= $strxml."</CONTENT>";
	$strxml	= $strxml."</MSTARS>";
	return $strxml;
}