<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_user_management extends CI_Model {
    
    function __construct() 
    {
		parent::__construct();
	}
 
    function getUser($limit=15,$page=0,$filter=array(),$excel=false)
    {        
        $start = $page>0 ? $limit*$page : 0; 
        
        $sql = "SELECT *,(select name from role where id=user.role_id) 'role_name' from user";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
        
        if ($excel){
            return $data;
        } else {        
            $sql  .= " LIMIT ".$start.",".$limit;    
            $data  = $this->db->query($sql);             
            return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
        }
        
    }
 
    function getUserById($id)
    {
        $sql = "SELECT *,(select name from role where id=user.role_id) 'role_name' from user where id=$id";
        $data  = $this->db->query($sql);
        return $data->row();        
    }
	
    function cekUser($username='',$id='')
    {
        $query = $this->db->get_where('user', array('username' => $username,'id !=' => $id));
        if ($query->num_rows()>0){
            return true;
        } else {
            return false;
        }
    }
 
    function getRole($limit=15,$page=0,$filter=array(),$excel=false)
    {        
        $start = $page>0 ? $limit*$page : 0; 
        /*
        $sql = "SELECT 	c.*,
                    	(SELECT NAME FROM partner WHERE id=c.partner_id) 'partner_name',
                    	s.program_name
                FROM 	
                role c
                LEFT JOIN
                (
                	SELECT * FROM (
                		SELECT id, GROUP_CONCAT(program_name) 'program_name'
                		FROM (
                			SELECT 
                				id,
                				SUBSTRING_INDEX(SUBSTRING_INDEX(program_id, ',', n.digit+1), ',', -1) program_id,
                				( SELECT NAME FROM program WHERE id=SUBSTRING_INDEX(SUBSTRING_INDEX(program_id, ',', n.digit+1), ',', -1) ) program_name
                			FROM 
                				role a
                			INNER JOIN
                				(SELECT 0 digit UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7) n
                				ON LENGTH(REPLACE(program_id, ',' , '')) <= LENGTH(program_id)-n.digit  
                		) z
                		GROUP BY id
                	) t
                ) s
                ON c.id=s.id";
        */
        //$sql = "SELECT *,(select name from partner where id=role.partner_id) 'partner_name',(select name from program where id=role.program_id) 'program_name' from role";
        
        $sql = "SELECT * from role";
            
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
        
        if ($excel){
            return $data;
        } else {        
            $sql  .= " LIMIT ".$start.",".$limit;    
            $data  = $this->db->query($sql);             
            return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
        }
        
    }
 
    function getRoleById($id)
    {
        //$sql = "SELECT *,(select name from partner where id=role.partner_id) 'partner_name',(select name from program where id=role.program_id) 'program_name' from role where id=$id";
        $sql = "SELECT * from role where id=$id";
        $data  = $this->db->query($sql);
        return $data->row();        
    }
 
    function getRoles()
    {
        $sql = "SELECT * from role";
        $data  = $this->db->query($sql);
        return $data->result();        
    }
 
    function getRoleMenuById($id)
    {
        $sql = "SELECT * from role_menu where role_id=$id";
        $data  = $this->db->query($sql);
        return $data->result();        
    }
    
    function cekRoleExist($id)
    {
        $sql = "SELECT role_id FROM user WHERE role_id='$id'";                
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
        
        if ( $total > 0 ){
            return true;
        } else {
            return false;
        }
    }
 
    function getMenu()
    {        
        $sql = "SELECT * from menu order by sort";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
        
        $data  = $this->db->query($sql);             
        return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';        
    }
	
    function addUser($data)
    {
        return $this->db->insert('user', $data); 
    }
 
    function addRole($data)
    {
        return $this->db->insert('role', $data);         
    }
 
    function addRoleMenu($data)
    {
        return $this->db->insert_batch('role_menu', $data);         
    }
	
    function updateUser($data,$id)
    {
        return $this->db->update('user', $data, array('id' => $id)); 
    }
	
    function updateRole($data,$id)
    {
        return $this->db->update('role', $data, array('id' => $id)); 
    }
 
    function deleteUser($id)
    {
        return $this->db->delete('user', array('id'=>$id));         
    }
 
    function deleteRole($id)
    {
        return $this->db->delete('role', array('id'=>$id));         
    }
	
    function deleteRoleMenu($id)
    {
        return $this->db->delete('role_menu', array('role_id' => $id)); 
    }
	   
	function getMainMenu($limit=15,$page=0,$filter=array(),$excel=false)
    {        
        $start = $page>0 ? $limit*$page : 0; 
        
        $sql = "SELECT * from menu";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
        
        if ($excel){
            return $data;
        } else {        
            $sql  .= " LIMIT ".$start.",".$limit;    
            $data  = $this->db->query($sql);             
            return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
        }
        
    }
 
    function getMenuById($id)
    {
        $sql = "SELECT * from menu where id=$id";
        $data  = $this->db->query($sql);
        return $data->row();        
    }
	
    function cekMenu($name='',$id='')
    {
        $query = $this->db->get_where('menu', array('name'=>$name,'id !=' => $id));
        if ($query->num_rows()>0){
            return true;
        } else {
            return false;
        }
    }
 
    function deleteMenu($id)
    {
        return $this->db->delete('menu', array('id'=>$id));         
    }
 
    function updateMenu($data,$id)
    {
        return $this->db->update('menu', $data, array('id' => $id)); 
    }
	
    function addMenu($data)
    {
        return $this->db->insert('menu', $data); 
    }
    function getMenus()
    {
        $sql = "SELECT * from menu";
        $data  = $this->db->query($sql);
        return $data->result();        
    }
	
}