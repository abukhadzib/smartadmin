<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_reset_password extends CI_Model {
    
    function __construct() {
		parent::__construct();
	}
	
    function cekCode($code=''){
        $query = $this->db->get_where('t_reset_password', array('code' => $code, 'status' => '0'));
        if ($query->num_rows()>0){
            return true;
        } else {
            return false;
        }
    }
	
    function getResetPasswordData($code=''){
        $query = $this->db->get_where('t_reset_password', array('code' => $code, 'status' => '0'));
        if ($query->num_rows()>0){
            return $query->row();
        } else {
            return false;
        }
    }
	
    function updatePassword($email,$password){
        return $this->db->update('user', array('password' => md5($password)), array('email' => $email)); 
    }
	
    function updateResetPassword($code){
        return $this->db->update('t_reset_password', array('status' => '1'), array('code' => $code)); 
    }
	   
}