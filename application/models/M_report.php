<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_report extends CI_Model {
    
    function __construct() {
		parent::__construct();
	}
 
    function getActivity($limit=15,$page=0,$excel=false)
    {    
        $start = $page>0 ? $limit*$page : 0; 
        
        $sql = "SELECT *,(SELECT NAME FROM user WHERE id=t_log.userid) 'user_name' FROM t_log WHERE DATE_FORMAT(created_date,'%Y-%m-%d') > DATE_SUB(NOW(), INTERVAL 1 WEEK) ORDER BY id DESC";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
         
        if ($excel){
            return $data;
        } else {        
            $sql  .= " LIMIT ".$start.",".$limit;    
            $data  = $this->db->query($sql);             
            return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
        }   
    } 	

    function getHistory($limit=15,$page=0,$excel=false)
    {
    	$start = $page>0 ? $limit*$page : 0;
    
    	$sql = "SELECT * FROM transaksi WHERE user_id=$this->userid ORDER BY date DESC";
    	$data  = $this->db->query($sql);
    	$total = $data->num_rows();
    	 
    	if ($excel){
    		return $data;
    	} else {
    		$sql  .= " LIMIT ".$start.",".$limit;
    		$data  = $this->db->query($sql);
    		return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
    	}
    }
    
}