<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_api extends CI_Model {
    
    function __construct() {
		parent::__construct();
	}
 
    function cekMerchantId($code=''){
        
        $sql = "select id_merchant from merchant WHERE id_merchant='$code'";
        $query = $this->db->query($sql);
        if ($query->num_rows()>0){
            return true;
        } else {
            return false;
        }
    } 
    
    function cekInquiryId($trxid,$merchantCode){
        
        $sql = "select * from inquiry WHERE trxid = '$trxid' AND id_merchant = '$merchantCode' AND status = '0'";
        $query = $this->db->query($sql);
        if ($query->num_rows()>0){
            return true;
        } else {
            return false;
        }
    } 
    
    function addInquiry($data=array()){
        return $this->db->insert('inquiry', $data); 
    }
    
    function addTrx($data=array()){
        return $this->db->insert('transaksi', $data); 
    }
    
    function getMerchantByCode($merchantCode,$limit=15,$page=0,$excel=false)
    {
        $start = $page>0 ? $limit*$page : 0; 
        
        $sql = "SELECT id,id_merchant,name,phone,alamat,jenis_usaha,zipcode,email,discount,status,is_active FROM merchant WHERE id_merchant = '$merchantCode'";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
         
        if ($excel){
            return $data;
        } else {        
            $sql  .= " LIMIT ".$start.",".$limit;    
            $data  = $this->db->query($sql);             
            return '{"total":"'.$total.'","rows":'.json_encode($data->row_array()).'}';
        }       
    }
    
    function getIdDiscount($merchantCode,$limit=15,$page=0,$excel=false)
    {
       
        $sql = "SELECT id,id_merchant,name,phone,alamat,jenis_usaha,zipcode,email,discount,status,is_active FROM merchant WHERE id_merchant = '$merchantCode'";
        $data  = $this->db->query($sql);
        return  $data->row_array();
         
            
    }
    
    function getInquiryById($trxid,$limit=15,$page=0,$excel=false)
    {
        $start = $page>0 ? $limit*$page : 0; 
        
        $sql = "SELECT * FROM inquiry WHERE trxid = '$trxid'";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
         
        if ($excel){
            return $data;
        } else {        
            $sql  .= " LIMIT ".$start.",".$limit;    
            $data  = $this->db->query($sql);             
            return '{"total":"'.$total.'","rows":'.json_encode($data->row_array()).'}';
        }       
    }
    
    function getMerchantByCodeOld($merchantCode,$limit=15,$page=0,$excel=false)
    {
        $start = $page>0 ? $limit*$page : 0; 
        
        $sql = "SELECT * FROM merchant WHERE id_merchant = '$merchantCode'";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
         
        if ($excel){
            return $data;
        } else {        
            $sql  .= " LIMIT ".$start.",".$limit;    
            $data  = $this->db->query($sql);             
            return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
        }       
    }
    
    function getMerchantData($merchantCode)
    {
        $sql = "SELECT * FROM merchant WHERE id_merchant = '$merchantCode'";
        $data  = $this->db->query($sql);
        return $data->row_array();        
    }
    
    function getDataDiscount($app_code,$id)
    {
        $sql = "SELECT * FROM discount WHERE app_code = '$app_code' AND id = '$id' order BY id DESC limit 1;";
        $data  = $this->db->query($sql);
        return $data->row_array();        
    }
    
    function sumBudget($data)
    {
        $fromDate       = $data['from_date'];
        $toDate         = $data['to_date'];
        $id             = $data['id'];
        
        $sql    = "SELECT SUM(discount) as budget,id FROM transaksi WHERE (date BETWEEN '$fromDate 00:00:00' AND '$toDate 23:59:59') AND id_discount='$id' order BY id DESC limit 1;";
        $data   = $this->db->query($sql);
        log_message('error', 'Query Budget : '.$sql);
        return $data->row_array();        
        
    }
    
    function checkMaxUser($merchant_code,$msisdn,$data)
    {
        $fromDate       = $data['from_date'];
        $toDate         = $data['to_date'];
        $id             = $data['id'];
        $sql = "SELECT COUNT(1) as max,id FROM transaksi WHERE (date BETWEEN '$fromDate 00:00:00' AND '$toDate 23:59:59') AND msisdn = '$msisdn' AND id_discount='$id' AND status = 'SUCCESS' AND discount > 0 order BY id DESC limit 1;";
        $data  = $this->db->query($sql);
        log_message('error', 'Query Max User : '.$sql);
        return $data->row_array();        
    }
    
    function getDataInquiry($trxId)
    {
        $sql = "SELECT * FROM inquiry WHERE trxid = '$trxId' order BY id DESC";
        $data  = $this->db->query($sql);
        return $data->row_array();        
    }
    
    function getDataAmount($merchantCode,$amount)
    {
        $sql = "SELECT * FROM transaksi WHERE id_merchant = '$merchantCode' AND amount = '$amount' AND status = 'SUCCESS' order BY id DESC limit 1;";
        $data  = $this->db->query($sql);
        return $data->row_array();        
    }
    
    function updateMerchantBalance($data,$code)
    {
    	return $this->db->update('merchant', $data, array('id_merchant' => $code));
    }
    
    function updateInquiry($data,$trxid)
    {
    	return $this->db->update('inquiry', $data, array('trxid' => $trxid));
    }
}