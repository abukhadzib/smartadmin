<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_base extends CI_Model {
    
    function __construct() {
		parent::__construct();
	}
        
    function getMenu($roleId){
        $sql = "SELECT * FROM (
                SELECT *,'' AS 'notifikasi' 
                FROM menu WHERE id IN (SELECT menu_id FROM role_menu WHERE role_id='$roleId')
                UNION
                SELECT *,0 FROM menu WHERE id IN (
                	SELECT DISTINCT header FROM menu WHERE id IN (SELECT menu_id FROM role_menu WHERE role_id='$roleId') AND header IS NOT NULL
                ) ) a ORDER BY sort
                ";
        $data = $this->db->query($sql);
        return $data->result();
    }
    
    function cekMenuRole($roleId,$url){
        $sql = "SELECT * FROM (
                SELECT * FROM menu WHERE id IN (SELECT menu_id FROM role_menu WHERE role_id=$roleId)
                UNION
                SELECT * FROM menu WHERE id IN (
                	SELECT DISTINCT header FROM menu WHERE id IN (SELECT menu_id FROM role_menu WHERE role_id=$roleId) AND header IS NOT NULL
                ) ) a WHERE url = '$url'
                ";
        $data = $this->db->query($sql);
        if ($data->num_rows()>0){
            return true;
        } else {
            return false;
        }
    }
    
    function cekMenuRoleAction($roleId,$url,$action){
        $sql = "SELECT * FROM (
                SELECT *, (SELECT create_ FROM role_menu WHERE menu_id=a.id AND role_id=$roleId) 'create_', 
                    	  (SELECT read_   FROM role_menu WHERE menu_id=a.id AND role_id=$roleId) 'read_', 
                    	  (SELECT update_ FROM role_menu WHERE menu_id=a.id AND role_id=$roleId) 'update_', 
                    	  (SELECT delete_ FROM role_menu WHERE menu_id=a.id AND role_id=$roleId) 'delete_'
                FROM (
                	SELECT * FROM menu WHERE id IN (SELECT menu_id FROM role_menu WHERE role_id=$roleId)
                	UNION
                	SELECT * FROM menu WHERE id IN (
                		SELECT DISTINCT header FROM menu WHERE id IN (SELECT menu_id FROM role_menu WHERE role_id=$roleId) AND header IS NOT NULL
                	) 
                ) a 
                ) b
                WHERE url = '$url' AND $action='1'
                ";
         
        $data = $this->db->query($sql);
        if ($data->num_rows()>0){
            return true;
        } else {
            return false;
        }
    }
    
    function getEmailAdmin(){
        $sql = "select email,name from user where tipe='Admin'";
        $data = $this->db->query($sql);
        return $data->result();
    }
 
    function addLog($data){
        $data['ip'] = $this->input->ip_address();
        $data['ua'] = $this->input->user_agent();        
        return $this->db->insert('t_log', $data);         
    }
 
    function addLogAPI($data){
        $data['ip'] = $this->input->ip_address();
        $data['ua'] = $this->input->user_agent();        
        return $this->db->insert('t_log_api', $data);         
    }
 
    function addLogEmail($data){
        return $this->db->insert('t_log_email', $data);         
    }
 
    function getConfig($name=''){
        $sql  = "select value from config where name='$name'";
        $data = $this->db->query($sql);
        $row  = $data->row();
        return isset($row->value) ? $row->value : '';
    }
 
    function updateConfig($name='',$value=''){
        $sql  = "update config set value='$value' where name='$name'";
        return $this->db->query($sql);
    }
    
}