<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_signup extends CI_Model {
    
    function __construct() {
		parent::__construct();
	}
	
    function cekUser($username=''){
        $query = $this->db->get_where('user', array('username' => $username));
        if ($query->num_rows()>0){
            return true;
        } else {
            return false;
        }
    }
	
    function createUser($data){
        return $this->db->insert('user', $data); 
    }
	
    function createActivateUser($data){
        return $this->db->insert('t_activate_user', $data); 
    }
	   
}