<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_forgot_password extends CI_Model {
    
    function __construct() {
		parent::__construct();
	}
	
    function cekEmail($email=''){
        $query = $this->db->get_where('user', array('email' => $email));
        if ($query->num_rows()>0){
            return true;
        } else {
            return false;
        }
    }
	
    function getUserByEmail($email=''){
        $query = $this->db->get_where('user', array('email' => $email));
        if ($query->num_rows()>0){
            return $query->row();
        } else {
            return false;
        }
    }
	
    function createResetPassword($data){
        return $this->db->insert('t_reset_password', $data); 
    }
	   
}