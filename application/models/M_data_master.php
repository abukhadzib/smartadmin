<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_data_master extends CI_Model {
    
    function __construct() {
		parent::__construct();
	}
 
    function getPartner($limit=15,$page=0,$filter=array(),$excel=false)
    {        
        $start = $page>0 ? $limit*$page : 0; 
        
        $role_partner = $this->partner;
        
        $search = $this->session->userdata('search_partner_mesinpesan');
        $where  = !empty($search)       ? " where (name like '%".mysql_real_escape_string($search)."%' or  keyword like '%".mysql_real_escape_string($search)."%' or address1 like '%".mysql_real_escape_string($search)."%' or phone like '%".mysql_real_escape_string($search)."%' or sms_response like '%".mysql_real_escape_string($search)."%')" : '' ;
        $where .= !empty($role_partner) ? ( !empty($where) ? ' and ' : ' where ') . "id='$role_partner'" : '';
        
        $sql = "SELECT * from partner $where order by id desc";
        
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
        
        if ($excel){
            return $data;
        } else {        
            $sql  .= " LIMIT ".$start.",".$limit;    
            $data  = $this->db->query($sql);             
            return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
        }        
    }
 
    function getPartnerById($id)
    {
        $sql = "SELECT * from partner where id=$id";
        $data  = $this->db->query($sql);
        return $data->row();        
    }
 
    function getPartnerByKeyword($keyword)
    {
        $sql = "SELECT * from partner where keyword='$keyword'";
        $data  = $this->db->query($sql);
        return $data->row();        
    }
 
    function getPartnerBySecureKey($key)
    {
        $sql = "SELECT * from partner where secure_key='$key'";
        $data  = $this->db->query($sql);
        return $data->row();        
    }
 
    function getPartners()
    {
        $partner_user_login = $this->partner;
        $where = !empty($partner_user_login) ? " where id='$partner_user_login'" : '';
        
        $sql = "SELECT * from partner $where order by name";
        $data  = $this->db->query($sql);
        return $data->result();        
    }
 
    function getPartnersCount()
    {
        $partner_user_login = $this->partner;
        $where = !empty($partner_user_login) ? " where id='$partner_user_login'" : '';        
        $sql   = "SELECT count(*) 'total' from partner $where";
        $data  = $this->db->query($sql);
        $row   = $data->row();
        return '{"total":"'.$row->total.'"}';             
    }
	
    function addPartner($data)
    {
        return $this->db->insert('partner', $data); 
    }
    
    function cekPartnerKeywordExist($keyword,$id='')
    {
        $sql = "SELECT keyword from partner where keyword='$keyword'". (!empty($id)?" and id!='$id'":'');
                
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
        
        if ( $total > 0 ){
            return true;
        } else {
            return false;
        }
    }
    
    function cekPartnerExist($id)
    {
        $sql = "SELECT partner_id FROM program WHERE partner_id='$id'
                UNION
                SELECT partner_id FROM sub_program WHERE partner_id='$id'
                UNION
                SELECT partner_id FROM role WHERE partner_id='$id'";
                
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
        
        if ( $total > 0 ){
            return true;
        } else {
            return false;
        }
    }
    
    function cekProgramExist($id)
    {
        $sql = "SELECT program_id FROM sub_program WHERE program_id='$id'
                UNION
                SELECT program_id FROM role WHERE program_id='$id'";
                
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
        
        if ( $total > 0 ){
            return true;
        } else {
            return false;
        }
    }
    
    function cekProgramKeywordExist($keyword,$partner_id,$program_id='')
    {
        $sql = "SELECT keyword from program where keyword='$keyword' and partner_id='$partner_id'". (!empty($program_id)?" and id!='$program_id'":'');
                
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
        
        if ( $total > 0 ){
            return true;
        } else {
            return false;
        }
    }
    
    function cekSubProgramKeywordExist($keyword,$partner_id,$program_id,$subprogram_id='')
    {
        $sql = "SELECT keyword from sub_program where keyword='$keyword' and partner_id='$partner_id' and program_id='$program_id'". (!empty($subprogram_id)?" and id!='$subprogram_id'":'');
                
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
        
        if ( $total > 0 ){
            return true;
        } else {
            return false;
        }
    }
	
    function updatePartner($data,$id)
    {
        return $this->db->update('partner', $data, array('id' => $id)); 
    }
 
    function deletePartner($id)
    {
        return $this->db->delete('partner', array('id'=>$id));         
    }
 
    function getProgram($limit=15,$page=0,$filter=array(),$excel=false)
    {        
        $start = $page>0 ? $limit*$page : 0; 
        
        $role_partner    = $this->partner;
        $program_partner = $this->program;
        
        $where       = '';
        $name        = $this->session->userdata('name_view_program_mesinpesan');
        $description = $this->session->userdata('description_view_program_mesinpesan');
        $keyword     = $this->session->userdata('keyword_view_program_mesinpesan');
        $partner     = $this->session->userdata('partner_view_program_mesinpesan');
        $response    = $this->session->userdata('response_view_program_mesinpesan');
        
        $where      .= !empty($name)            ? (empty($where) ?' where ':' and ') . "name like '%$name%'"               : '';
        $where      .= !empty($description)     ? (empty($where) ?' where ':' and ') . "description like '%$description%'" : '';
        $where      .= !empty($keyword)         ? (empty($where) ?' where ':' and ') . "keyword like '%$keyword%'"         : '';
        $where      .= !empty($partner)         ? (empty($where) ?' where ':' and ') . "partner_id='$partner'"             : '';
        $where      .= !empty($response)        ? (empty($where) ?' where ':' and ') . "sms_response like '%$response%'"   : '';
        $where      .= !empty($role_partner)    ? (empty($where) ?' where ':' and ') . "partner_id='$role_partner'"        : '';
        $where      .= !empty($program_partner) ? (empty($where) ?' where ':' and ') . "id in ($program_partner)"          : '';
        
        $sql = "SELECT *,(select name from partner where id=program.partner_id) 'partner_name' from program $where order by id desc";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
        
        if ($excel){
            return $data;
        } else {        
            $sql  .= " LIMIT ".$start.",".$limit;    
            $data  = $this->db->query($sql);             
            return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
        }        
    }
 
    function getPrograms($partner='',$program='')
    {
        $role_partner    = $this->partner;
        $program_partner = $this->program;
        
        $where   = '';
        $where  .= !empty($partner)         ? (empty($where)?' where ':' and ') . "partner_id='$partner'"       : '';
        $where  .= !empty($program)         ? (empty($where)?' where ':' and ') . "id='$program'"               : '';
        $where  .= !empty($role_partner)    ? (empty($where)?' where ':' and ') . "partner_id='$role_partner'"  : '';
        $where  .= !empty($program_partner) ? (empty($where)?' where ':' and ') . "id in ($program_partner)"    : '';
        
        $sql     = "SELECT * from program $where order by name";
        
        $data    = $this->db->query($sql);
        return $data->result();        
    }
 
    function getProgramsCount($partner='',$program='')
    {
        $role_partner    = $this->partner;
        $program_partner = $this->program;
        
        $where   = '';
        $where  .= !empty($partner)         ? (empty($where)?' where ':' and ') . "partner_id='$partner'"       : '';
        $where  .= !empty($program)         ? (empty($where)?' where ':' and ') . "id='$program'"               : '';
        $where  .= !empty($role_partner)    ? (empty($where)?' where ':' and ') . "partner_id='$role_partner'"  : '';
        $where  .= !empty($program_partner) ? (empty($where)?' where ':' and ') . "id in ($program_partner)"    : '';
        
        $sql   = "SELECT count(*) 'total' from program $where ";
        $data  = $this->db->query($sql);
        $row   = $data->row();
        return '{"total":"'.$row->total.'"}';       
    }
 
    function getProgramPartnerData($partner='',$program='')
    {        
        $role_partner    = $this->partner;
        $program_partner = $this->program;
        
        $where   = '';
        $where  .= !empty($partner)         ? (empty($where)?' where ':' and ') . "partner_id='$partner'"       : '';
        $where  .= !empty($program)         ? (empty($where)?' where ':' and ') . "id='$program'"               : '';
        $where  .= !empty($role_partner)    ? (empty($where)?' where ':' and ') . "partner_id='$role_partner'"  : '';
        $where  .= !empty($program_partner) ? (empty($where)?' where ':' and ') . "id in ($program_partner)"    : '';
        
        $sql     = "SELECT * from program $where order by id";
        $data    = $this->db->query($sql);
        $total   = $data->num_rows();        
        return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
    }
 
    function getProgramByKeywordPartner($keyword,$partner='')
    {
        $where = !empty($partner) ? " and partner_id='$partner'" : '';
        $sql   = "SELECT * FROM program WHERE keyword='$keyword' $where";
        $data  = $this->db->query($sql);
        return $data->row();        
    }
 
    function getProgramBySecureKey($key)
    {
        $sql = "SELECT * from program where secure_key='$key'";
        $data  = $this->db->query($sql);
        return $data->row();        
    }
 
    function getProgramById($id)
    {
        $sql = "SELECT *,(select name from partner where id=program.partner_id) 'partner_name' from program where id=$id";
        $data  = $this->db->query($sql);
        return $data->row();        
    }
	
    function addProgram($data)
    {
        return $this->db->insert('program', $data); 
    }
	
    function updateProgram($data,$id)
    {
        return $this->db->update('program', $data, array('id' => $id)); 
    }
 
    function deleteProgram($id)
    {
        return $this->db->delete('program', array('id'=>$id));         
    }
 
    function getSubProgram($limit=15,$page=0,$filter=array(),$excel=false)
    {        
        $start = $page>0 ? $limit*$page : 0; 
        
        $role_partner    = $this->partner;
        $program_partner = $this->program;
        
        $where       = '';
        $name        = $this->session->userdata('name_view_subprogram_mesinpesan');
        $description = $this->session->userdata('description_view_subprogram_mesinpesan');
        $keyword     = $this->session->userdata('keyword_view_subprogram_mesinpesan');
        $partner     = $this->session->userdata('partner_view_subprogram_mesinpesan');
        $program     = $this->session->userdata('program_view_subprogram_mesinpesan');
        $response    = $this->session->userdata('response_view_subprogram_mesinpesan');
        
        $where      .= !empty($name)            ? (empty($where)?' where ':' and ') . "name like '%$name%'"               : '';
        $where      .= !empty($description)     ? (empty($where)?' where ':' and ') . "description like '%$description%'" : '';
        $where      .= !empty($keyword)         ? (empty($where)?' where ':' and ') . "keyword like '%$keyword%'"         : '';
        $where      .= !empty($partner)         ? (empty($where)?' where ':' and ') . "partner_id='$partner'"             : '';
        $where      .= !empty($program)         ? (empty($where)?' where ':' and ') . "program_id='$program'"             : '';
        $where      .= !empty($response)        ? (empty($where)?' where ':' and ') . "sms_response like '%$response%'"   : '';
        $where      .= !empty($role_partner)    ? (empty($where)?' where ':' and ') . "partner_id='$role_partner'"        : '';
        $where      .= !empty($program_partner) ? (empty($where)?' where ':' and ') . "program_id in ($program_partner)"  : '';
               
        $sql = "SELECT *,(select name from partner where id=sub_program.partner_id) 'partner_name',(select name from program where id=sub_program.program_id) 'program_name' from sub_program $where order by id desc";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
        
        if ($excel){
            return $data;
        } else {        
            $sql  .= " LIMIT ".$start.",".$limit;    
            $data  = $this->db->query($sql);             
            return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
        }        
    }
 
    function getSubPrograms($partner='',$program='',$subprogram='')
    {
        $role_partner    = $this->partner;
        $program_partner = $this->program;
        
        $where   = '';
        $where  .= !empty($partner)         ? (empty($where)?' where ':' and ') . "partner_id='$partner'" : '';
        $where  .= !empty($program)         ? (empty($where)?' where ':' and ') . "program_id='$program'" : '';
        $where  .= !empty($subprogram)      ? (empty($where)?' where ':' and ') . "id='$subprogram'" : '';
        $where  .= !empty($role_partner)    ? (empty($where)?' where ':' and ') . "partner_id='$role_partner'"        : '';
        $where  .= !empty($program_partner) ? (empty($where)?' where ':' and ') . "program_id in ($program_partner)"  : '';
        
        $sql    = "SELECT * from sub_program $where order by id";
        $data   = $this->db->query($sql);
        return $data->result();        
    }
 
    function getSubProgramsCount($partner='',$program='',$subprogram='')
    {
        $role_partner    = $this->partner;
        $program_partner = $this->program;        
        
        $where   = '';
        $where  .= !empty($partner)         ? (empty($where)?' where ':' and ') . "partner_id='$partner'" : '';
        $where  .= !empty($program)         ? (empty($where)?' where ':' and ') . "program_id='$program'" : '';
        $where  .= !empty($subprogram)      ? (empty($where)?' where ':' and ') . "id='$subprogram'" : '';
        $where  .= !empty($role_partner)    ? (empty($where)?' where ':' and ') . "partner_id='$role_partner'"        : '';
        $where  .= !empty($program_partner) ? (empty($where)?' where ':' and ') . "program_id in ($program_partner)"  : '';
        
        $sql     = "SELECT count(*) 'total' from sub_program $where";
        $data    = $this->db->query($sql);
        $row     = $data->row();
        return '{"total":"'.$row->total.'"}';
    }
 
    function getSubProgramByKeywordPartnerProgram($keyword,$partner='',$program='')
    {
        $role_partner    = $this->partner;
        $program_partner = $this->program;        
        
        $where   = " WHERE keyword='$keyword'";
        $where  .= !empty($partner)         ? (empty($where)?' where ':' and ') . "partner_id='$partner'" : '';
        $where  .= !empty($program)         ? (empty($where)?' where ':' and ') . "program_id='$program'" : '';
        $where  .= !empty($role_partner)    ? (empty($where)?' where ':' and ') . "partner_id='$role_partner'"        : '';
        $where  .= !empty($program_partner) ? (empty($where)?' where ':' and ') . "program_id in ($program_partner)"  : '';
        
        $sql    = "SELECT * FROM sub_program $where";
        $data   = $this->db->query($sql);
        return $data->row();        
    }
 
    function getSubProgramBySecureKey($key)
    {
        $sql = "SELECT * from sub_program where secure_key='$key'";
        $data  = $this->db->query($sql);
        return $data->row();        
    }
 
    function getSubProgramPartnerData($partner='',$program='')
    {        
        $role_partner    = $this->partner;
        $program_partner = $this->program;        
        
        $where   = '';
        $where  .= !empty($partner)         ? (empty($where)?' where ':' and ') . "partner_id='$partner'" : '';
        $where  .= !empty($program)         ? (empty($where)?' where ':' and ') . "program_id='$program'" : '';
        $where  .= !empty($role_partner)    ? (empty($where)?' where ':' and ') . "partner_id='$role_partner'"        : '';
        $where  .= !empty($program_partner) ? (empty($where)?' where ':' and ') . "program_id in ($program_partner)"  : '';
        
        $sql    = "SELECT * from sub_program $where order by id";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();        
        return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
    }
 
    function getSubProgramById($id)
    {
        $sql = "SELECT *,(select name from partner where id=sub_program.partner_id) 'partner_name',(select name from program where id=sub_program.program_id) 'program_name' from sub_program where id=$id";
        $data  = $this->db->query($sql);
        return $data->row();        
    }
	
    function addSubProgram($data)
    {
        return $this->db->insert('sub_program', $data); 
    }
	
    function updateSubProgram($data,$id)
    {
        return $this->db->update('sub_program', $data, array('id' => $id)); 
    }
 
    function deleteSubProgram($id)
    {
        return $this->db->delete('sub_program', array('id'=>$id));         
    }
 
    function getFilter($limit=15,$page=0,$filter=array(),$excel=false)
    {        
        $start = $page>0 ? $limit*$page : 0; 
        
        $search = $this->session->userdata('search_filter_mesinpesan');
        $where  = !empty($search) ? " where word like '%".mysql_real_escape_string($search)."%'" : '' ;
         
        $sql = "SELECT * from filter $where order by word";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
        
        if ($excel){
            return $data;
        } else {        
            $sql  .= " LIMIT ".$start.",".$limit;    
            $data  = $this->db->query($sql);             
            return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
        }        
    }
 
    function getFilterWord()
    {
        $sql   = "SELECT word from filter";
        $data  = $this->db->query($sql);
        return $data->result();        
    }
 
    function getFilterById($id)
    {
        $sql = "SELECT * from filter where id=$id";
        $data  = $this->db->query($sql);
        return $data->row();        
    }
	
    function addFilter($data)
    {
        return $this->db->insert('filter', $data); 
    }
	
    function updateFilter($data,$id)
    {
        return $this->db->update('filter', $data, array('id' => $id)); 
    }
 
    function deleteFilter($id)
    {
        return $this->db->delete('filter', array('id'=>$id));         
    }	   
}