<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'controllers/Base.php');

class Pesan extends Base 
{
    function __construct()
    {
		parent::__construct();
        $this->load->model('m_pesan'); 
        $this->load->model('m_data_master');
	}
	
	function index()
	{
        if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
         
        redirect(site_url('pesan/view'));
	} 
	
	function view($page=1)
	{
        if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
        
        $page = is_numeric($page) ? $page : 1;
        $page = $page < 1 ? 1 : $page;
        $page--;
        
        if ($this->input->post())
        {   
            $setPesanFilter = array('date1_view_pesan_mesinpesan'      => $this->input->post('date1'),
                                    'date2_view_pesan_mesinpesan'      => $this->input->post('date2'),
                                    'msisdn_view_pesan_mesinpesan'     => $this->input->post('msisdn'),
                                    'partner_view_pesan_mesinpesan'    => $this->input->post('partner'),
                                    'program_view_pesan_mesinpesan'    => $this->input->post('program'),
                                    'subprogram_view_pesan_mesinpesan' => $this->input->post('subprogram'),
                                    'pesan_view_pesan_mesinpesan'      => $this->input->post('pesan'));
			$this->session->set_userdata($setPesanFilter);
            redirect(site_url('pesan/view'));
        }    
                
        $data = $this->m_pesan->getPesan($this->limit_page,$page);
        $data = json_decode($data);
        
        $this->data['page']         = $page;
        $this->data['perpage']      = $this->limit_page;
        $this->data['username']     = $this->username;
        $this->data['name']         = $this->name;
        $this->data['content']      = $data->rows;
        $this->data['view_content'] = 'pesan/pesan';
        
        $this->data['partner']      = $this->m_data_master->getPartners();                 
        $this->data['program']      = $this->m_data_master->getPrograms($this->session->userdata('partner_view_pesan_mesinpesan'));
        $this->data['subprogram']   = $this->m_data_master->getSubPrograms($this->session->userdata('partner_view_pesan_mesinpesan'),$this->session->userdata('program_view_pesan_mesinpesan'));
        
        $this->data['date1_view_pesan_mesinpesan']      = $this->session->userdata('date1_view_pesan_mesinpesan');
        $this->data['date2_view_pesan_mesinpesan']      = $this->session->userdata('date2_view_pesan_mesinpesan');
        $this->data['msisdn_view_pesan_mesinpesan']     = $this->session->userdata('msisdn_view_pesan_mesinpesan');
        $this->data['partner_view_pesan_mesinpesan']    = $this->session->userdata('partner_view_pesan_mesinpesan');
        $this->data['program_view_pesan_mesinpesan']    = $this->session->userdata('program_view_pesan_mesinpesan');
        $this->data['subprogram_view_pesan_mesinpesan'] = $this->session->userdata('subprogram_view_pesan_mesinpesan');
        $this->data['pesan_view_pesan_mesinpesan']      = $this->session->userdata('pesan_view_pesan_mesinpesan');
        
        $link = site_url('pesan/view');
        $this->data['pagination'] = paging(ceil($data->total/$this->limit_page),$page+1,$link,$data->total,$this->limit_page);
        
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'pesan/view','data'=>'');
        
        if ( !$this->cekAksesAction('pesan/view','read_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';    
        }
        
        $this->addLog($data_log);
        
        $this->load->view('home',$this->data);
	} 
	
	function pesan_edit($id='')
	{
        if ( empty($this->username) ) {
            echo "Session anda telah berakhir, refresh browser anda.";
            exit;
	    } 
        
        if ( empty($id) ) {
            echo "{success:false, Msg:'Parameter tidak lengkap !.'}";
            exit;
	    } 
        
        if ($this->input->post())
        {
            if ( !$this->cekAksesAction('pesan/view','update_') ){
                echo "{success:false, Msg:'Anda tidak mempunyai hak merubah data Pesan !.'}";
                exit;
            }
			
            $data = array( 'pesan_filter'  => $this->input->post('pesan_filter'),
                           'modified_date' => date('Y-m-d H:i:s'));
            
            $data_log = array('userid'=>$this->userid,'action'=>'Update','modul'=>'pesan/pesan_edit/'.$id,'data'=>json_encode($data));
            $this->addLog($data_log);
            
            if ( $this->m_pesan->updatePesan($data,$this->input->post('pesan_id')))
            {
                echo "{success:true, Msg:'Proses rubah data Pesan <b>Berhasil</b>'}";
                exit; 
            }    
            else
            {
                echo "{success:true, Msg:'Ada kesalahan dalam merubah data Pesan.'}";
                exit; 
            }        
        }
        
        $this->data['pesan']  = $this->m_pesan->getPesanById($id);
        
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'pesan/pesan_edit/'.$id,'data'=>'');
        
        if ( !$this->cekAksesAction('pesan/view','update_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';    
        }
        
        $this->addLog($data_log);
                
        $this->load->view('pesan/pesan_edit',$this->data);
	}  
	
	function pesan_delete($id='')
	{
        if ( empty($this->username) ) {
            echo "Session anda telah berakhir, refresh browser anda.";
            exit;
	    } 
        
        if ( empty($id) ) {
            echo "{success:false, Msg:'Parameter tidak lengkap !'}";
            exit;
	    } 
        
        if ($this->input->post() && $this->input->post('action')=='delete')
        {
            $id_post = $this->input->post('id');
            
            $data_log = array('userid'=>$this->userid,'action'=>'Delete','modul'=>'pesan/pesan_delete/'.$id,'data'=>'');
            
            if ( $id_post!=$id ){
                echo "{success:false, Msg:'Parameter delete salah !'}";
                exit;
            }
            
            if ( !$this->cekAksesAction('pesan/view','delete_') ){
                echo "{success:false, Msg:'Anda tidak berhak menghapus data Pesan !'}";
                $data_log['data'] = "{success:false, Msg:'You have no rights to delete pesan !'}";
                $this->addLog($data_log);
                exit; 
            }
            
            if ( $this->pesan->cekPesanExist($id) ){
                echo "{success:false, Msg:'Data Pesan masih digunakan !.'}";
                exit;
            }
            
            if ( $this->pesan->deletePesan($id) ){
                echo "{success:true, Msg:'Proses hapus data Pesan <b>Berhasil</b> !'}";
                $data_log['data'] = "{success:true, Msg:'Berhasil hapus pesan'}";
                $this->addLog($data_log);
            }
        } else {
            echo "{success:false, Msg:'Parameter hapus data Pesan salah !'}";
        }
	} 
    
    function clear_search()
    {
        $setPesanFilter = array('date1_view_pesan_mesinpesan'      => '',
                                'date2_view_pesan_mesinpesan'      => '',
                                'msisdn_view_pesan_mesinpesan'     => '',
                                'partner_view_pesan_mesinpesan'    => '',
                                'program_view_pesan_mesinpesan'    => '',
                                'subprogram_view_pesan_mesinpesan' => '',
                                'pesan_view_pesan_mesinpesan'      => '');
        $this->session->set_userdata($setPesanFilter);
        redirect(site_url('pesan/view'));    
    }
    
    function mo()
    {
        $sms_response = ''; // 'Terimakasih atas partisipasi anda.';
        $msisdn       = '';
        
        $sms_response = replaceMsg($sms_response);
        $sms_response = htmlspecialchars($sms_response);  

        $dataXML      = array('msisdn'       => $msisdn,
                              'content_text' => $sms_response);
                              
        $xml_response = createXML($dataXML);
                                
        if ($this->input->get())
        {
            $message = explode(' ',$this->input->get('message'));
            $msisdn  = $this->input->get('msisdn');
            
            //cek message            
            $sms_response = ''; // 'Terimakasih atas partisipasi anda.';
                        
            $partner_keyword    = isset($message[0]) ? $message[0] : '';
            $program_keyword    = isset($message[1]) ? $message[1] : '';
            $subprogram_keyword = isset($message[2]) ? $message[2] : '';
            
            $partner      = $this->m_data_master->getPartnerByKeyword($partner_keyword);
            $partner_id   = isset($partner->id)           ? $partner->id           : '';
            $partner_name = isset($partner->name)         ? $partner->name         : '';
            $partner_sms  = isset($partner->sms_response) ? $partner->sms_response : '';
            $sms_response = !empty($partner_sms)          ? $partner_sms           : $sms_response;
            
            $subprogram_keyword =  empty($partner_id) ? $program_keyword : $subprogram_keyword;
            $program_keyword    =  empty($partner_id) ? $partner_keyword : $program_keyword;
            
            $program      = $this->m_data_master->getProgramByKeywordPartner($program_keyword,$partner_id);
            $program_id   = isset($program->id)           ? $program->id           : '';
            $program_name = isset($program->name)         ? $program->name         : '';
            $program_sms  = isset($program->sms_response) ? $program->sms_response : '';
            $sms_response = !empty($program_sms)          ? $program_sms           : $sms_response;
            $program_partner_id  = isset($program->partner_id) ? $program->partner_id : '';
            
            $l1 = false;
            if ( empty($partner_id) && !empty($program_partner_id) ){
                $l1 = true;
                $partner         = $this->m_data_master->getPartnerById($program_partner_id);
                $partner_id      = isset($partner->id)        ? $partner->id           : '';
                $partner_name    = isset($partner->name)      ? $partner->name         : '';
                $partner_keyword = isset($partner->keyword)   ? $partner->keyword      : '';
            }
            
            $subprogram_keyword = empty($program_id) ? $partner_keyword : $subprogram_keyword;
            
            $subprogram      = $this->m_data_master->getSubProgramByKeywordPartnerProgram($subprogram_keyword,$partner_id,$program_id);
            $subprogram_id   = isset($subprogram->id)           ? $subprogram->id           : '';
            $subprogram_name = isset($subprogram->name)         ? $subprogram->name         : '';
            $subprogram_sms  = isset($subprogram->sms_response) ? $subprogram->sms_response : '';
            $sms_response    = !empty($subprogram_sms)          ? $subprogram_sms           : $sms_response;
            
            if ( $l1 && empty($subprogram_id) ){
                $subprogram      = $this->m_data_master->getSubProgramByKeywordPartnerProgram($program_keyword,$partner_id,$program_id);
                $subprogram_id   = isset($subprogram->id)           ? $subprogram->id           : '';
                $subprogram_name = isset($subprogram->name)         ? $subprogram->name         : '';
                $subprogram_sms  = isset($subprogram->sms_response) ? $subprogram->sms_response : '';
                $sms_response    = !empty($subprogram_sms)          ? $subprogram_sms           : $sms_response;
            }
            
            if ( empty($partner_id)    ) $partner_keyword    = '';
            if ( empty($program_id)    ) $program_keyword    = '';
            if ( empty($subprogram_id) ) $subprogram_keyword = '';
            
            $pesan = '';
            for($i=(!empty($subprogram_id)?3:(!empty($program_id)?2:(!empty($partner_id)?1:0)));$i<count($message);$i++)
            {
                $pesan .= $message[$i] . ' ';
            }
            $pesan          = trim($pesan);
            
            $filter         = $this->m_data_master->getFilterWord();
            $filter_words   = array();
            
            foreach($filter as $filter_row)
            {
                $filter_words[] = $filter_row->word;
            }
            
            // Filter pesan
            $pesan_filtered = filter_words($pesan, $filter_words);         
            
            // Operator
            $operator = 'XL';
            
            if ( substr($msisdn, 0,5) == '62855' || substr($msisdn, 0,5) == '62856' || 
                 substr($msisdn, 0,5) == '62857' || substr($msisdn, 0,5) == '62858' || 
                 substr($msisdn, 0,5) == '62815' || substr($msisdn, 0,5) == '62814' || 
                 substr($msisdn, 0,5) == '62816' )
            {
                $operator = 'INDOSAT';
            }
            
            if ( substr($msisdn, 0,5) == '62811' || substr($msisdn, 0,5) == '62812' || 
                 substr($msisdn, 0,5) == '62813' || substr($msisdn, 0,5) == '62821' || 
                 substr($msisdn, 0,5) == '62822' || substr($msisdn, 0,5) == '62823' || 
                 substr($msisdn, 0,5) == '62851' || substr($msisdn, 0,5) == '62852' || 
                 substr($msisdn, 0,5) == '62853' )
            {
                $operator = 'TELKOMSEL';
            }
            
            $data = array('trxid'               => $this->input->get('trxid'),
                          'progid'              => $this->input->get('progid'),
                          'msisdn'              => $this->input->get('msisdn'),
                          'realmsisdn'          => $this->input->get('realmsisdn'),
                          'regmethode'          => $this->input->get('regmethode'),
                          'message'             => $this->input->get('message'),
                          'action'              => $this->input->get('action'),
                          'contentid'           => $this->input->get('contentid'),
                          'a'                   => $this->input->get('a'),
                          'b'                   => $this->input->get('b'), 
                          'c'                   => $this->input->get('c'),
                          'd'                   => $this->input->get('d'), 
                          'channel'             => $this->input->get('channel'),
                          'qs'                  => $_SERVER['QUERY_STRING'],
                          'ua'                  => $_SERVER['HTTP_USER_AGENT']
                          );
                          
            $this->m_pesan->addMO($data);
            
            $sms_response = replaceMsg($sms_response);
            $sms_response = htmlspecialchars($sms_response);  
    
            $dataXML      = array('msisdn'       => $msisdn,
                                  'content_text' => $sms_response);
                                  
            $xml_response = createXML($dataXML);        
                          
            $data = array('trxid'               => $this->input->get('trxid'),
                          'progid'              => $this->input->get('progid'),
                          'msisdn'              => $this->input->get('msisdn'),
                          'realmsisdn'          => $this->input->get('realmsisdn'),
                          'regmethode'          => $this->input->get('regmethode'),
                          'message'             => $this->input->get('message'),
                          'action'              => $this->input->get('action'),
                          'contentid'           => $this->input->get('contentid'),
                          'a'                   => $this->input->get('a'),
                          'b'                   => $this->input->get('b'), 
                          'c'                   => $this->input->get('c'),
                          'd'                   => $this->input->get('d'), 
                          'channel'             => $this->input->get('channel'),
                          'qs'                  => $_SERVER['QUERY_STRING'],
                          'ua'                  => $_SERVER['HTTP_USER_AGENT'],
                          'operator'            => $operator,
                          'partner_id'          => $partner_id,
                          'partner_name'        => $partner_name,
                          'partner_keyword'     => $partner_keyword,
                          'program_id'          => $program_id,
                          'program_name'        => $program_name,
                          'program_keyword'     => $program_keyword,
                          'subprogram_id'       => $subprogram_id,
                          'subprogram_name'     => $subprogram_name,
                          'subprogram_keyword'  => $subprogram_keyword,
                          'pesan'               => $pesan,
                          'pesan_filter'        => $pesan_filtered,
                          'sms_response'        => $sms_response,
                          'xml_response'        => $xml_response    
                          );
                          
            $this->m_pesan->addPesan($data);
        }
        
        $this->output->set_header("Content-Type:text/xml");        
        echo $xml_response;        
    }
    
    function dr()
    {
        $sms_response = 'Terimakasih atas partisipasi anda.';
        $msisdn       = '';
       
        if ($this->input->get())
        {
            $trxid   = $this->input->get('trxid');
            $keyword = $this->input->get('keyword');
            $program = $this->input->get('program');
            $channel = $this->input->get('channel');
            $syntax  = $this->input->get('syntax');
            $msisdn  = $this->input->get('msisdn');
            $sc      = $this->input->get('sc');
            $status  = $this->input->get('status');
            $chgid   = $this->input->get('chgid');
            $chgamt  = $this->input->get('chgamt');
            $telco   = $this->input->get('telco');
            $push    = $this->input->get('push');
            $unreg   = $this->input->get('unreg');
                                    
            $pesan        = $this->m_pesan->getPesanByTrxId($trxid);            
            $sms_response = isset($pesan->sms_response) ? $pesan->sms_response : $sms_response;
            
            $data    = array('status'=>$status,'status_date'=>date('Y-m-d H:i:s'));
            $this->m_pesan->updatePesanByTrxId($data,$trxid);
            
            $data    = array('trxid'   => $trxid,
                             'keyword' => $keyword,
                             'program' => $program,
                             'channel' => $channel,
                             'syntax'  => $syntax,
                             'msisdn'  => $msisdn,
                             'sc'      => $sc,
                             'status'  => $status,
                             'chgid'   => $chgid,
                             'chgamt'  => $chgamt,
                             'telco'   => $telco,
                             'push'    => $push,
                             'unreg'   => $unreg,
                             'qs'      => $_SERVER['QUERY_STRING'],
                             'ua'      => $_SERVER['HTTP_USER_AGENT']
                             );
            $this->m_pesan->addDR($data);
        }
        
        $sms_response = replaceMsg($sms_response);
        $sms_response = htmlspecialchars($sms_response);  

        $dataXML      = array('msisdn'       => $msisdn,
                              'content_text' => $sms_response);
                              
        $xml_response = createXML($dataXML);
        
        $this->output->set_header("Content-Type:text/xml");  
        echo $xml_response;             
    }
	
	function excel()
	{
        if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
        
        ob_start();
        
        $this->data['content']      = $this->m_pesan->getPesans();
        
        if ( !$this->cekAksesAction('pesan/view','read_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';   
            $this->load->view('home',$this->data);
            exit; 
        }
        
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'pesan/excel','data'=>'');
        $this->addLog($data_log);        
        
        $html = $this->load->view('pesan/pesan_excel',$this->data,true);
        
        $name = 'Pesan_'.time().'.xlsx';
        $file = 'assets/pesan/'.$name;
        
        $tmpfile = time().'.html';
        file_put_contents($tmpfile, $html);
        
        $this->load->library('Excel');
        
        $reader = new PHPExcel_Reader_HTML; 
        $content = $reader->load($tmpfile); 
        
        unlink($tmpfile);
        
        $objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel2007');
        $objWriter->save($file);        
        
        header('Location: '.base_url($file));            
	}  
	
	function export_excel()
	{
         if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
        
        ob_start();
        
        if ( !$this->cekAksesAction('pesan/view','read_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';   
            $this->load->view('home',$this->data);
            exit; 
        }
        
        $pesan = $this->m_pesan->getPesans();
                
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'pesan/export_excel','data'=>'');        
        $this->addLog($data_log);        
        
        $name = 'Pesan_'.time().'.xlsx';
        $file = 'assets/pesan/'.$name;        
        
        $this->load->library('Excel');
        
        $col = array(array('col' => 'A', 'name' => 'No'),
                     array('col' => 'B', 'name' => 'Tanggal'),
                     array('col' => 'C', 'name' => 'MSISDN'),
                     array('col' => 'D', 'name' => 'Partner'),
                     array('col' => 'E', 'name' => 'Program'),
                     array('col' => 'F', 'name' => 'Sub Program'),
                     array('col' => 'G', 'name' => 'Pesan'),
                     array('col' => 'H', 'name' => 'Pesan Filter'));
        
        $objPHPExcel = new PHPExcel();
		
		$objPHPExcel->getProperties()
					    ->setCreator("Mesin Pesan Application")
						->setLastModifiedBy("Mesin Pesan Application")
						->setTitle("Mesin Pesan Message List Report")
						->setSubject("Mesin Pesan Message List Report")
						->setDescription("Mesin Pesan Message List Report")
						->setKeywords("Mesin Pesan application message sms report")
						->setCategory("Reporting");
						
		$objPHPExcel->setActiveSheetIndex(0);		
        
        $no = 1;
		
        for($i=0;$i<count($col);$i++)
        {
            $objPHPExcel->getActiveSheet()->getStyle( $col[$i]['col'].$no )->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
    		$objPHPExcel->getActiveSheet()->getStyle( $col[$i]['col'].$no )->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    		$objPHPExcel->getActiveSheet()->getStyle( $col[$i]['col'].$no )->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    		$objPHPExcel->getActiveSheet()->getStyle( $col[$i]['col'].$no )->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    		$objPHPExcel->getActiveSheet()->getStyle( $col[$i]['col'].$no )->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    		$objPHPExcel->getActiveSheet()->getColumnDimension( $col[$i]['col'] )->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->setCellValue( $col[$i]['col'].$no , $col[$i]['name'] );
        }
        
        $objPHPExcel->getActiveSheet()->getStyle('A'.$no.':H'.$no)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('CCCCCCCC');
        
		$no++;
        
		foreach($pesan as $pesan_row)
		{
			$objPHPExcel->getActiveSheet()->getStyle('A'.$no.':H'.$no)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$no.':H'.$no)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$no.':H'.$no)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$no.':H'.$no)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$no.':H'.$no)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()
						->setCellValue('A'.$no, $no - 1)
						->setCellValue('B'.$no, date("d-m-Y H:i:s", strtotime($pesan_row->created_date)))
						->setCellValue('C'.$no, str_replace(array('62', '4'), array("0", "4"), '`'.$pesan_row->msisdn))
						->setCellValue('D'.$no, $pesan_row->partner_name)
						->setCellValue('E'.$no, $pesan_row->program_name)
						->setCellValue('F'.$no, $pesan_row->subprogram_name)
						->setCellValue('G'.$no, $pesan_row->pesan)
						->setCellValue('H'.$no, $pesan_row->pesan_filter);
			$objPHPExcel->getActiveSheet()->getCell('C'.$no)->setValueExplicit(str_replace(array('62'), array("0"), $pesan_row->msisdn), PHPExcel_Cell_DataType::TYPE_STRING);
			$no++;
		}		
		
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$name);
		header('Cache-Control: max-age=0');
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	      
	} 
}