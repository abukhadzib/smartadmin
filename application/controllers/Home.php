<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'controllers/Base.php');

class Home extends Base {

	function __construct()
    {
		parent::__construct(); 
        $this->load->model('m_data_master'); 
        $this->load->model('m_pesan'); 
	}
	
	function index()
	{
		if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
        
        $this->data['view_content']     = 'dashboard';
        
        $this->load->view('home',$this->data);
	} 
        
}
