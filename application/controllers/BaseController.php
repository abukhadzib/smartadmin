<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class BaseController extends REST_Controller {

    function __construct()
    {
        parent::__construct(); 
        $CI =& get_instance();
        $this->msisdn   = $CI->config->item('msisdn'); 
        $this->webpass  = $CI->config->item('webpass'); 
        $this->pin      = $CI->config->item('pin');  
        $this->load->library('St24');
        $this->load->model('m_trx');
    }
	
    function notFound()
    {
        return $this->response(['status' => 'ERROR', 'message' => 'Invalid Url Path'], REST_Controller::HTTP_NOT_FOUND);  
    } 

    public function authenticateAddress($allowed=false){
        $CI =& get_instance();
        $clientIp = $_SERVER['REMOTE_ADDR'];
        
        if (array_key_exists($clientIp, $CI->config->item('IpWhiteList'))){
            $allowed = true;
            log_message('error', 'IP ACCESS FROM : '.$clientIp);
        }

        if (!$allowed) {
            log_message('error', 'FORBIDDEN ACCESS for IP : '.$clientIp);
            return $this->response(['status' => 'ERROR', 'message' => 'FORBIDDEN ACCESS : '.$clientIp ],  REST_Controller::HTTP_FORBIDDEN);
            
        }
    }
    
    function inquiryCta($body){
        
        $this->cekSignature($body);
        $kode = $this->cekCode($body);
        unset($body['signature']);
        unset($body['time']);
        $time               = date('YmdHis');
        $request            = $this->decode($body);
        $result             = $this->st24->inquiryToBank($request);
        $body['trxId']      = $this->getTraceNo($time);
        $body['fee']        = $kode['fee'];
        $body['total']      = $kode['fee']+$body['amount'];
        if(strrpos(strtoupper($result['message']  ), 'A/N') == TRUE){
            $explode = explode('REK', strtoupper($result['message']));
            $data = explode('A/N', strtoupper($explode[1]));
            $nama = explode('<MCOIN>', $data[1]);
            $body['nama_penerima']          = trim($nama[0]);
        }
        
        $result             = array_merge($result,$body);
        $body['created_at'] = date('Y-m-d H:i:s');
        if($result['status'] == 'OK'){
            $body['is_use'] = '0';
        }
        if(! $this->m_trx->addInquiryCta($body)){
            return $this->response(['status' => "ERROR", 'message' => "Inquiry to Bank gagal, Database Error"], 403);
        }else{
            return $result;
        }
    }
    
    function transferCta($body){
        $this->cekSignature($body);
        $this->cekCode($body);
        $inquiry = $this->cekTrxId($body);
        unset($body['signature']);
        unset($body['time']);
        
        $time               = date('YmdHis');
        $request            = $this->decode($body);
        $result             = $this->st24->transferToBank($request); 
        $body['trxId']      = $this->getTraceNo($time);
        $body['fee']        = $inquiry['fee'];
        $body['total']      = $inquiry['total'];
        if(strrpos(strtoupper($result['message']  ), 'BERHASIL') == TRUE){
            $data = explode('SALDO', strtoupper($result['message']));
            $result['message']          = trim($data[0]);
        }
        $result             = array_merge($result,$body);
        $body['created_at'] = date('Y-m-d H:i:s');
        $data = array('is_use' => '1');
        $this->m_trx->updateCtaInquiry($data,$inquiry['trxId']);
        if(! $this->m_trx->addTrxCta($body)){
            return $this->response(['status' => "ERROR", 'message' => "Transfer to Bank gagal, Database Error"], 403);
        }else{
            return $result;
        }
    }
    
    function decode($post){
        
        $post['msisdn']     = $this->msisdn;
        $post['webpass']    = $this->webpass;
        $post['pin']        = $this->pin;
        $post               = json_encode($post);
        $request            = json_decode($post);
        
        return $request;
    }
    
    function cekSignature($body){
        $CI =& get_instance();
        $key                = $CI->config->item('key');
        $signature          = hash('sha256',$body['no_rekening'].$body['amount'].$body['time'].$key);;
        
        if($signature != $body['signature']){  
            $this->response(['status' => "ERROR", 'message' => "Invalid Signature"], 403);
        }      
        return $signature;
        
    }
    
    function cekTrxId($body){
        $Inquiry = $this->m_trx->validateTrxId($body);
        if(! $Inquiry ){  
            $this->response(['status' => "ERROR", 'message' => "Invalid Trx Id"], 403);
        }      
        return $Inquiry;
        
    }
    
    function cekCode($body){
        $kode = $this->m_trx->validateCode($body);
        if(! $kode ){  
            $this->response(['status' => "ERROR", 'message' => "Kode Product Not Found"], 403);
        }      
        return $kode;
        
    }
    
    function getTraceNo($value){
        $CI =& get_instance();
        $key                = $CI->config->item('base_key');
        $traceNo            = base64_encode($value.$key);
        
        return md5($traceNo);
    }
    
}

