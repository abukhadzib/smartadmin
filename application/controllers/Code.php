<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'controllers/Base.php');

class Code extends Base {

	function __construct()
    {
		parent::__construct(); 
        $this->load->model('m_data_master'); 
        $this->load->model('m_pesan'); 
	}
	
	function index()
	{
		if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
        
        $this->load->library('ciqrcode');
	
            header("Content-Type: image/png");
            $params['data'] = 'This is a text to encode become QR Code';
            $params['trxid'] = date('YmdHis');
            $params['level'] = 'H';
            $params['size'] = 6;
            
            $this->ciqrcode->generateQR($params);
            
	} 
        
        function text() {
            $this->load->library('ciqrcode');
	
            header("Content-Type: image/png");
            $params['data'] = 'This is a text to encode become QR Code';
            $params['trxid'] = date('YmdHis');
            $params['level'] = 'H';
            $params['size'] = 6;
            
            $this->ciqrcode->generateQR($params);
            
        }
        
        function Tel() {
            $this->load->library('ciqrcode');
            header("Content-Type: image/png");
            $params['level'] = 'H';
            $params['size'] = 5;
            $phoneNo = '085777544678';
    
            // we building raw data
            $params['data'] = 'tel:'.$phoneNo; ; 
            
             $this->ciqrcode->generateQR($params);
        }
        
         function Sms() {
            $this->load->library('ciqrcode');
            header("Content-Type: image/png");
            $params['level'] = 'H';
            $params['size'] = 5;
            $phoneNo = '085777544678';
    
            // we building raw data
            $params['data'] = 'sms:'.$phoneNo; ; 
            
             $this->ciqrcode->generateQR($params);
        }
        
        function Skype() {
            $this->load->library('ciqrcode');
            header("Content-Type: image/png");
            $params['level'] = 'H';
            $params['size'] = 3;
            
            // here our data
            $skypeUserName = 'abukhadzib';

            // we building raw data
            $params['data'] = 'skype:'.urlencode($skypeUserName).'?call'; 
            
             $this->ciqrcode->generateQR($params);
        }
        
        function Email() {
            
            $this->load->library('ciqrcode');
            header("Content-Type: image/png");
            $params['level'] = 'H';
            $params['size'] = 3;
            
            // here our data
            $email = 'syamsul.anwar21@gmail.com';
            $subject = 'question';
            $body = 'please write your question here';

            // we building raw data
            $params['data'] = 'mailto:'.$email.'?subject='.urlencode($subject).'&body='.urlencode($body); 
            
             $this->ciqrcode->generateQR($params);
             
        }
        function Vcard() {
            $this->load->library('ciqrcode');
	
            header("Content-Type: image/png");
            $params['data'] = 'This is a text to encode become QR Code';
            $params['trxid'] = date('YmdHis');
            $params['level'] = 'H';
            $params['size'] = 3;
            $name = 'Syamsul';
            $phone = '087782257270';
            $kantor = 'McoinAsia';
            $email = 'syamsul.anwar21@gmail.com';
            $url    = 'www.okedong.com';
            $avatarJpegFileName = 'http://localhost/';
            //BEGIN:VCARD VERSION:3.0 N:User;Test FN:Test User ORG:Example Organisation TITLE:asgfas [asgasg] TEL;TYPE=work,voice:2523626 TEL;TYPE=cell,voice:2365236 TEL;TYPE=work,fax:236236 URL;TYPE=work:www.example.com EMAIL;TYPE=internet,pref:testu@example.com REV:20121015T195243Z END:VCARD
            
            // we building raw data
            $params['data']  = 'BEGIN:VCARD'."\n";
            $params['data'] .= 'FN:'.$name."\n";
            $params['data'] .= 'TEL;CELL;VOICE:'.$phone."\n";
            $params['data'] .= 'ORG:'.$kantor."\n";
            $params['data'] .= 'EMAIL:'.$email."\n";
            $params['data'] .= 'URL;TYPE=work:'.$url."\n";
            //$params['data'] .= 'PHOTO;JPEG;ENCODING=BASE64:'.base64_encode(file_get_contents($avatarJpegFileName))."\n";
            $params['data'] .= 'END:VCARD'; 
            
            $this->ciqrcode->generateVcard($params);

        }
        
        function save() {
            $this->load->library('ciqrcode');
	
            $params['data'] = 'This is a text to encode become QR Code';
            $params['level'] = 'H';
            $params['size'] = 10;
            $params['savename'] = FCPATH.'tes.png';
            $this->ciqrcode->generateQR($params);

            echo '<img src="'.base_url().'tes.png" />';
        }
		
}
