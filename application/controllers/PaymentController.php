<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'controllers/BaseController.php');

class PaymentController extends BaseController {

    function __construct()
    {
        parent::__construct();
         
    }
	
    function inquiryToBank_post(){
        
        /* CEK ACCESS IP ADDRESS*/
        $this->authenticateAddress();
        
        $body = $this->post();
        $this->validateinput->validateText($this->post('requestId', TRUE), "Request Id", "TEXT", false, 150);
        $this->validateinput->validateText($this->post('kode', TRUE), "Kode Product", "TEXT", false, 15);
        $this->validateinput->validateText($this->post('kode_bank', TRUE), "Kode Bank", "TEXT", false, 15);
        $this->validateinput->validateText($this->post('no_rekening', TRUE), "No rekening", "TEXT", false, 30);
        $this->validateinput->validateText($this->post('amount', TRUE), "Amount", "TEXT", false, 15);
        $this->validateinput->validateText($this->post('nohp_penerima', TRUE), "No HP Penerima", "TEXT", false, 15);
        $this->validateinput->validateText($this->post('nama_penerima', TRUE), "Nama Penerima", "TEXT", false, 50);
        $this->validateinput->validateText($this->post('kota_penerima', TRUE), "Kota Penerima", "TEXT", false, 70);
        $this->validateinput->validateText($this->post('keterangan', TRUE), "Keterangan", "TEXT", false, 100);
        $this->validateinput->validateText($this->post('time', TRUE), "waktu Transaksi", "TEXT", false, 100);
        $this->validateinput->validateText($this->post('signature', TRUE), "signature", "TEXT", true, 100);

        if ($this->validateinput->isError()) {
            $this->response(['status' => "ERROR", 'message' => $this->validateinput->getMessage()], 403);
        }
        
        log_message('error', ' Request : ' .  json_encode($body));
        log_message('error', ' ===== Inquiry To Bank ===== ');
        
        $result             = $this->inquiryCta($body);
        
        if(!$result){
           return $this->response(['status' => "ERROR", 'message' => "Inquiry gagal, koneksi ke server gagal"], 403);
        }
        
        $this->response($result, 200);
        return;
        
    }
    
    function transferToBank_post(){
        
        /* CEK ACCESS IP ADDRESS*/
        $this->authenticateAddress();
        
        $body = $this->post();
        $this->validateinput->validateText($this->post('trxId', TRUE), "TransactionId", "TEXT", false, 150);
        $this->validateinput->validateText($this->post('requestId', TRUE), "Request Id", "TEXT", false, 150);
        $this->validateinput->validateText($this->post('kode', TRUE), "Kode Product", "TEXT", false, 15);
        $this->validateinput->validateText($this->post('kode_bank', TRUE), "Kode Bank", "TEXT", false, 15);
        $this->validateinput->validateText($this->post('nama_pengirim', TRUE), "Nama Pengirim", "TEXT", false, 100);
        $this->validateinput->validateText($this->post('no_id', TRUE), "Nomor Identitas Pengirim", "TEXT", false, 19);
        $this->validateinput->validateText($this->post('no_rekening', TRUE), "Nomor Rekening", "TEXT", false, 30);
        $this->validateinput->validateText($this->post('nohp_penerima', TRUE), "Nomor Penerima", "TEXT", false, 15);
        $this->validateinput->validateText($this->post('amount', TRUE), "Amount", "TEXT", false, 15);
        $this->validateinput->validateText($this->post('nohp_pengirim', TRUE), "Nomor Pengirim", "TEXT", false, 15);
        $this->validateinput->validateText($this->post('time', TRUE), "waktu Transaksi", "TEXT", false, 100);
        $this->validateinput->validateText($this->post('signature', TRUE), "signature", "TEXT", true, 100);
        
        if ($this->validateinput->isError()) {
            $this->response(['status' => "ERROR", 'message' => $this->validateinput->getMessage()], 403);
        }
        
        log_message('error', ' Request : ' .  json_encode($body));
        log_message('error', ' ===== transfer To Bank ===== ');
        
        $result             = $this->transferCta($body);
      
        if(!$result){
           return $this->response(['status' => "ERROR", 'message' => "Transfer to Bank gagal, koneksi ke server gagal"], 403);
        }
        
        $this->response($result, 200);
        return;
        
    }
       
}

