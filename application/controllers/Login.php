<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'controllers/Base.php');

class Login extends Base {

	function __construct()
    {
		parent::__construct();
        $this->load->model('m_login');    
	}
	
	function index()
	{
        if ( !empty($this->username) ) {
	       redirect(site_url('home'));
	    } 
        
		//cek cookie remember_me
		$data_authCookieAdminSmart = $this->input->cookie('authCookieAdminSmart');
		$authCookie = $this->doAuthcookie($data_authCookieAdminSmart);
		if ( $authCookie )
		{
			redirect(site_url('home'));
		} 
		else
		{
			set_cookie('authCookieAdminSmart','',time()-3600);
		}
		
        $this->data['error_login'] = $this->session->flashdata('error_login');        
        $this->load->view('login',$this->data);
	}
    
    function doAuthcookie($data_authCookieAdminSmart)
	{
		$data_authCookieAdminSmart = base64_decode($data_authCookieAdminSmart);
		if ( empty($data_authCookieAdminSmart) )
		{
			return false;
		}
		
		list($user,$pass) = explode('&',$data_authCookieAdminSmart);
		list($usertext,$userval) = explode('=',$user);
		list($passtext,$passval) = explode('=',$pass);		
		$userpass = $this->m_login->cekUserPassAuth($userval,$passval);
		if ( $userpass )
		{
			$setuser = array('userid_AdminSmart'     => $userpass->id,
							 'username_AdminSmart'   => $userpass->username,
							 'name_AdminSmart'       => $userpass->name,
							 'role_AdminSmart'       => $userpass->role_id,
							 'role_tipe_AdminSmart'  => $userpass->tipe,
							 'isadmin_AdminSmart'    => $userpass->is_admin,
							 'partner_AdminSmart'    => $userpass->partner,
							 'program_AdminSmart'    => $userpass->program,
							 'email_user_AdminSmart' => $userpass->email);
			$this->session->set_userdata($setuser);
			$this->m_login->updateLastLogin($userpass->id);
			
			$data_log = array('userid'=>$userpass->id,'action'=>'Login Remember Me','modul'=>'Login','data'=>json_encode($setuser));
			$this->addLog($data_log);
			
			return true;			
		}
		else
		{
			return false;			
		}
	}
    
    function dologin()
	{
		if ($this->input->post())
		{
            $user = $this->input->post('username');
            $pass = $this->input->post('password');
			$remember_me = $this->input->post('remember_me');
            
            if ($this->m_login->cekUser($user))
			{
                $userpass = $this->m_login->cekUserPass($user,$pass);
                                 
                if ($userpass){
                    if ( $userpass->verified <> 'Y' )
                    {
                        $this->session->set_flashdata('error_login', 'Username not verified!');
                        redirect(site_url('login'));
                    }
                    else
                    {
                        if ( $remember_me )
						{
							$cookie_val = base64_encode( 'username='.$user.'&password='.md5($pass) );
							$cookie_expire = 3600 * 24 * 30; // 30 hari							
							set_cookie('authCookieAdminSmart',$cookie_val,$cookie_expire);
						}
						
						$setuser = array('userid_AdminSmart'     => $userpass->id,
                                         'username_AdminSmart'   => $userpass->username,
                                         'name_AdminSmart'       => $userpass->name,
                                         'role_AdminSmart'       => $userpass->role_id,
                                         'role_tipe_AdminSmart'  => $userpass->tipe,
							             'isadmin_AdminSmart'    => $userpass->is_admin,
							             'partner_AdminSmart'    => $userpass->partner,
                                         'program_AdminSmart'    => $userpass->program,
							             'email_user_AdminSmart' => $userpass->email);
                        $this->session->set_userdata($setuser);
                        $this->m_login->updateLastLogin($userpass->id);
                        
                        $data_log = array('userid'=>$userpass->id,'action'=>'Login','modul'=>'Login','data'=>json_encode($setuser));
                        $this->addLog($data_log);
                        
                        redirect(site_url('home'));   
                    }
                } else {
                    $this->session->set_flashdata('error_login', 'Password does not match!');
                    redirect(site_url('login'));
                }
            } else { 
                $this->session->set_flashdata('error_login', 'Username does not exist!');
                redirect(site_url('login'));
            }
        } else {
            redirect(site_url('login'));
        }
    }
    
}
