<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'controllers/base.php');

class Forgot_password extends Base {
    
    function __construct()
    {
		parent::__construct();
        $this->load->model('m_forgot_password');    
	}
	
	function index()
	{
        if ( !empty($this->username) ) {
	       redirect(site_url('home'));
	    } 
        
        $this->load->view('forgot_password',$this->data);
	} 
	
	function success()
	{
        if ( !empty($this->username) ) {
	       redirect(site_url('home'));
	    } 
        
        $this->load->view('forgot_password_success',$this->data);
	} 
    
    function doforgot(){
        if ($this->input->post()){
            $email = $this->input->post('email');
            if ($this->m_forgot_password->cekEmail($email)){
                
                $user = $this->m_forgot_password->getUserByEmail($email);
                
                $code = md5(uniqid(rand(), true));
                $url = site_url('reset_password/code/'.$code);
                
                $data = array('code'     => $code,
                              'userid'   => $user->id,
                              'username' => $user->username,
                              'email'    => $user->email);
                              
                if ( $this->m_forgot_password->createResetPassword($data) )
                {
                    $this->load->library('email');

                    $this->email->from($this->email_from, $this->email_from_name);
                    $this->email->to($email); 
                    
                    $this->email->subject('Change password Adstart');
                    $this->email->message('Click this link to change your password: '.$url);	
                    
                    $this->email->send();      
                    
                    redirect(site_url('forgot_password/success'));
                                      
                }
                                
            } else { 
                $this->data['error_email'] = '  Email tidak ditemukan !';
                $this->load->view('forgot_password',$this->data);
            }
        } else {
            redirect(site_url('forgot_password'));
        }
    }
    
}
