<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'controllers/base.php');

class Reset_password extends Base 
{
    function __construct()
    {
		parent::__construct();
        $this->load->model('m_reset_password');    
	}
	
	function index($code='')
	{
       $this->code($code);
	} 
	
	function code($code='')
	{
        if ( !empty($this->username) ) 
        {
	       redirect(site_url('home'));
	    } 
        
        if ( empty($code) ) 
        {
	       $this->data['error_reset'] = 'Your code is invalid.';
	    } 
        else
        {
            if ( ! $this->m_reset_password->cekCode($code) )
            {
                $this->data['error_reset'] = 'Your code is invalid.';
            } 
            else 
            {
                $data_reset = $this->m_reset_password->getResetPasswordData($code);
                $this->data['reset_email'] = $data_reset->email;
            }
        }
        
        $this->data['reset_code'] = $code;
        $this->load->view('reset_password',$this->data);
	} 
	
	function success()
	{
        if ( !empty($this->username) ) {
	       redirect(site_url('home'));
	    } 
        
        $this->load->view('reset_password_success',$this->data);
	} 
    
    function doreset()
    {
        if ($this->input->post())
        {
            $email = $this->input->post('email');
            $code = $this->input->post('code');
            $password = $this->input->post('password');
            
            if ($this->m_reset_password->updatePassword($email,$password))
            {
                $this->m_reset_password->updateResetPassword($code);
                
                redirect(site_url('reset_password/success'));                
            } 
            else 
            { 
               redirect(site_url('reset_password/failed'));
            }
        } 
        else 
        {
            redirect(site_url('reset_password'));
        }
    }
    
}
