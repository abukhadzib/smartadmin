<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'controllers/Base.php');

class Cta extends Base 
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_cta'); 
    }

    function index()
    {
        if ( empty($this->username) ) {
           redirect(site_url('login'));
        } 

        redirect(site_url('home'));
    } 

    function product($page=1)
    {
        if ( empty($this->username) ) {
       redirect(site_url('login'));
        } 

        $page = is_numeric($page) ? $page : 1;
        $page = $page < 1 ? 1 : $page;
        $page--;

        $data = $this->m_cta->getProduct($this->limit_page,$page);
        $data = json_decode($data);

        $this->data['page']         = $page;
        $this->data['perpage']      = $this->limit_page;
        $this->data['username']     = $this->username;
        $this->data['name']         = $this->name;
        $this->data['content']      = $data->rows;
        $this->data['view_content'] = 'cta/product';

        $link = site_url('cta/product');
        $this->data['pagination'] = paging(ceil($data->total/$this->limit_page),$page+1,$link,$data->total,$this->limit_page);

        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'cta/product','data'=>'');

        if ( !$this->cekAksesAction('cta/product','read_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';    
        }

        $this->addLog($data_log);

        $this->load->view('home',$this->data);
    }

    function product_add()
    {

        if ( empty($this->username) ) {
                echo "Session anda telah berakhir, refresh browser anda.";
                exit;
        }

        if ($this->input->post())
        {
            if ( !$this->cekAksesAction('cta/product','create_') ){
                    echo "{success:false, Msg:'Anda tidak mempunyai hak menambah data Product !.'}";
                    exit;
            }

            if ( $this->m_cta->cekCodeProduct($this->input->post('code')) )
            {
                    echo "{success:false, Msg:'Kode Product Sudah Ada !.'}";
                    exit;
            }

            $data = array( 'code'        => $this->input->post('code'),
                            'name'       => $this->input->post('name'),
                            'fee'        => $this->input->post('fee'),
                            'created_at' => date('Y-m-d H:i:s')
            );

            $data_log = array('userid'=>$this->userid,'action'=>'Add','modul'=>'cta/product_add','data'=>json_encode($data));
            $this->addLog($data_log);

            if ( $this->m_cta->addProduct($data))
            {
                    echo "{success:true, Msg:'Proses tambah data Product <b>Berhasil</b>'}";
                    exit;
            }
            else
            {
                    echo "{success:true, Msg:'Ada kesalahan dalam menambah data Product.'}";
                    exit;
            }
        }

        $data_log = array('userid'=>$this->userid,'action'=>'Add','modul'=>'cta/product_add','data'=>'');

        if ( !$this->cekAksesAction('cta/product','create_') ){
                $this->data['view_content'] = 'forbidden';
                $data_log['action'] = 'View Forbidden';
        }

        $this->load->view('home',$this->data);
    }

    function product_edit($id='')
    {
        if ( empty($this->username) ) {
                echo "Session anda telah berakhir, refresh browser anda.";
                exit;
        }

        if ($this->input->post())
        {
            if ( !$this->cekAksesAction('cta/product','update_') ){
                    echo "{success:true, Msg:'Anda tidak mempunyai hak merubah data Product !.'}";
                    exit;
            }

            if ( $this->m_cta->cekCodeProductnId($this->input->post('code'),$this->input->post('id')) )
            {
                    echo "{success:false, Msg:'Kode Product Sudah Ada !.'}";
                    exit;
            }

            $data = array( 'code'        => $this->input->post('code'),
                            'name'       => $this->input->post('name'),
                            'fee'        => $this->input->post('fee'),
                            'update_at'  => date('Y-m-d H:i:s')
            );


            if ( $this->m_cta->updateProduct($data,$this->input->post('id')))
            {
                    echo "{success:true, Msg:'Proses rubah data Product <b>Berhasil</b> !'}";
                    exit;
            }
            else
            {
                    echo "{success:false, Msg:'Parameter merubah data Product salah !'}";
                    exit;
            }
        }

        $this->data['content']      = '';
        $this->data['product']         = $this->m_cta->getProductById($id);

        if ( !$this->cekAksesAction('cta/product','update_') ){
                $this->data['view_content'] = 'forbidden';
                $data_log['action'] = 'View Forbidden';
        }

        $this->data['view_content']     = 'cta/product_edit';
        $this->load->view('home',$this->data);
    }

    function product_delete($id='')
    {
        if ( empty($this->username) ) {
            echo "{success:false, Msg:'Login dibutuhkan !'}";
            exit;
            } 

        if ( empty($id) ) {
            echo "{success:false, Msg:'Parameter tidak lengkap !'}";
            exit;
            } 

        if ($this->input->post() && $this->input->post('action')=='delete')
        {


            if ( !$this->cekAksesAction('cta/product','delete_') ){
                echo "{success:false, Msg:'Anda tidak berhak menghapus data Product !'}";
                exit; 
            }

            $id = $this->input->post('id');
            if ( $this->m_cta->deleteProduct($id) ){
                echo "{success:true, Msg:'Proses hapus data Product <b>Berhasil</b> !'}";
            }
        } else {
            echo "{success:false, Msg:'Parameter hapus data Product salah !'}";
        }

        $data_log = array('userid'=>$this->userid,'action'=>'delete','modul'=>'cta/product_delete','data'=>'');

        $this->addLog($data_log);
    }
	
    function inquiry($page=1)
    {
        if ( empty($this->username) ) {
                redirect(site_url('login'));
        }

        $page = is_numeric($page) ? $page : 1;
        $page = $page < 1 ? 1 : $page;
        $page--;

        $data = $this->m_cta->getCtaInquiry($this->limit_page,$page);
        $data = json_decode($data);

        $this->data['page']         = $page;
        $this->data['perpage']      = $this->limit_page;
        $this->data['username']     = $this->username;
        $this->data['name']         = $this->name;
        $this->data['content']      = $data->rows;
        $this->data['view_content'] = 'cta/inquiry';

        $link = site_url('cta/inquiry');
        $this->data['pagination'] = paging(ceil($data->total/$this->limit_page),$page+1,$link,$data->total,$this->limit_page);

        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'cta/inquiry','data'=>'');

        if ( !$this->cekAksesAction('cta/inquiry','read_') ){
                $this->data['view_content'] = 'forbidden';
                $data_log['action'] = 'View Forbidden';
        }

        $this->addLog($data_log);

        $this->load->view('home',$this->data);
    }
        
    function transaksi($page=1)
    {
        if ( empty($this->username) ) {
            redirect(site_url('login'));
        }

        $page = is_numeric($page) ? $page : 1;
        $page = $page < 1 ? 1 : $page;
        $page--;

        $data = $this->m_cta->getCtaTransaksi($this->limit_page,$page);
        $data = json_decode($data);

        $this->data['page']         = $page;
        $this->data['perpage']      = $this->limit_page;
        $this->data['username']     = $this->username;
        $this->data['name']         = $this->name;
        $this->data['content']      = $data->rows;
        $this->data['view_content'] = 'cta/transaksi';

        $link = site_url('cta/transaksi');
        $this->data['pagination'] = paging(ceil($data->total/$this->limit_page),$page+1,$link,$data->total,$this->limit_page);

        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'cta/transaksi','data'=>'');

        if ( !$this->cekAksesAction('cta/transaksi','read_') ){
                $this->data['view_content'] = 'forbidden';
                $data_log['action'] = 'View Forbidden';
        }

        $this->addLog($data_log);

        $this->load->view('home',$this->data);

    } 

    function export_inquiry(){
        $filter=array();

        $daterange      = $this->input->get('daterange');
        $startDate      = '';
        $endDate        = '';
        if(!empty($daterange)){
            $date       = explode('-',$daterange);
            $startDate  = date('Y-m-d',strtotime($date[0]));
            $endDate    = date('Y-m-d',strtotime($date[1]));
        }

        $name='data_cta_inquiry-'.date('His');

        $data     = $this->m_cta->exportCtaInquiry($startDate,$endDate,$filter,true);

        to_excel($data,$name); 
    }
        
    function export_transaksi(){
        $filter=array();

        $daterange      = $this->input->get('daterange');
        $startDate      = '';
        $endDate        = '';
        if(!empty($daterange)){
            $date       = explode('-',$daterange);
            $startDate  = date('Y-m-d',strtotime($date[0]));
            $endDate    = date('Y-m-d',strtotime($date[1]));
        }
        $name='data_Cta_transaksi-'.date('His');

        $data     = $this->m_cta->exportCtaTransaksi($startDate,$endDate,$filter,true);

        to_excel($data,$name); 
    }
    
}
