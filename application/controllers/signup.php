<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'controllers/base.php');

class Signup extends Base {
    
    function __construct()
    {
		parent::__construct();
        $this->load->model('m_signup');    
	}
	
	function index()
	{
        if ( !empty($this->username) ) {
	       redirect(site_url('home'));
	    } 
        
        $this->data['error_signup'] = $this->session->flashdata('error_signup');
        
        $this->load->view('signup',$this->data);
	} 
	
	function success()
	{
        if ( !empty($this->username) ) {
	       redirect(site_url('home'));
	    } 
        
        $this->load->view('signup_success',$this->data);
	} 
	
	function failed()
	{
        if ( !empty($this->username) ) {
	       redirect(site_url('home'));
	    } 
        
        $this->load->view('signup_failed',$this->data);
	} 
        
    function dosignup()
    {
        if ($this->input->post())
        {
            if ( $this->m_signup->cekUser($this->input->post('username')) )
            {
                $this->session->set_flashdata('error_signup', 'Username already exist !.');
                redirect(site_url('signup'));
            }
            
            $ga_screenshot = '';
            
            $config['upload_path']   = './assets/upload/ga_screenshot/';
            $config['allowed_types'] = 'gif|jpg|png|bmp';
            
            $this->load->library('upload', $config);
            
            if ($this->upload->do_upload('screnshoot'))
            {
                $upload_data   = $this->upload->data();
                $ga_screenshot = $upload_data['file_name'];
            }
			
            $data = array( 'username'      => $this->input->post('username'),
                           'password'      => md5($this->input->post('password')),
                           'email'         => $this->input->post('email'),
                           'tipe'          => $this->input->post('type_signup'),
                           'status'        => $this->input->post('status_signup'),
                           'name'          => $this->input->post('name'),
                           'phone'         => $this->input->post('phone'),
                           'address1'      => $this->input->post('address1'),
                           'address2'      => $this->input->post('address2'),
                           'zip'           => $this->input->post('zipcode'),
                           'country'       => $this->input->post('country'),
                           'fax'           => $this->input->post('fax'),
                           'ref_id'        => $this->input->post('ref_id'),
                           'description'   => $this->input->post('description'),
                           'url'           => $this->input->post('url'),
                           'company'       => $this->input->post('company'),
                           'category'      => $this->input->post('category'),
                           'web_url'       => $this->input->post('web_url'),
                           'mobile_url'    => $this->input->post('mobile_site'),
                           'bank'          => $this->input->post('bank_account'),
                           'npwp'          => $this->input->post('npwp'),
                           'pageview'      => $this->input->post('pageview'),
                           'ga_screenshot' => $ga_screenshot,
                           'role_id'       => $this->input->post('type_signup') == 'Publisher'  ? '2' : 
                                              $this->input->post('type_signup') == 'Advertiser' ? '3' : '1'
                    );
                    
            if ( $this->m_signup->createUser($data))
            {
                $code = md5(uniqid(rand(), true));
                $url = site_url('user/activate/'.$code);
                
                $data = array('code'     => $code,
                              'userid'   => $this->db->insert_id(),
                              'username' => $this->input->post('username'),
                              'email'    => $this->input->post('email'));
                
                if ( $this->m_signup->createActivateUser($data) )
                {
                    $this->load->library('email');
                    
                    $this->email->from($this->email_from, $this->email_from_name);
                    $this->email->to($this->input->post('email')); 
                    
                    $this->email->subject('Activate Account Signup Adstart');
                    $this->email->message('
                    
                    Dear, '. $this->input->post('name') . '
                    
                    Click this link to Activate your signup account: '.$url. '
                    
                    Thanks,
                    
                    Adstart.
                    
                    ');	
                    
                    $this->email->send();      
                    
                    redirect(site_url('signup/success'));
                }
            }
            else
            {
                redirect(site_url('signup/failed'));
            }            
            
        } else {
            redirect(site_url('signup'));
        }
    }
    
    function email_test()
    {
        $this->load->library('email');
    
        $this->email->from($this->email_from, $this->email_from_name);
        $this->email->to('a17dh4@yahoo.com,iskandar@m-stars.net,andha145@gmail.com'); 
        $this->email->subject('Activate Account Signup Adstart');
        $this->email->message('
        
        Dear, Test
        
        This is a testing email
        
        Thanks,
        
        Adstart.
        
        ');	
        
        if ( $this->email->send() )
        {
            echo "email testing berhasil dikirim";
        }     
        else
        {
            echo "email testing tidak berhasil dikirim";
        }
        
        echo $this->email->print_debugger(); 
        
    }
}
