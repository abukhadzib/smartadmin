<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'controllers/Base.php');

class Report extends Base 
{
    function __construct()
    {
		parent::__construct();
        $this->load->model('m_report');
	}
	
	function index()
	{
        if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
         
        redirect(site_url('report/activity'));
	} 
	
	function activity($page=1)
	{
        if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
        
        $page = is_numeric($page) ? $page : 1;
        $page = $page < 1 ? 1 : $page;
        $page--;
        
        $data = $this->m_report->getActivity($this->limit_page,$page);
        $data = json_decode($data);
        
        $this->data['page']         = $page;
        $this->data['perpage']      = $this->limit_page;
        $this->data['username']     = $this->username;
        $this->data['name']         = $this->name;
        $this->data['content']      = $data->rows;
        $this->data['view_content'] = 'report/activity';
        
        $link = site_url('report/activity');
        $this->data['pagination'] = paging(ceil($data->total/$this->limit_page),$page+1,$link,$data->total,$this->limit_page);
        
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'report/activity','data'=>'');
        
        if ( !$this->cekAksesAction('report/activity','read_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';    
        }
        
        //$this->addLog($data_log);
        
        $this->load->view('home',$this->data);
	} 
}