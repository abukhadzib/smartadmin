<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'controllers/Base.php');

class Merchant extends Base 
{
    function __construct()
    {
		parent::__construct();
        $this->load->model('m_merchant');
        $this->load->library('apiemail');
        $this->load->library('ciqrcode');
	}
	
	function index()
	{
        if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
         
        redirect(site_url('merchant/merchant'));
	} 
	
        function sound($sound ='')
	{
        if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
         
        redirect(site_url('assets/sound/'.$sound));
	}
        
	function merchant($page=1)
	{
        if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
            
        $page = is_numeric($page) ? $page : 1;
        $page = $page < 1 ? 1 : $page;
        $page--;
        
        $data = $this->m_merchant->getMerchant($this->limit_page,$page);
        $data = json_decode($data);
        
        $this->data['page']         = $page;
        $this->data['perpage']      = $this->limit_page;
        $this->data['username']     = $this->username;
        $this->data['name']         = $this->name;
        $this->data['content']      = $data->rows;
        $this->data['view_content'] = 'merchant/merchant';
        
        $link = site_url('merchant/merchant');
        $this->data['pagination'] = paging(ceil($data->total/$this->limit_page),$page+1,$link,$data->total,$this->limit_page);
        
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'merchant/merchant','data'=>'');
        
        if ( !$this->cekAksesAction('merchant/merchant','read_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';    
        }
        
        $this->addLog($data_log);
        
        $this->load->view('home',$this->data);
	} 

        function search_merchant($page=1)
	{
        if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
        
        $page = is_numeric($page) ? $page : 1;
        $page = $page < 1 ? 1 : $page;
        $page--;
        
        if($this->input->post()){
        
            $merchantCode   = $this->input->post('merchant_code');
            $msisdn         = $this->input->post('msisdn');

            
            $data = $this->m_merchant->searchMerchant($merchantCode,$msisdn,$this->limit_page,$page);
            $data = json_decode($data);

            $this->data['page']         = $page;
            $this->data['perpage']      = $this->limit_page;
            $this->data['username']     = $this->username;
            $this->data['name']         = $this->name;
            $this->data['content']      = $data->rows;
            $this->data['view_content'] = 'merchant/merchant';

            //$link = site_url('merchant/merchant');
            //$this->data['pagination'] = paging(ceil($data->total/$this->limit_page),$page+1,$link,$data->total,$this->limit_page);
        }else{
            $this->data['view_content'] = 'forbidden'; 
        }
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'merchant/merchant','data'=>'');
        
        if ( !$this->cekAksesAction('merchant/merchant','read_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';    
        }
        
        $this->addLog($data_log);
        
        $this->load->view('home',$this->data);
	}
        
        function merchant_add()
	{
        if ( empty($this->username) ) {
            echo "Session anda telah berakhir, refresh browser anda.";
            exit;
	    } 
        
        if ($this->input->post())
        {
            if ( !$this->cekAksesAction('merchant/merchant','create_') ){
                echo "{success:false, Msg:'Anda tidak mempunyai hak menambah data Merchant !.'}";
                exit;
            }  
		
            if ( $this->m_merchant->cekIdMerchant($this->input->post('id_merchant') ) ){
                echo "{success:true, Msg:'Id Merchant Sudah Ada !!.'}";
                exit;
            }  
            
            $nama           = $this->input->post('nama_merchant');
            $msisdn         = $this->input->post('handphone');
            $usaha          = $this->input->post('usaha');
            $type           = $this->input->post('type');
            $identitas      = $this->input->post('identitas');
            $alamat         = $this->input->post('alamat');
            $kodeMerchant   = $this->input->post('id_merchant');
            $bank           = $this->input->post('bank');
            $rekening       = $this->input->post('rekening');
            $namaRekening   = $this->input->post('nama_rekening');
            $hubungan       = $this->input->post('hubungan');
            $kodePos        = $this->input->post('zipcode');
            $email          = $this->input->post('email');
            $statusType     = $this->input->post('status');
            $sales          = $this->input->post('sales');
            $pic            = $this->input->post('pic');
            $discount       = $this->input->post('discount');
            //$is_active      = $this->input->post('is_active');
            
            $usaha          = str_replace('.', ' ', $usaha);
            $nama           = str_replace('.', ' ', $nama);
            $msisdn         = str_replace('.', '', $msisdn);
            $kodePos        = str_replace('.', '', $kodePos);
            $kodeMerchant   = str_replace('.', '', $kodeMerchant);
            $identitas      = str_replace('.', '', $identitas);
            $rekening       = str_replace('.', '', $rekening);
            $alamat         = str_replace('.', ' ', $alamat);
            $alamat         = str_replace('&', ' ', $alamat);
            
            if(substr($msisdn, 0, 1) === "0" && substr($msisdn, 0, 2) !== "62") {
                $msisdn = substr_replace($msisdn,"62",0,1);
            }
             
            $data = array( 'id_merchant'        => strtoupper($kodeMerchant),  
                            'name'              => $nama,
                            'phone'             => $msisdn,
                            'jenis_usaha'       => $usaha,
                            'zipcode'           => $kodePos,
                            'type_identitas'    => $type,
                            'alamat'            => $alamat,
                            'no_identitas'      => $identitas,
                            'id_bank'           => $this->input->post('idbank'),
                            'bank'              => $bank,
                            'rekening'          => $rekening,
                            'nama_rekening'     => $namaRekening,
                            'hubungan'          => $hubungan,
                            'mdr'               => $this->input->post('mdr'),
                            'email'             => $email,
                            'status'            => $statusType,
                            'sales'             => $sales,
                            'discount'          => $discount,
                            'pic'               => $pic,
                            //'is_active'         => $is_active,
                            'cluster'           => $this->input->post('cluster'),
                            'tgl_daftar'        => date('Y-m-d H:i:s')
                    );
            
            $data_log = array('userid'=>$this->userid,'action'=>'Add','modul'=>'merchant/merchant_add','data'=>json_encode($data));
            $this->addLog($data_log);
            
            /*Register Merchant to ST24*/
            if($statusType == 'MERCHANT'){
                $result = $this->apimerchant->regMerchant($nama,$msisdn,$usaha,$type,$identitas,$alamat,$kodeMerchant,$bank,$rekening,$kodePos,$email);
            }else{
                $result = $this->apimerchant->regLKD($nama,$msisdn,$usaha,$type,$identitas,$alamat,$kodeMerchant,$bank,$rekening,$kodePos,$email);
            }
            
            $status = explode(';', $result);
            $statusCode = $status[0];
            
            if($statusCode == '00'){
                        
                if ( $this->m_merchant->addMerchant($data))
                {
                    echo "{success:true, Msg:'Proses tambah data ".$statusType." <b>Berhasil</b>'}";
                    
                    $this->apiemail->sendEmail($nama,$msisdn,$usaha,$type,$identitas,$alamat,$kodeMerchant,$bank,$rekening,$kodePos,$email,$statusType);
                    
                    exit; 
                }    
                else
                {
                    echo "{success:true, Msg:'Ada kesalahan dalam menambah data ".$statusType." .'}";
                    exit; 
                }      
            }else if(strpos($result, 'BERHASIL') !== false){
                if ( $this->m_merchant->addMerchant($data))
                {
                    echo "{success:true, Msg:'Proses tambah data ".$statusType." <b>Berhasil</b>'}";
                    
                    $this->apiemail->sendEmail($nama,$msisdn,$usaha,$type,$identitas,$alamat,$kodeMerchant,$bank,$rekening,$kodePos,$email,$statusType);
                    
                    exit; 
                }    
                else
                {
                    echo "{success:true, Msg:'Ada kesalahan dalam menambah data ".$statusType." .'}";
                    exit; 
                }      
            }else{
                echo "{success:true, Msg:'Registrasi ".$statusType." di Mcoin Gagal'}";
                exit; 
            }
        }
         
        
            $data_log = array('userid'=>$this->userid,'action'=>'Add','modul'=>'merchant/merchant_add','data'=>'');

            if ( !$this->cekAksesAction('merchant/merchant','create_') ){
                $this->data['view_content'] = 'forbidden'; 
                $data_log['action'] = 'View Forbidden';    
            }

            $this->addLog($data_log);

            $this->data['bank']             = $this->m_merchant->getBank();
            $this->data['discount']         = $this->m_merchant->discountList();
            $this->data['view_content']     = 'merchant/merchant_add';
            $this->load->view('home',$this->data);

	} 
        
        function merchant_edit($id='')
	{
            if ( empty($this->username) ) {
                    echo "Session anda telah berakhir, refresh browser anda.";
                    exit;
            }

            if ( empty($id) ) {
                    echo "{success:true, Msg:'Parameter tidak lengkap !.'}";
                    exit;
            }

            if ($this->input->post())
            {
                if ( !$this->cekAksesAction('merchant/merchant','update_') ){
                        echo "{success:true, Msg:'Anda tidak mempunyai hak merubah data Merchant !.'}";
                        exit;
                }


                $balance = $this->input->post('balance');
                $mdr     = $this->input->post('mdr');
                
                $fee = ($balance*$mdr)/100;
                $withdraw = $balance - $fee;

                $data = array( 'id_merchant'        => strtoupper($this->input->post('id_merchant')),  
                    'name'              => $this->input->post('nama_merchant'),
                    'phone'             => $this->input->post('handphone'),
                    'jenis_usaha'       => $this->input->post('usaha'),
                    'zipcode'           => $this->input->post('zipcode'),
                    'alamat'            => $this->input->post('alamat'),
                    'type_identitas'    => $this->input->post('type'),
                    'no_identitas'      => $this->input->post('identitas'),
                    'id_bank'           => $this->input->post('idbank'),
                    'bank'              => $this->input->post('bank'),
                    'email'             => $this->input->post('email'),
                    'rekening'          => $this->input->post('rekening'),
                    'nama_rekening'     => $this->input->post('nama_rekening'),
                    'hubungan'          => $this->input->post('hubungan'),
                    'mdr'               => $this->input->post('mdr'),
                    'balance'           => $this->input->post('balance'),
                    'sales'             => $this->input->post('sales'),
                    'pic'               => $this->input->post('pic'),
                    'cluster'           => $this->input->post('cluster'),
                    'discount'          => $this->input->post('discount'),
                    'is_active'         => $this->input->post('is_active'),
                    'fee'               => $fee,
                    'withdraw'          => $withdraw,
                    'status'            => $this->input->post('status')
                );

                $data_log = array('userid'=>$this->userid,'action'=>'Edit','modul'=>'data_master/Merchant_edit','data'=>json_encode($data));
                $this->addLog($data_log);
            
                if ( $this->m_merchant->updateMerchant($data,$this->input->post('id')))
                {
                        echo "{success:true, Msg:'Proses rubah data Merchant <b>Berhasil</b> !'}";
                        exit;
                }
                else
                {
                        echo "{success:false, Msg:'Parameter merubah data Merchant salah !'}";
                        exit;
                }
            }

            $this->data['content']      = '';
            $this->data['merchant']         = $this->m_merchant->getMerchantById($id);

            if ( !$this->cekAksesAction('merchant/merchant','update_') ){
                    $this->data['view_content'] = 'forbidden';
                    $data_log['action'] = 'View Forbidden';
            }

            $this->data['bank']             = $this->m_merchant->getBank();
            $this->data['discount']         = $this->m_merchant->discountList();
            $this->data['view_content']     = 'merchant/merchant_edit';
            $this->load->view('home',$this->data);
                
	}
        
        function merchant_delete($id='')
	{
            if ( empty($this->username) ) {
                echo "{success:false, Msg:'Login dibutuhkan !'}";
                exit;
                } 

            if ( empty($id) ) {
                echo "{success:false, Msg:'Parameter tidak lengkap !'}";
                exit;
                } 

            if ($this->input->post() && $this->input->post('action')=='delete')
            {


                if ( !$this->cekAksesAction('merchant/merchant','delete_') ){
                    echo "{success:false, Msg:'Anda tidak berhak menghapus data Merchant !'}";
                    exit; 
                }

                $id = $this->input->post('id');
                if ( $this->m_merchant->deleteMerchant($id) ){
                    echo "{success:true, Msg:'Proses hapus data Merchant <b>Berhasil</b> !'}";
                }
            } else {
                echo "{success:false, Msg:'Parameter hapus data Merchant salah !'}";
            }

            $data_log = array('userid'=>$this->userid,'action'=>'delete','modul'=>'merchant/merchant_delete','data'=>'');

            $this->addLog($data_log);
        
	}

        function discount($page=1)
	{
            if ( empty($this->username) ) {
                   redirect(site_url('login'));
                } 

            $page = is_numeric($page) ? $page : 1;
            $page = $page < 1 ? 1 : $page;
            $page--;

            $data = $this->m_merchant->getDiscount($this->limit_page,$page);
            $data = json_decode($data);

            $this->data['page']         = $page;
            $this->data['perpage']      = $this->limit_page;
            $this->data['username']     = $this->username;
            $this->data['name']         = $this->name;
            $this->data['content']      = $data->rows;
            $this->data['view_content'] = 'merchant/discount';

            $link = site_url('merchant/discount');
            $this->data['pagination'] = paging(ceil($data->total/$this->limit_page),$page+1,$link,$data->total,$this->limit_page);

            $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'merchant/discount','data'=>'');

            if ( !$this->cekAksesAction('merchant/discount','read_') ){
                $this->data['view_content'] = 'forbidden'; 
                $data_log['action'] = 'View Forbidden';    
            }

            $this->addLog($data_log);

            $this->load->view('home',$this->data);
	} 
        
        function discount_add()
	{
            if ( empty($this->username) ) {
                echo "Session anda telah berakhir, refresh browser anda.";
                exit;
                } 

            if ($this->input->post())
            {
                if ( !$this->cekAksesAction('merchant/merchant','create_') ){
                    echo "{success:false, Msg:'Anda tidak mempunyai hak menambah data Merchant !.'}";
                    exit;
                }  

            if ($this->m_merchant->cekStatusDiscountAllMerchant() && $this->input->post('status') == 'ALL MERCHANT' ){
                echo "{success:false, Msg:'Discount Untuk All Merchant Sudah Ada !!.'}";
                exit;
            }  
                
                
                $name           = $this->input->post('name');
                $get_discount   = $this->input->post('get_discount');
                $type           = $this->input->post('type');
                $discount       = $this->input->post('discount');
                $max_discount   = $this->input->post('max_discount');
                $fee            = $this->input->post('fee');
                $app_code       = $this->input->post('app_code');
                $fromDate       = $this->input->post('from_date');
                $toDate         = $this->input->post('to_date');
                $maxUser        = $this->input->post('maxuser');
                $budget         = $this->input->post('budget');
                $status         = $this->input->post('status');

                $data = array(  'name'                  => $name, 
                                'nominal_get_discount'  => $get_discount,  
                                'type'                  => $type,
                                'discount'              => $discount,
                                'max_discount'          => $max_discount,
                                'fee'                   => $fee,
                                'from_date'             => $fromDate,
                                'to_date'               => $toDate,
                                'limit_user'            => $maxUser,
                                'limit_balance'         => $budget,
                                'status'                => $status,
                                'app_code'              => strtoupper($app_code)
                        );

                $data_log = array('userid'=>$this->userid,'action'=>'Add','modul'=>'merchant/discount_add','data'=>json_encode($data));
                $this->addLog($data_log);
                if($status == 'ALL MERCHANT'){
                    $id = $this->m_merchant->addDiscount($data);
                    if ($this->m_merchant->injectDiscount($id))
                    {
                        echo "{success:true, Msg:'Proses tambah data  <b>Berhasil</b>'}";
                        exit; 
                    }    
                    else
                    {
                        $this->m_merchant->deleteDiscount($id);
                        echo "{success:true, Msg:'Ada kesalahan dalam menambah data .'}";
                        exit; 
                    }
                }else{
                     if ($this->m_merchant->addDiscount($data))
                    {
                        echo "{success:true, Msg:'Proses tambah data  <b>Berhasil</b>'}";
                        exit; 
                    }    
                    else
                    {
                        echo "{success:true, Msg:'Ada kesalahan dalam menambah data .'}";
                        exit; 
                    }
                }
                     

            }
         

            $data_log = array('userid'=>$this->userid,'action'=>'Add','modul'=>'merchant/discount_add','data'=>'');

            if ( !$this->cekAksesAction('merchant/discount','create_') ){
                $this->data['view_content'] = 'forbidden'; 
                $data_log['action'] = 'View Forbidden';    
            }

            $this->addLog($data_log);

            $this->data['view_content']     = 'forbidden';
            $this->load->view('home',$this->data);
        
	} 
        
        function discount_edit($id='')
	{
            if ( empty($this->username) ) {
                    echo "Session anda telah berakhir, refresh browser anda.";
                    exit;
            }


            if ($this->input->post())
            {
                if ( !$this->cekAksesAction('merchant/discount','update_') ){
                        echo "{success:true, Msg:'Anda tidak mempunyai hak merubah data Discount !.'}";
                        exit;
                }

                $name           = $this->input->post('name');
                $get_discount   = $this->input->post('get_discount');
                $type           = $this->input->post('type');
                $discount       = $this->input->post('discount');
                $max_discount   = $this->input->post('max_discount');
                $fee            = $this->input->post('fee');
                $app_code       = $this->input->post('app_code');
                $fromDate       = $this->input->post('from_date');
                $toDate         = $this->input->post('to_date');
                $maxUser        = $this->input->post('maxuser');
                $budget         = $this->input->post('budget');
                $status         = $this->input->post('status');
                
                $data = array(  'name'                  => $name,
                                'nominal_get_discount'  => $get_discount,  
                                'type'                  => $type,
                                'discount'              => $discount,
                                'max_discount'          => $max_discount,
                                'fee'                   => $fee,
                                'from_date'             => $fromDate,
                                'to_date'               => $toDate,
                                'limit_user'            => $maxUser,
                                'limit_balance'         => $budget,
                                'status'                => $status,
                                'app_code'              => strtoupper($app_code)
                        );

                $getData = $this->m_merchant->getDiscountAllMerchant();
                $idDiscount = isset($getData['id']) ? $getData['id'] : '';
                $statusDiscount = isset($getData['status']) ? $getData['status'] : '';
                
                if($status == 'ALL MERCHANT' && $idDiscount != '' && $this->input->post('id') != $idDiscount){
                    echo "{success:false, Msg:'Discount untuk All Merchant sudah Ada'}";
                    exit;
                }
                
                $data_log = array('userid'=>$this->userid,'action'=>'Edit','modul'=>'merchant/discount_edit','data'=>json_encode($data));
                $this->addLog($data_log);
                
                if($idDiscount != ''){
                    if($status == 'ALL MERCHANT' && $this->input->post('id') == $idDiscount ){
                        if ( $this->m_merchant->updateDiscount($data,$this->input->post('id')))
                        {
                                echo "{success:true, Msg:'Proses rubah data Discount <b>Berhasil</b> !'}";
                                exit;
                        }
                        else
                        {
                                echo "{success:false, Msg:'Parameter merubah data Discount salah !'}";
                                exit;
                        }
                    }else if($status != 'ALL MERCHANT' && $this->input->post('id') == $idDiscount ){
                        if ( $this->m_merchant->updateDiscount($data,$this->input->post('id')) && $this->m_merchant->changeInjectDiscount($this->input->post('id')))
                        {
                                echo "{success:true, Msg:'Proses rubah data Discount <b>Berhasil</b> !'}";
                                exit;
                        }
                        else
                        {
                                echo "{success:false, Msg:'Parameter merubah data Discount salah !'}";
                                exit;
                        }
                    }else{
                        if ( $this->m_merchant->updateDiscount($data,$this->input->post('id')))
                        {
                                echo "{success:true, Msg:'Proses rubah data Discount <b>Berhasil</b> !'}";
                                exit;
                        }
                        else
                        {
                                echo "{success:false, Msg:'Parameter merubah data Discount salah !'}";
                                exit;
                        }
                    }
                }else{
                    if($status == 'ALL MERCHANT'){
                        if ( $this->m_merchant->updateDiscount($data,$this->input->post('id')) && $this->m_merchant->injectDiscount($this->input->post('id')) )
                        {
                                echo "{success:true, Msg:'Proses rubah data Discount <b>Berhasil</b> !'}";
                                exit;
                        }
                        else
                        {
                                echo "{success:false, Msg:'Parameter merubah data Discount salah !'}";
                                exit;
                        }
                    }else{
                        if ( $this->m_merchant->updateDiscount($data,$this->input->post('id')))
                        {
                                echo "{success:true, Msg:'Proses rubah data Discount <b>Berhasil</b> !'}";
                                exit;
                        }
                        else
                        {
                                echo "{success:false, Msg:'Parameter merubah data Discount salah !'}";
                                exit;
                        }
                    }
                    
                }
                
                
            }

            $this->data['content']      = '';
            $this->data['discount']         = $this->m_merchant->getDiscountById($id);

            if ( !$this->cekAksesAction('merchant/discount','update_') ){
                    $this->data['view_content'] = 'forbidden';
                    $data_log['action'] = 'View Forbidden';
            }

            $this->data['view_content']     = 'merchant/discount_edit';
            $this->load->view('home',$this->data);
                
	}
        
        function discount_delete($id='')
	{
            if ( empty($this->username) ) {
                echo "{success:false, Msg:'Login dibutuhkan !'}";
                exit;
                } 

            if ( empty($id) ) {
                echo "{success:false, Msg:'Parameter tidak lengkap !'}";
                exit;
                } 

            if ($this->input->post() && $this->input->post('action')=='delete')
            {


                if ( !$this->cekAksesAction('merchant/discount','delete_') ){
                    echo "{success:false, Msg:'Anda tidak berhak menghapus data discount !'}";
                    exit; 
                }

                $id = $this->input->post('id');
                if ( $this->m_merchant->deleteDiscount($id) ){
                    echo "{success:true, Msg:'Proses hapus data discount <b>Berhasil</b> !'}";
                }
            } else {
                echo "{success:false, Msg:'Parameter hapus data discount salah !'}";
            }

            $data_log = array('userid'=>$this->userid,'action'=>'delete','modul'=>'merchant/discount_delete','data'=>'');

            $this->addLog($data_log);
        
	}
        
        function list_discount($page=1)
	{
        if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
            
        $page = is_numeric($page) ? $page : 1;
        $page = $page < 1 ? 1 : $page;
        $page--;
        
        $data = $this->m_merchant->getListDiscount($this->limit_page,$page);
        $data = json_decode($data);
        
        $this->data['page']         = $page;
        $this->data['perpage']      = $this->limit_page;
        $this->data['username']     = $this->username;
        $this->data['name']         = $this->name;
        $this->data['content']      = $data->rows;
        $this->data['view_content'] = 'merchant/list';
        
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'merchant/list_discount','data'=>'');
        
        if ( !$this->cekAksesAction('merchant/list_discount','read_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';    
        }
        
        $this->addLog($data_log);
        
        $this->load->view('home',$this->data);
	} 
        
        function generate_code($id='')
	{
		if ( empty($this->username) ) {
			echo "Session anda telah berakhir, refresh browser anda.";
			exit;
		}
	
		if ( empty($id) ) {
			echo "{success:true, Msg:'Parameter tidak lengkap !.'}";
			exit;
		}
	
		$this->data['content']          = '';
                $code =  $this->m_merchant->getMerchantById($id);
		$this->data['merchant']         = $code;//$this->m_merchant->getMerchantById($id);
	
		if ( !$this->cekAksesAction('merchant/merchant','update_') ){
			$this->data['view_content'] = 'forbidden';
			$data_log['action'] = 'View Forbidden';
		}
                
                $params['data'] = strtoupper($code->id_merchant);
                $params['level'] = 'H';
                $params['size'] = 12;
                $params['savename'] = CODEPATH.strtoupper($code->id_merchant).'.png';
                $this->ciqrcode->generateCode($params);
                
		$this->load->view('merchant/generate_code',$this->data);
	}
        
	function inquiry($page=1)
	{
		if ( empty($this->username) ) {
			redirect(site_url('login'));
		}
	
		$page = is_numeric($page) ? $page : 1;
		$page = $page < 1 ? 1 : $page;
		$page--;
	
		$data = $this->m_merchant->getInquiry($this->limit_page,$page);
		$data = json_decode($data);
	
		$this->data['page']         = $page;
		$this->data['perpage']      = $this->limit_page;
		$this->data['username']     = $this->username;
		$this->data['name']         = $this->name;
		$this->data['content']      = $data->rows;
		$this->data['view_content'] = 'merchant/inquiry';
	
		//$link = site_url('merchant/inquiry');
		//$this->data['pagination'] = paging(ceil($data->total/$this->limit_page),$page+1,$link,$data->total,$this->limit_page);
	
		$data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'merchant/inquiry','data'=>'');
	
		if ( !$this->cekAksesAction('merchant/inquiry','read_') ){
			$this->data['view_content'] = 'forbidden';
			$data_log['action'] = 'View Forbidden';
		}
	
		$this->addLog($data_log);
	
		$this->load->view('home',$this->data);
	}
	
        function search_inquiry($page=1)
	{
        if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
        
        $page = is_numeric($page) ? $page : 1;
        $page = $page < 1 ? 1 : $page;
        $page--;
        
        if($this->input->post()){
        
            $merchantCode   = $this->input->post('merchant_code');
            $msisdn         = $this->input->post('msisdn');
            $daterange      = $this->input->post('daterange');
            $startDate = '';
            $endDate = '';
            
            if(!empty($daterange)){
                $date       = explode('-',$daterange);
                $startDate  = date('Y-m-d',strtotime($date[0]));
                $endDate    = date('Y-m-d',strtotime($date[1]));
            }
            $data = $this->m_merchant->searchInquiry($merchantCode,$msisdn,$startDate,$endDate,$this->limit_page,$page);
            $data = json_decode($data);

            $this->data['page']         = $page;
            $this->data['perpage']      = $this->limit_page;
            $this->data['username']     = $this->username;
            $this->data['name']         = $this->name;
            $this->data['content']      = $data->rows;
            $this->data['view_content'] = 'merchant/inquiry';

            //$link = site_url('merchant/inquiry');
            //$this->data['pagination'] = paging(ceil($data->total/$this->limit_page),$page+1,$link,$data->total,$this->limit_page);
        }else{
            $this->data['view_content'] = 'forbidden'; 
        }
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'merchant/inquiry','data'=>'');
        
        if ( !$this->cekAksesAction('merchant/inquiry','read_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';    
        }
        
        $this->addLog($data_log);
        
        $this->load->view('home',$this->data);
	}
        
        function transaksi($page=1)
	{
        if ( empty($this->username) ) {
			redirect(site_url('login'));
		}

		$page = is_numeric($page) ? $page : 1;
		$page = $page < 1 ? 1 : $page;
		$page--;
	
		$data = $this->m_merchant->getTransaksi($this->limit_page,$page);
		$data = json_decode($data);
	
		$this->data['page']         = $page;
		$this->data['perpage']      = $this->limit_page;
		$this->data['username']     = $this->username;
		$this->data['name']         = $this->name;
		$this->data['content']      = $data->rows;
		$this->data['view_content'] = 'merchant/transaksi';
	
		//$link = site_url('merchant/transaksi');
		//$this->data['pagination'] = paging(ceil($data->total/$this->limit_page),$page+1,$link,$data->total,$this->limit_page);
	
		$data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'merchant/transaksi','data'=>'');
	
		if ( !$this->cekAksesAction('merchant/transaksi','read_') ){
			$this->data['view_content'] = 'forbidden';
			$data_log['action'] = 'View Forbidden';
		}
	
		$this->addLog($data_log);
	
		$this->load->view('home',$this->data);
        
	} 
        
        function search_transaksi($page=1)
	{
        if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
        
        $page = is_numeric($page) ? $page : 1;
        $page = $page < 1 ? 1 : $page;
        $page--;
        
        if($this->input->post()){
        
            $merchantCode   = $this->input->post('merchant_code');
            $status         = $this->input->post('status');
            $daterange      = $this->input->post('daterange');
            $startDate = '';
            $endDate = '';
            
            if(!empty($daterange)){
                $date       = explode('-',$daterange);
                $startDate  = date('Y-m-d',strtotime($date[0]));
                $endDate    = date('Y-m-d',strtotime($date[1]));
            }
            $data = $this->m_merchant->searchTransaksi($merchantCode,$status,$startDate,$endDate,$this->limit_page,$page);
            $data = json_decode($data);

            $this->data['page']         = $page;
            $this->data['perpage']      = $this->limit_page;
            $this->data['username']     = $this->username;
            $this->data['name']         = $this->name;
            $this->data['content']      = $data->rows;
            $this->data['view_content'] = 'merchant/transaksi';

            //$link = site_url('merchant/transaksi');
            //$this->data['pagination'] = paging(ceil($data->total/$this->limit_page),$page+1,$link,$data->total,$this->limit_page);
        }else{
            $this->data['view_content'] = 'forbidden'; 
        }
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'merchant/transaksi','data'=>'');
        
        if ( !$this->cekAksesAction('merchant/transaksi','read_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';    
        }
        
        $this->addLog($data_log);
        
        $this->load->view('home',$this->data);
	}
        
        function qrcode()
        {
            if ( empty($this->username) ) {
           redirect(site_url('login'));
            } 
            
            if($this->input->post()){
                $code   = $this->input->post('code');
                
                if ( $this->m_merchant->cekIdMerchant($code ) ){
                echo "{success:false, Msg:'Kode Merchant Sudah Ada !!.'}";
                exit;
                }
                
                echo "{success:true, Msg:'Generate QR Code  <b>Berhasil</b>'}";
                
                $params['data'] = strtoupper($code);
                $params['level'] = 'H';
                $params['size'] = 12;
                $params['savename'] = CODEPATH.strtoupper($code).'.png';
                $this->ciqrcode->generateCode($params);
                exit;
            }else{
                
                $this->data['view_content']     = 'merchant/qrcode';
            }
		
            if ( !$this->cekAksesAction('merchant/qrcode','read_') ){
                $this->data['view_content'] = 'forbidden'; 
                $data_log['action'] = 'View Forbidden';    
            }


            $this->load->view('home',$this->data);
        }
        
        function showCode($code='') {
        	
        	
            $this->data['username']     = $this->username;
            $this->data['name']         = $this->name;
            $this->data['view_content'] = 'merchant/code';
            $this->data['code'] = strtoupper($code);
            $this->load->view('home',  $this->data);
        }
        
        function export_merchant($type=''){
            $filter=array();
            
            $daterange      = $this->input->get('daterange');
            $type           = $this->input->get('type');
            $startDate      = '';
            $endDate        = '';
            
            if(!empty($daterange)){
                $date       = explode('-',$daterange);
                $startDate  = date('Y-m-d',strtotime($date[0]));
                $endDate    = date('Y-m-d',strtotime($date[1]));
            }
            
            $name='data_merchant-'.date('His');

            $data     = $this->m_merchant->exportMerchant($type,$startDate,$endDate,$filter,true);
            
            to_excel($data,$name); 
        }
        
        function export_inquiry(){
            $filter=array();
            
            $merchantCode   = $this->input->get('merchant_code');
            $msisdn         = $this->input->get('msisdn');
            $daterange      = $this->input->get('daterange');
            $startDate      = '';
            $endDate        = '';
            if(!empty($daterange)){
                $date       = explode('-',$daterange);
                $startDate  = date('Y-m-d',strtotime($date[0]));
                $endDate    = date('Y-m-d',strtotime($date[1]));
            }
            
            $name='data_inquiry-'.date('His');

            $data     = $this->m_merchant->exportInquiry($merchantCode,$msisdn,$startDate,$endDate,$filter,true);
            
            to_excel($data,$name); 
        }
        
        function export_transaksi(){
            $filter=array();
            
            $merchantCode   = $this->input->get('merchant_code');
            $status         = $this->input->get('status');
            $daterange      = $this->input->get('daterange');
            $startDate      = '';
            $endDate        = '';
            if(!empty($daterange)){
                $date       = explode('-',$daterange);
                $startDate  = date('Y-m-d',strtotime($date[0]));
                $endDate    = date('Y-m-d',strtotime($date[1]));
            }
            $name='data_transaksi-'.date('His');

            $data     = $this->m_merchant->exportTransaksi($merchantCode,$status,$startDate,$endDate,$filter,true);
            
            to_excel($data,$name); 
        }
}