<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'controllers/Base.php');

class Logout extends Base {

    function __construct()
    {
		parent::__construct();        
        
		set_cookie('authCookieAdminSmart','',time()-3600);
		
        $setuser = array('userid_AdminSmart'     => '',
                         'username_AdminSmart'   => '',
                         'name_AdminSmart'       => '',
                         'role_AdminSmart'       => '',
                         'isadmin_AdminSmart'    => '',
                         'partner_AdminSmart'    => '',
                         'program_AdminSmart'    => '',
                         'email_user_AdminSmart' => '');
         
        $data_log = array('userid'=>$this->userid,'action'=>'Logout','modul'=>'Logout','data'=>json_encode($setuser));
        $this->addLog($data_log);
        
        $this->session->set_userdata($setuser);          
                        
        redirect(site_url('login'));         
	}
    
    function index()
	{
        
	}
    
}
