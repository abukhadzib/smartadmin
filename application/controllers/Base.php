<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Base extends CI_Controller {

	var $css;
    var $img;
    var $images;
    var $js;
    var $title;
    var $data;
    var $limit_page    = 1000000;
    var $limit_page_default    = 1000000;
    var $space_page_no = 5;
    var $userid = '';
    var $username = '';
    var $name = '';
    var $role = '';
    var $role_tipe = '';
    var $partner = '';
    var $program = '';
    var $isAdmin = '';
    
    function __construct()
    {
		parent::__construct();
        
        $this->userid                     = $this->session->userdata('userid_AdminSmart');   
        $this->username                   = $this->session->userdata('username_AdminSmart');  
        $this->name                       = $this->session->userdata('name_AdminSmart');  
        $this->role                       = $this->session->userdata('role_AdminSmart');    
        $this->role_tipe                  = $this->session->userdata('role_tipe_AdminSmart');    
        $this->isAdmin                    = $this->session->userdata('isadmin_AdminSmart');
        $this->partner                    = $this->session->userdata('partner_AdminSmart');
        $this->program                    = $this->session->userdata('program_AdminSmart');
        
        $this->css                        = $this->config->item('css_url');   
        $this->img                        = $this->config->item('img_url');  
        $this->images                     = $this->config->item('images_url');  
        $this->js                         = $this->config->item('js_url');   
        $this->plugins                    = $this->config->item('plugins_url');   
        $this->title                      = $this->config->item('app_title');    
        $this->qrcode                     = $this->config->item('qrcode_url');  
        $this->sound                     = $this->config->item('sound_url'); 
        
        $this->data['css']                = $this->css;
        $this->data['img']                = $this->img;
        $this->data['images']             = $this->images;
        $this->data['js']                 = $this->js;
        $this->data['plugins']            = $this->plugins;
        $this->data['title']              = $this->title;
        $this->data['qrcode']             = $this->qrcode;
        $this->data['sound']             = $this->sound;
        
        $this->data['view_content']       = '';
        $this->data['pagination']         = '';   
          
        $this->data['username']           = $this->username;
        $this->data['name']               = $this->name;
        $this->data['admin']              = $this->isAdmin;     
        $this->data['menu_role']          = empty($this->role) ? '' : $this->menu();  
              
	}
    
    function addLog($data)
    {
        return $this->m_base->addLog($data); 
    }
    
    function menu()
    {
        $role_menu = $this->m_base->getMenu($this->role);     
        
        $menu_role = array();
        
        foreach($role_menu as $menu_row):
            if (empty($menu_row->header)):
                $menu_role[$menu_row->id]['menu'] = $menu_row;
            else:
                $menu_role[$menu_row->header]['submenu'][$menu_row->id] = $menu_row;
                $menu_role[$menu_row->header]['menu']->notifikasi += $menu_row->notifikasi;
            endif;
        endforeach;
        
        return $menu_role;
    }
    
    function cekAkses($url)
    {
        return $this->m_base->cekMenuRole($this->role,$url);
    }
    
    function cekAksesAction($url,$action)
    {
        return $this->m_base->cekMenuRoleAction($this->role,$url,$action);
    }
     
    function getConfig($name='')
    {
        return $this->m_base->getConfig($name);
    }
    
    function updateConfig($name='',$value='')
    {
        return $this->m_base->updateConfig($name,$value);
    }
    
    function pdf($orientation='P',$paper,$title='Pdf Example',$author='Author',$name='pdfexample.pdf',$dest='I',$html='',$font_size=10)
    {
        ob_start();  
        
        $this->load->library('pdf');
        
        $pdf = new Pdf($orientation, 'mm', $paper, true, 'UTF-8', false);
        $pdf->SetTitle($title);
        
        $pdf->SetTopMargin(10);
        $pdf->setFooterMargin(10);
        $pdf->SetAutoPageBreak(true,10);
        
        $pdf->SetAuthor($author);
        $pdf->SetDisplayMode('real', 'default');
        
        $pdf->SetMargins(10, 10, 10);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        
        //$pdf->SetFont('dejavusans', '', 10);
        $pdf->SetFont('helvetica', '', $font_size);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        
        //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        
        $pdf->AddPage($orientation);
        $pdf->writeHTML($html, true, false, false, false, '');
        
        $pdf->Output($name,$dest);
    }
    
    function js()
    {
        $js = $this->load->view('js',$this->data,true);
        $this->load->library('jsmin',array('js'=>$js));
        echo $this->jsmin->min();
    }
    
}
