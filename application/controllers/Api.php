<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'controllers/Base.php');

class Api extends Base {

	function __construct()
    {
		parent::__construct(); 
        $this->load->model('m_data_master'); 
        $this->load->model('m_pesan'); 
	}
	
	function index()
	{
		$ret = array('status'=>'FAILED','error_msg'=>'Invalid parameter !');
        
        header('Content-Type: application/json');
        echo json_encode($ret); 
	} 
	
	function pesan()
	{
		$ret = array('status'=>'FAILED');
        
        if ( $this->input->get() )
        {
            $key  = $this->input->get('key');
            $page = $this->input->get('page');
            $date = $this->input->get('date');
            
            $page = is_numeric($page) ? $page : 1;
            $page = $page < 1 ? 1 : $page;
            $page--;
                          
            $partner = $this->m_data_master->getPartnerBySecureKey($key);
            if ( $partner )
            {
                $ret['status'] = 'SUCCESS';
                
                $data = $this->m_pesan->getPesanByPartnerId($partner->id,$this->limit_page,$page,$date);  
                $data = json_decode($data);
                
                $ret['data']       = $data->rows;
                $ret['total_row']  = $data->total;
                $ret['total_page'] = ceil($data->total/$this->limit_page);
                $ret['page']       = $page+1;
                $ret['per_page']   = $this->limit_page ;
            }
            else
            {
                $program = $this->m_data_master->getProgramBySecureKey($key);
                if ( $program )
                {
                    $ret['status'] = 'SUCCESS';
                    
                    $data = $this->m_pesan->getPesanByProgramId($program->id,$this->limit_page,$page,$date);  
                    $data = json_decode($data);
                    
                    $ret['data']       = $data->rows;
                    $ret['total_row']  = $data->total;
                    $ret['total_page'] = ceil($data->total/$this->limit_page);
                    $ret['page']       = $page+1;
                    $ret['per_page']   = $this->limit_page ;
                }
                else
                {
                    $subprogram = $this->m_data_master->getSubProgramBySecureKey($key);
                    if ( $subprogram )
                    {
                        $ret['status'] = 'SUCCESS';
                        
                        $data = $this->m_pesan->getPesanBySubProgramId($subprogram->id,$this->limit_page,$page,$date);  
                        $data = json_decode($data);
                        
                        $ret['data']       = $data->rows;
                        $ret['total_row']  = $data->total;
                        $ret['total_page'] = ceil($data->total/$this->limit_page);
                        $ret['page']       = $page+1;
                        $ret['per_page']   = $this->limit_page ;
                    }
                    else
                    {
                        $ret = array('status'=>'FAILED','error_msg'=>'Key parameter is not recognized!');    
                    }    
                }                
            }
        }
        else
        {
            $ret = array('status'=>'FAILED','error_msg'=>'Parameters required!');    
        }
        
        header('Content-Type: application/json');
        echo json_encode($ret); 
	} 
}
